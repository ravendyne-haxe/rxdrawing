// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxdrawing.core.graphics.NamedColor;
import rxdrawing.components.containers.Group;
import rxlayout.types.SizeValue;
import rxdrawing.components.containers.HBox;
import rxdrawing.components.Label;
import rxdrawing.components.Shape;
import rxdrawing.components.containers.VBox;
import rxlayout.types.LayoutBox;
import rxdrawing.components.containers.StackingContainer;
import rxdrawing.components.containers.Container;
import rxdrawing.elements.Panel;
import rxdrawing.elements.Path;
// import rxdrawing.elements.Group;
import rxdrawing.elements.Image;
import rxdrawing.elements.Text;
import rxdrawing.elements.QuadraticCurve;
import rxdrawing.elements.CubicCurve;
import rxdrawing.elements.Polygon;
import rxdrawing.elements.Polyline;
import rxdrawing.core.graphics.Graphics;
import rxdrawing.elements.Ellipse;
import rxdrawing.elements.Line;
import rxdrawing.elements.Circle;
import rxdrawing.elements.Rectangle;
import rxsvggraphics.SvgGraphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;

class ContainersDemo {
    public static function stacking( container : js.html.DivElement ) {
        var g = new SvgGraphics( container );

        var sc1 = new StackingContainer( g );
        sc1.layout.setPositionAndSize( 100.px(), 100.px(), 200.px(), 350.px() );
        sc1.setBorder( 2, 'blue' );
        sc1.setBackground( 'gray' );

        var c1 = new Container( g, sc1 );
        c1.layout.setSize( 150.px(), 150.px() );
        c1.setBorder( 2, 'blue' );
        c1.setBackground( 'orange' );

        var c2 = new Container( g, sc1 );
        c2.layout.setSize( 100.px(), 100.px() );
        c2.setBorder( 2, 'blue' );
        c2.setBackground( 'maroon' );

        var c3 = new Container( g, sc1 );
        // c3.layout.setPositionAndSize( 20.px(), 20.px(), 50.px(), 50.px() );
        c3.layout.setSize( 50.px(), 50.px() );
        c3.setBorder( 2, 'blue' );
        c3.setBackground( 'cornfowerblue' );

        var c4 = new HBox( g, sc1 );
        c4.layout.setPositionAndSize( 20.px(), 20.px(), 25.px(), 25.px() );
        // c4.layout.setSize( 25.px(), 25.px() );
        c4.setBorder( 2, 'blue' );
        c4.setBackground( 'aqua' );

        sc1.layout.doLayout( LayoutBox.UNDEFINED );

        sc1.layout.setPosition( 50.px(), 50.px() );
        sc1.layout.doLayout( LayoutBox.UNDEFINED );
    }

    public static function hbox( container : js.html.DivElement ) {
        var g = new SvgGraphics( container );

        var root = new Container( g );
        // var root = new StackingContainer( g );
        root.layout.setPositionAndSize( 20.px(), 20.px(), 900.px(), 500.px() );
        root.setBackground( 'teal' );

        var hbox = new HBox( g, root );
        // hbox.layout.setPositionAndSize( 50.px(), 50.px(), 600.px(), 350.px() );
        hbox.layout.setPositionAndSize( 25.px(), 50.px(), 600.px(), 350.px() );
        hbox.container.margins = 20;
        hbox.container.gaps = 10;
        // hbox.container.verticalAlignment = Start;
        hbox.container.verticalAlignment = Center;
        // hbox.container.verticalAlignment = End;
        hbox.setBorder( 2, 'blue' );
        hbox.setBackground( 'gray' );

        var c1 = new Container( g, hbox );
        c1.layout.setSize( 150.px(), 150.px() );
        c1.setBorder( 2, 'blue' );
        c1.setBackground( 'orange' );

        var c2 = new Container( g, hbox );
        c2.layout.setSize( 100.px(), 100.px() );
        c2.setBorder( 2, 'blue' );
        c2.setBackground( 'maroon' );

        var c3 = new Container( g, hbox );
        c3.layout.setSize( 50.px(), 50.px() );
        c3.setBorder( 2, 'blue' );
        c3.setBackground( 'cornfowerblue' );

        var c4 = new Shape( g, new Rectangle( g, 25, 25 ), hbox );
        c4.setStroke( 'blue', 2 );
        c4.setFill( 'aqua' );

        var c5 = new Shape( g, new Rectangle( g, 25, 25 ), hbox );
        c5.layout.setSize( 50.px(), SizeValue.AUTO );
        c5.setStroke( 'blue', 2 );
        c5.setFill( 'purple' );


        root.layout.doLayout( LayoutBox.UNDEFINED );
        // hbox.layout.doLayout( LayoutBox.UNDEFINED );
    }

    public static function vbox( container : js.html.DivElement ) {
        var g = new SvgGraphics( container );

        var root = new Container( g );
        root.layout.setPositionAndSize( 20.px(), 20.px(), 500.px(), 600.px() );
        root.setBackground( 'teal' );

        var vbox = new VBox( g, root );
        // vbox.layout.setPositionAndSize( 50.px(), 50.px(), 600.px(), 350.px() );
        // vbox.layout.setPositionAndSize( 25.px(), 50.px(), SizeValue.UNDEFINED, 350.px() );
        vbox.layout.setPosition( 25.px(), 50.px() );
        vbox.container.margins = 20;
        vbox.container.gaps = 10;
        vbox.container.verticalAlignment = Start;
        // vbox.container.verticalAlignment = Center;
        // vbox.container.verticalAlignment = End;
        vbox.setBorder( 2, 'blue' );
        vbox.setBackground( 'gray' );

        var c1 = new Container( g, vbox );
        c1.layout.setSize( 250.px(), 75.px() );
        c1.setBorder( 2, 'blue' );
        c1.setBackground( 'orange' );

        // var sc2 = new StackingContainer( g, vbox );
        // sc2.layout.setSize( 100.percent(), 120.px() );

        // var c2 = new HBox( g, sc2 );
        // c2.layout.setSize( 100.percent(), 100.percent() );
        var c2 = new HBox( g, vbox );
        c2.layout.setSize( 100.percent(), 120.px() );
        c2.container.margins = 20;
        c2.container.gaps = 10;
        c2.container.verticalAlignment = Start;
        c2.setBorder( 2, 'blue' );
        c2.setBackground( 'maroon' );


        root.layout.doLayout( LayoutBox.UNDEFINED );
        // vbox.layout.doLayout( LayoutBox.UNDEFINED );
    }

    public static function group( container : js.html.DivElement ) {
        var g = new SvgGraphics( container );

        var grp = new Group( g );

        var ch1 = new Shape( g, new Rectangle( g, 0, 0 ) );
        ch1.layout.setPosition( 100.px(), 120.px() );
        ch1.layout.setSize( 100.px(), 120.px() );
        ch1.setFill( NamedColor.bisque );
        ch1.setParent( grp );

        var ch2 = new Shape( g, new Rectangle( g, 0, 0 ) );
        ch2.layout.setPosition( 250.px(), 120.px() );
        ch2.layout.setSize( 100.px(), 120.px() );
        ch2.setFill( NamedColor.orangered );
        ch2.setParent( grp );

        grp.layout.setPosition( 150.px(), 150.px() );

        grp.layout.doLayout( LayoutBox.UNDEFINED );
    }
}
