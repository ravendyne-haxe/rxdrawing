// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

class ClearDemo {
    public static function run( container : js.html.DivElement ) {
        var c = container;
        while( c.firstChild != null ) {
            c.removeChild( c.firstChild );
        }
    }
}
