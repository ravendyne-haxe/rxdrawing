// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxdrawing.components.controls.toggle.CheckBox;
import rxdrawing.components.controls.toggle.Radio;
import rxdrawing.components.controls.toggle.Button;
import rxdrawing.components.Label;
import rxdrawing.components.containers.VBox;
import rxlayout.types.LayoutBox;
import rxsvggraphics.SvgGraphics;
import rxlayout.types.SizeValue;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;

class ControlsDemo {
    public static function run( container : js.html.DivElement ) {
        var g = new SvgGraphics( container );

        var vb = new VBox( g, 10 );
        vb.container.margins = 10;
        vb.setBackground( 'gray' );
        vb.layout.setPosition( 50.px(), 50.px() );

        var lbl = new Label( g, "Label", vb );
        lbl.setFill( 'cyan' );
        lbl.setFontFamily('sans-serif');
        lbl.setFontSize(40);

        var btn = new Button( g, { label: "Click me", labelColour: 'orange', iconUrl: 'bps-logo.png' }, vb );
        btn.background = 'maroon';
        // btn.layout.setSize( 100.percent(), 80.px() );
        // btn.layout.setSize( 200.px(), 80.px() );
        // btn.layout.setSize( SizeValue.AUTO, 80.px() );
        btn.onclick = (e) -> {
            trace('click!');
            trace(e);
            trace(btn.isToggled);
        }

        var ck1 = new CheckBox( g, { label: "check box" }, vb );
        ck1.colour = 'maroon';
        ck1.colourHover = 'orange';
        ck1.colourPressed = 'teal';
        // setting both ck1 and rd1 sie to (100.percent(), SizeValue.UNDEFINED)
        // will show case where button/check-ctrl size collapses below
        // icon + label size
        // ck1.layout.setSize( 100.percent(), SizeValue.UNDEFINED );
        // ck1.layout.setSize( 200.px(), 80.px() );
        // ck1.layout.setSize( SizeValue.AUTO, 80.px() );
        ck1.onclick = (e) -> {
            trace('click!');
            trace(e);
            trace(ck1.isToggled);
        }

        var rd1 = new Radio( g, { label: "radio toggle" }, vb );
        rd1.colour = 'maroon';
        rd1.colourHover = 'orange';
        rd1.colourPressed = 'teal';
        // rd1.layout.setSize( 100.percent(), SizeValue.UNDEFINED );
        // rd1.layout.setSize( 100.percent(), 80.px() );
        // rd1.layout.setSize( 200.px(), 80.px() );
        // rd1.layout.setSize( SizeValue.AUTO, 80.px() );
        rd1.onclick = (e) -> {
            trace('click!');
            trace(e);
            trace(rd1.isToggled);
        }

        vb.layout.doLayout( LayoutBox.UNDEFINED );
    }
}
