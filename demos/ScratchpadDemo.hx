// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxdrawing.core.graphics.NamedColor;
import rxdrawing.components.Label;
import rxdrawing.components.Shape;
import rxdrawing.components.containers.VBox;
import rxlayout.types.LayoutBox;
import rxdrawing.components.containers.StackingContainer;
import rxdrawing.components.containers.Container;
// import rxdrawing.components.containers.Group;
import rxdrawing.elements.Panel;
import rxdrawing.elements.Path;
import rxdrawing.elements.Group;
import rxdrawing.elements.Image;
import rxdrawing.elements.Text;
import rxdrawing.elements.QuadraticCurve;
import rxdrawing.elements.CubicCurve;
import rxdrawing.elements.Polygon;
import rxdrawing.elements.Polyline;
import rxdrawing.core.graphics.Graphics;
import rxdrawing.elements.Ellipse;
import rxdrawing.elements.Line;
import rxdrawing.elements.Circle;
import rxdrawing.elements.Rectangle;
import rxsvggraphics.SvgGraphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;

class ScratchpadDemo {
    public static function run( container : js.html.DivElement ) {

        // https://www.w3schools.com/colors/colors_names.asp
        // https://developer.mozilla.org/en-US/docs/Web/CSS/named-color

        var theme = new DemoGraphicsTheme();

        var g = new SvgGraphics( container );

        elements( g );

        components( g );
    }

    static function components( g : Graphics ) {
    }

    static function elements( g : Graphics ) {


        var cc_p0 : GraphicsPoint = {
            x: 400.,
            y: 100.,
        };
        var cc_p1 : GraphicsPoint = {
            x: 325.,
            y: 190.,
        };
        var cc_p2 : GraphicsPoint = {
            x: 510.,
            y: 250.,
        };
        var cc_p3 : GraphicsPoint = {
            x: 510.,
            y: 30.,
        };

        var cc1 = new CubicCurve( g, cc_p0, cc_p1, cc_p2, cc_p3 );
        trace( '===== CubicCurve' );
        // cc1.setStroke('cornflowerblue', 2);
        cc1.setStroke( NamedColor.cornflowerblue, 2);
        // cc1.setStroke(theme.primaryColor(), 2);
        trace( cc1.toString() );
        trace( cc1.getAABBox() );

        var cc2 = new CubicCurve( g, cc_p0, cc_p1, cc_p2, cc_p3 );
        trace( '===== CubicCurve' );
        cc2.setStroke('cornflowerblue', 2);
        trace( cc2.toString() );
        cc2.moveTo( 25, 25 );
        trace( cc2.toString() );
        cc2.resizeTo( 50, 50 );
        trace( cc2.toString() );
        trace( cc2.getAABBox() );




        var qc_p0 : GraphicsPoint = {
            x: 270.,
            y: 250.,
        };
        var qc_p1 : GraphicsPoint = {
            x: 230.,
            y: 60.,
        };
        var qc_p2 : GraphicsPoint = {
            x: 400.,
            y: 100.,
        };

        var qc1 = new QuadraticCurve( g, qc_p0, qc_p1, qc_p2 );
        trace( '===== QuadraticCurve' );
        qc1.setStroke('teal', 2);
        trace( qc1.toString() );
        trace( qc1.getAABBox() );

        var qc2 = new QuadraticCurve( g, qc_p0, qc_p1, qc_p2 );
        trace( '===== QuadraticCurve' );
        qc2.setStroke('teal', 2);
        trace( qc2.toString() );
        qc2.moveTo( 25, 25 );
        trace( qc2.toString() );
        qc2.resizeTo( 50, 50 );
        trace( qc2.toString() );
        trace( qc2.getAABBox() );




        var pgrp = new Group( g );
        var pth1 = new Path( g, [], pgrp );
        pth1.setStroke('red', 2);
        pth1.setAttribute( Id, 'pth1' );
        pth1
        .next( CPathMoveTo({ x: 0, y: 0 }) )
        .next( CPathLineTo({ x: 80, y: 0 }) )
        .next( CPathLineTo({ x: 80, y: 80 }) )
        .next( CPathLineTo({ x: 160, y: 80 }) )
        .next( CPathLineTo({ x: 160, y: 0 }) )
        .next( CPathLineTo({ x: 240, y: 0 }) )
        .update();
        trace( '===== Path' );
        trace( pth1.getAABBox() );

        var pth2 = new Path( g, [], pgrp );
        // var pth2 = new Path( g, [] );
        pth2.setStroke('red', 2);
        pth2.setAttribute( Id, 'pth2' );
        pth2
        .next( CPathMoveTo({ x: 0, y: 0 }) )
        .next( CPathLineTo({ x: 80, y: 0 }) )
        .next( CPathLineTo({ x: 80, y: 80 }) )
        .next( CPathLineTo({ x: 160, y: 80 }) )
        .next( CPathLineTo({ x: 160, y: 0 }) )
        .next( CPathLineTo({ x: 240, y: 0 }) )
        .update();
        trace( '===== Path' );
        trace( pth2.toString() );
        pth2.moveTo( 25, 25 );
        trace( pth2.toString() );
        pth2.resizeTo( 50, 50 );
        trace( pth2.toString() );
        trace( pth2.getAABBox() );

        pgrp.moveTo( 400, 400 );
        // pgrp.layout.setPosition( 400.px(), 400.px() );

    }
}
