// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxdrawing.components.Label;
import rxdrawing.components.Shape;
import rxdrawing.components.containers.VBox;
import rxlayout.types.LayoutBox;
import rxdrawing.components.containers.StackingContainer;
import rxdrawing.components.containers.Container;
import rxdrawing.elements.Panel;
import rxdrawing.elements.Path;
import rxdrawing.elements.Group;
import rxdrawing.elements.Image;
import rxdrawing.elements.Text;
import rxdrawing.elements.QuadraticCurve;
import rxdrawing.elements.CubicCurve;
import rxdrawing.elements.Polygon;
import rxdrawing.elements.Polyline;
import rxdrawing.core.graphics.Graphics;
import rxdrawing.elements.Ellipse;
import rxdrawing.elements.Line;
import rxdrawing.elements.Circle;
import rxdrawing.elements.Rectangle;
import rxsvggraphics.SvgGraphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;

class ElementsDemo {
    public static function run( container : js.html.DivElement ) {

        // https://www.w3schools.com/colors/colors_names.asp
        // https://developer.mozilla.org/en-US/docs/Web/CSS/named-color

        var g = new SvgGraphics( container );

        var rc1 = new Rectangle( g, 100, 200 );
        rc1.setOrigin( 75, 50 );
        rc1.setFill('green');


        var cc1 = new Circle( g, 50, 75, 50 );
        cc1.setFill('orange');


        var ln1 = new Line( g, 75, 250, 175, 325 );
        ln1.setFill('orange');


        var el1 = new Ellipse( g, 100, 50, 300, 300 );
        // el1.setFill('#222422');
        el1.setFill('purple');


        var points : Array<Float> = [
            0,375,
            100,375, 100,325, 200,325, 200,375,
            300,375, 300,250, 400,250, 400,375,
            500,375, 500,175, 600,175, 600,375,
            700,375, 700,100, 800,100, 800,375,
            900,375, 900,25, 1000,25, 1000,375,
            1100,375
        ];
        var pl1 = new Polyline( g, points.toGraphicsPointArray() );
        pl1.setStroke('blue', 2);
        pl1.setOrigin( 50, 25 );


        var points : Array<Float> = [
            850,75,  958,137.5, 958,262.5,
            850,325, 742,262.6, 742,137.5,
        ];
        var pl2 = new Polygon( g, points.toGraphicsPointArray() );
        pl2.setStroke('royalblue', 2);
        pl2.setFill('lime');


        var cc1 = new CubicCurve( g, {
            x: 400.,
            y: 100.,
        },{
            x: 325.,
            y: 190.,
        },{
            x: 510.,
            y: 250.,
        },{
            x: 510.,
            y: 30.,
        });
        cc1.setStroke('cornflowerblue', 2);


        var qc1 = new QuadraticCurve( g, {
            x: 270.,
            y: 250.,
        },{
            x: 230.,
            y: 60.,
        },{
            x: 400.,
            y: 100.,
        });
        qc1.setStroke('teal', 2);


        var txt1 = new Text( g,  'Hello there!' );
        txt1.setOrigin( 200, 200 );
        txt1.setFill('darkorchid');
        txt1.setFontSize( 40 );
        txt1.setFontStyle( 'italic' );
        // txt1.setStroke('teal');


        var img1 = new Image( g,  'bps-logo.png', 120, 120 );
        // img1.setStroke('teal');
        img1.setOrigin( 100, 400 );


        var grp1 = new Group( g );
        grp1.moveTo( 100, 500 );
        var grc1 = new Rectangle( g, 50, 50, grp1 );
        grc1.setFill('coral');
        grc1.setOrigin( 10, 10 );
        var grc2 = new Rectangle( g, 50, 50, grp1 );
        grc2.setFill('cornflowerblue');
        grc2.setOrigin( 70, 10 );


        var pth1 = new Path( g, [] );
        pth1.setStroke('red', 2);
        pth1
        .next( CPathMoveTo({ x: 0, y: 0 }) )
        .next( CPathLineTo({ x: 80, y: 0 }) )
        .next( CPathLineTo({ x: 80, y: 80 }) )
        .next( CPathLineTo({ x: 160, y: 80 }) )
        .next( CPathLineTo({ x: 160, y: 0 }) )
        .next( CPathLineTo({ x: 240, y: 0 }) )
        .update();
        pth1.setOrigin( 400, 400 );

        var pnls = panelsDemo( g );
        pnls.setOrigin( 250, 450 );
    }

    public static function panelsDemo( g : Graphics ) {

        var x = 0.;
        var y = 0.;
        var width = 64.;
        var height = 96.;
        var gap = 8.;
        var rx = 16.;
        var ry = 16.;
        var curved = false;

        var makePanel = ( p, i, j, options ) -> {
            var panel = new Panel( g, width, height, options, p );
            panel.setAttributes([
                Stroke=>'blue',
                StrokeWidth=>'2',
                Fill=>'orange',
            ]);
            panel.setOrigin( x + i * (width + gap), y + j * (height + gap) );
            return panel;
        }

        var grp = new Group( g );
        makePanel( grp, 0, 0, { rx:rx, ry:ry, tl:true, tr:true, br:true, bl:true, curved:curved } );
        makePanel( grp, 1, 0, { rx:rx, ry:ry, tl:false, tr:true, br:true, bl:true, curved:curved } );
        makePanel( grp, 2, 0, { rx:rx, ry:ry, tl:true, tr:false, br:true, bl:true, curved:curved } );
        makePanel( grp, 3, 0, { rx:rx, ry:ry, tl:true, tr:true, br:false, bl:true, curved:curved } );
        makePanel( grp, 4, 0, { rx:rx, ry:ry, tl:true, tr:true, br:true, bl:false, curved:curved } );

        makePanel( grp, 0, 1, { rx:rx, ry:ry, tl:false, tr:false, br:false, bl:false, curved:curved } );
        makePanel( grp, 1, 1, { rx:rx, ry:ry, tl:true, tr:false, br:false, bl:false, curved:curved } );
        makePanel( grp, 2, 1, { rx:rx, ry:ry, tl:false, tr:true, br:false, bl:false, curved:curved } );
        makePanel( grp, 3, 1, { rx:rx, ry:ry, tl:false, tr:false, br:true, bl:false, curved:curved } );
        makePanel( grp, 4, 1, { rx:rx, ry:ry, tl:false, tr:false, br:false, bl:true, curved:curved } );

        makePanel( grp, 0, 2, { rx:rx, ry:ry, tl:false, tr:true, br:false, bl:true, curved:curved } );
        makePanel( grp, 1, 2, { rx:rx, ry:ry, tl:true, tr:false, br:true, bl:false, curved:curved } );

        return grp;
    }
}
