// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

typedef DemoFunction = ( container : js.html.DivElement ) -> Void;

typedef DemoEntry = {
    var label: String;
    var run: DemoFunction;
}

@:keep
@:expose
final class DemosService {
    private function new() {}

    public static var onLoad : DemoFunction = ContainersDemo.vbox;
    // public static var onLoad : DemoFunction = ScratchpadDemo.run;
    public static var clear: DemoFunction = ClearDemo.run;

    public static var demos : Array<DemoEntry> = [
        {
            label: 'Clear',
            run: ClearDemo.run,
        },
        {
            label: 'Scratchpad',
            run: ScratchpadDemo.run,
        },
        {
            label: 'Elements',
            run: ElementsDemo.run,
        },
        {
            label: 'Containers - Stacking',
            run: ContainersDemo.stacking,
        },
        {
            label: 'Containers - HBox',
            run: ContainersDemo.hbox,
        },
        {
            label: 'Containers - VBox',
            run: ContainersDemo.vbox,
        },
        {
            label: 'Containers - Group',
            run: ContainersDemo.group,
        },
        {
            label: 'Controls',
            run: ControlsDemo.run,
        },
        {
            label: 'Shape',
            run: ShapeDemo.run,
        },
    ];

}
