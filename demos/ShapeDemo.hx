// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxdrawing.components.Label;
import rxdrawing.components.Shape;
import rxdrawing.components.containers.VBox;
import rxlayout.types.LayoutBox;
import rxdrawing.components.containers.StackingContainer;
import rxdrawing.components.containers.Container;
import rxdrawing.elements.Panel;
import rxdrawing.elements.Path;
import rxdrawing.elements.Group;
import rxdrawing.elements.Image;
import rxdrawing.elements.Text;
import rxdrawing.elements.QuadraticCurve;
import rxdrawing.elements.CubicCurve;
import rxdrawing.elements.Polygon;
import rxdrawing.elements.Polyline;
import rxdrawing.core.graphics.Graphics;
import rxdrawing.elements.Ellipse;
import rxdrawing.elements.Line;
import rxdrawing.elements.Circle;
import rxdrawing.elements.Rectangle;
import rxsvggraphics.SvgGraphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;

class ShapeDemo {
    public static function run( container : js.html.DivElement ) {
        var g = new SvgGraphics( container );

        var vb = new VBox( g, 10 );
        vb.container.margins = 10;
        vb.setBackground( 'gray' );
        vb.layout.setPosition( 50.px(), 50.px() );

        var shp = new Shape( g, new Circle( g, 30 ), vb );
        shp.setFill( 'cyan' );

        // shp = new Shape( g, new CubicCurve( g ), vb );
        // shp.setFill( 'aqua' );
        // shp.setStroke( 'orange', 2 );

        // shp = new Shape( g, new Ellipse( g, 30, 15 ), vb );
        // shp.setFill( 'cyan' );

        shp = new Shape( g, new Image( g, 'bps-logo.png', 48, 48 ), vb );
        shp.setStroke( 'black', 1 );

        // shp = new Shape( g, new Text( g ), vb );
        // shp.setFill( 'aqua' );
        // shp.setStroke( 'orange', 2 );

        // shp = new Shape( g, new Line( g, 10, 10, 30, 15 ), vb );
        shp = new Shape( g, new Line( g, 0, 0, 30, 15 ), vb );
        shp.setStroke( 'blue', 2 );

        shp = new Shape( g, new Panel( g, 70, 35 ), vb );
        shp.setFill( 'maroon' );
        shp.setStroke( 'orange', 2 );

        // shp = new Shape( g, new Path( g, [] ), vb );
        // shp.setStroke( 'red', 2 );

        shp = new Shape( g, new Polygon( g, [0., 0, 30, 15, 0, 30].toGraphicsPointArray() ), vb );
        shp.setStroke( 'red', 2 );
        shp.setFill('none');

        shp = new Shape( g, new Polyline( g, [0., 0, 30, 15, 0, 30].toGraphicsPointArray() ), vb );
        shp.setStroke( 'purple', 2 );
        shp.setFill('none');

        // shp = new Shape( g, new QuadraticCurve( g ), vb );
        // shp.setFill( 'aqua' );
        // shp.setStroke( 'orange', 2 );

        shp = new Shape( g, new Rectangle( g, 90, 25 ), vb );
        shp.setFill( 'aqua' );
        shp.setStroke( 'orange', 2 );

        shp = new Shape( g, new Text( g, "TExT" ), vb );
        shp.setFill( 'aqua' );
        // shp.setStroke( 'orange', 2 );

        vb.layout.doLayout( LayoutBox.UNDEFINED );
    }
}
