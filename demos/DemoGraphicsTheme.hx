// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxdrawing.core.graphics.NamedColor;
import rxdrawing.core.graphics.RGBA8888Color;
import rxdrawing.core.graphics.theme.MaterialPalette;
import rxdrawing.core.graphics.theme.GraphicsTheme;


class DemoGraphicsTheme implements GraphicsTheme {
    public function new(){}

    // Blue Gray
    public var primaryPalette: Map<String,RGBA8888Color> = MaterialPalette.Blue_Gray;
    public var primaryPaletteContrast: Map<String,RGBA8888Color> = [
        '50'  =>NamedColor.black,
        '100' =>NamedColor.black,
        '200' =>NamedColor.black,
        '300' =>NamedColor.black,
        '400' =>NamedColor.black,
        '500' =>NamedColor.black,
        '600' =>NamedColor.white,
        '700' =>NamedColor.white,
        '800' =>NamedColor.white,
        '900' =>NamedColor.white,
        'A100'=>NamedColor.black,
        'A200'=>NamedColor.black,
        'A400'=>NamedColor.black,
        'A700'=>NamedColor.black,
    ];

    // Gray
    public var secondaryPalette: Map<String,RGBA8888Color> = MaterialPalette.Gray;
    public var secondaryPaletteContrast: Map<String,RGBA8888Color> = [
        '50'  =>NamedColor.white,
        '100' =>NamedColor.white,
        '200' =>NamedColor.white,
        '300' =>NamedColor.white,
        '400' =>NamedColor.white,
        '500' =>NamedColor.white,
        '600' =>NamedColor.white,
        '700' =>NamedColor.white,
        '800' =>NamedColor.white,
        '900' =>NamedColor.white,
        'A100'=>NamedColor.white,
        'A200'=>NamedColor.white,
        'A400'=>NamedColor.white,
        'A700'=>NamedColor.white,
    ];

    public var primary : String = '500';
    public var primaryLight : String = '200';
    public var primaryDark : String = '700';
    public var primaryVariant : String = '700';

    public var secondary : String = '500';
    public var secondaryLight : String = '200';
    public var secondaryDark : String = '700';
    public var secondaryVariant : String = '700';

    public function primaryColor() { return primaryPalette[ primary ]; }

    public var backgroundColor : RGBA8888Color = NamedColor.white;
    public var surfaceColor : RGBA8888Color = NamedColor.white;
    public var errorColor : RGBA8888Color = 0xb00020ff;

    public var onPrimaryColor : RGBA8888Color = NamedColor.white;
    public var onSecondaryColor : RGBA8888Color = NamedColor.black;
    public var onBackgroundColor : RGBA8888Color = NamedColor.black;
    public var onSurfaceColor : RGBA8888Color = NamedColor.black;
    public var onErrorColor : RGBA8888Color = NamedColor.white;
}
