// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

// import utest.Runner;
// import utest.ui.Report;
import utest.UTest;


class TestsMain {

    public static function main() {

        new TestGraphics(0,0);

        UTest.run([
            new TestRGBA8888Color(),
            new TestRxDrawingTools(),
        ]);
    }
}
