// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;


import utest.Assert;
import utest.Test;

import rxdrawing.core.graphics.RGBA8888Color;
import rxdrawing.core.graphics.NamedColor;

using rxdrawing.RxDrawingTools;


class TestRGBA8888Color extends Test {

    function testFromCSShex() {
        var c : RGBA8888Color;

        c = '';
        Assert.equals( NamedColor.black.toInt(), c.toInt() );

        c = '#';
        Assert.equals( NamedColor.black.toInt(), c.toInt() );

        c = '#1';
        Assert.equals( NamedColor.black.toInt(), c.toInt() );

        c = '#12345';
        Assert.equals( NamedColor.black.toInt(), c.toInt() );

        c = '#123456789';
        Assert.equals( NamedColor.black.toInt(), c.toInt() );

        c = 'aabbccff';
        Assert.equals( NamedColor.black.toInt(), c.toInt() );

        c = '#abc';
        Assert.equals( 0xaa, c.ri );
        Assert.equals( 0xbb, c.gi );
        Assert.equals( 0xcc, c.bi );
        Assert.equals( 0xff, c.ai );

        c = '#abcd';
        Assert.equals( 0xaa, c.ri );
        Assert.equals( 0xbb, c.gi );
        Assert.equals( 0xcc, c.bi );
        Assert.equals( 0xdd, c.ai );

        c = '#a1b1c1';
        Assert.equals( 0xa1, c.ri );
        Assert.equals( 0xb1, c.gi );
        Assert.equals( 0xc1, c.bi );
        Assert.equals( 0xff, c.ai );

        c = '#a2b2c2d2';
        Assert.equals( 0xa2, c.ri );
        Assert.equals( 0xb2, c.gi );
        Assert.equals( 0xc2, c.bi );
        Assert.equals( 0xd2, c.ai );
    }
}
