// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

@:structInit class TestElement implements GraphicsElement {
    public var tag : String;
    public var attrs : Map<String,String>;
    public var parent : TestElement;
    public var children : Array<TestElement>;

    public var origin : GraphicsPoint;
    public var aabb : GraphicsBox;
    public var transforms : Array<GraphicsTransformOperation>;

    // events
    public var eventsMouseUp : Array<GraphicsMouseEvent -> Void>;
    public var eventsMouseDown : Array<GraphicsMouseEvent -> Void>;
    public var eventsMouseEnter : Array<GraphicsMouseEvent -> Void>;
    public var eventsMouseLeave : Array<GraphicsMouseEvent -> Void>;
    public var eventsMouseClick : Array<GraphicsMouseEvent -> Void>;
    public var eventsMouseMove : Array<GraphicsMouseEvent -> Void>;
    public var eventsMouseWheel : Array<GraphicsWheelEvent -> Void>;

    public var events : Map< String, Array<GraphicsWheelEvent -> Void> >;
    public var eventsCustom : Map< String, Array<Dynamic -> Void> >;
}

class TestGraphics implements Graphics {

    var _width : Float;
    var _height : Float;

    public function new( width : Float, height : Float ) {
        _width = width;
        _height = height;
    }

    function createTestElement( tag : String ) : GraphicsElement {
        var te : TestElement = {
            tag: tag,
            attrs: [],
            parent: null,
            children: [],

            origin: {x:0., y:0.},
            aabb: {x:0., y:0., width: 0., height: 0.},
            transforms: [],

            eventsMouseUp: [],
            eventsMouseDown: [],
            eventsMouseEnter: [],
            eventsMouseLeave: [],
            eventsMouseClick: [],
            eventsMouseMove: [],
            eventsMouseWheel: [],

            events: [],
            eventsCustom: [],
        }
        return te;
    }

    function castAsTestElement( e : GraphicsElement ) : TestElement {
        return cast e;
    }

    function getAttributeName( ga : GraphicsAttribute ) : String {
        switch( ga ) {
            case Id: return 'id';

            case Stroke: return 'stroke';
            case StrokeWidth: return 'stroke-width';
            case StrokeOpacity: return 'stroke-opacity';
            case Fill: return 'fill';
            case FillOpacity: return 'fill-opacity';

            case Opacity: return 'opacity';
            case Visible: return 'visible';

            case FontFamily: return 'font-family';
            case FontSize: return 'font-size';
            case FontStyle: return 'font-style';
            case FontWeight: return 'font-weight';

            case Custom(name): return name;
        }
    }

    function getGraphicsMouseEventName( en : GraphicsEventName ) : String {
        switch( en ) {
            case MouseUp: return 'mouseup';
            case MouseDown: return 'mousedown';
            case MouseEnter: return 'mouseenter';
            case MouseLeave: return 'mouseleave';
            case MouseClick: return 'click';
            case MouseMove: return 'mousemove';
            case MouseWheel: return 'wheel';

            case Custom(evtName): return evtName;
        }
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // ATTRIBUTES
    //
    /////////////////////////////////////////////////////////////////////////////
    public function setAttribute( el : GraphicsElement, attr : GraphicsAttribute, value : String ) : Void {
        castAsTestElement(el).attrs.set( getAttributeName( attr ), value );
    }

    public function setAttributes( el : GraphicsElement, attrs : Map<GraphicsAttribute, String> ) : Void {
        for( attr=>value in attrs.keyValueIterator() ) setAttribute( el, attr, value );
    }

    public function getAttribute( el : GraphicsElement, attr : GraphicsAttribute ) : String {
        return castAsTestElement(el).attrs.get( getAttributeName( attr ) );
    }

    public function removeAttribute( el : GraphicsElement, attr : GraphicsAttribute ) : Void {
        castAsTestElement(el).attrs.remove( getAttributeName( attr ) );
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // CREATE
    //
    /////////////////////////////////////////////////////////////////////////////

    public function createPath( pi : Array<PathInstruction> ) : GraphicsElement {
        return createTestElement('path');
    }

    public function createCircle( cx : Float, cy : Float, r : Float ) : GraphicsElement {
        return createTestElement('circle');
    }

    public function createEllipse( cx : Float, cy : Float, rx : Float, ry : Float ) : GraphicsElement {
        return createTestElement('ellipse');
    }

    public function createGroup( children : Array<GraphicsElement> ) : GraphicsElement {
        return createTestElement('group');
    }

    public function createRect( x : Float, y : Float, width : Float, height : Float, ?rx : Float, ?ry : Float ) : GraphicsElement {
        return createTestElement('rect');
    }

    public function createLine( x1 : Float, y1 : Float, x2 : Float, y2 : Float ) : GraphicsElement {
        return createTestElement('line');
    }

     public function createCubicCurve( p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint, p3 : GraphicsPoint ) : GraphicsElement {
        return createTestElement('cubic-curve');
    }

    public function updatePath( c : GraphicsElement, pi : Array<PathInstruction> ) : Void {
    }

    public function updateCubicCurve( c : GraphicsElement, p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint, p3 : GraphicsPoint ) : Void {
    }

    public function createQuadraticCurve( p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint ) : GraphicsElement {
        return createTestElement('quadratic-curve');
    }

    public function createText( text : String, ?x : Float, ?y : Float ) : GraphicsElement {
        return createTestElement('text');
    }

    public function createImage( href : String, ?width : Float, ?height : Float, ?x : Float, ?y : Float ) : GraphicsElement {
        return createTestElement('image');
    }

    public function updateQuadraticCurve( c : GraphicsElement, p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint ) : Void {
    }

    public function updatePanel( path : GraphicsElement,  x : Float, y : Float, width : Float, height : Float, ?options : PanelOptions ) : Void {
    }

    public function createPanel( width : Float, height : Float, ?x : Float, ?y : Float, ?options : PanelOptions ) : GraphicsElement {
        return createTestElement('panel');
    }

    public function updateText( el : GraphicsElement, text : String, ?x : Float, ?y : Float ) : Void {
    }

    public function updatCircle( el : GraphicsElement, cx : Float, cy : Float, r : Float ) : Void {
    }

    public function updateEllipse( el : GraphicsElement,cx : Float, cy : Float, rx : Float, ry : Float ) : Void {
    }

    public function updateRect( el : GraphicsElement, x : Float, y : Float, width : Float, height : Float, ?rx : Float, ?ry : Float ) : Void {
    }

    public function updateLine( el : GraphicsElement, x1 : Float, y1 : Float, x2 : Float, y2 : Float ) : Void {
    }

    public function updateImage( el : GraphicsElement, href : String, ?width : Float, ?height : Float, ?x : Float, ?y : Float ) : Void {
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // EVENTS
    //
    /////////////////////////////////////////////////////////////////////////////
    public function addWheelEventListener( el : GraphicsElement, handler: GraphicsWheelEvent -> Void ) : GraphicsEventHnd {
        return null;
    }
    public function dispatchWheelEvent( el : GraphicsElement, event: GraphicsWheelEvent ) : Void {
    }

    public function addMouseEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: GraphicsMouseEvent -> Void ) : GraphicsEventHnd {
        return null;
    }
    public function dispatchMouseEvent( el : GraphicsElement, evtName : GraphicsEventName, event: GraphicsMouseEvent ) : Void {}

    public function addEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: GraphicsEvent -> Void ) : GraphicsEventHnd {
        return null;
    }
    public function dispatchEvent( el : GraphicsElement, evtName : GraphicsEventName, event: GraphicsEvent ) : Void {}

    public function addCustomEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: Dynamic -> Void ) : GraphicsEventHnd {
        return null;
    }
    public function dispatchCustomEvent( el : GraphicsElement, evtName : GraphicsEventName, event: Dynamic ) : Void {}

    public function removeEventListener( el : GraphicsElement, evtName : GraphicsEventName, handle: GraphicsEventHnd ) : Void {
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // STRUCTURE
    //
    /////////////////////////////////////////////////////////////////////////////
    public function appendChild( el : GraphicsElement, ?c : GraphicsElement ) : Void {
    }

    public function removeChild( el : GraphicsElement, ?c : GraphicsElement ) : Void {
    }

    public function moveToFront( el : GraphicsElement, ?c : GraphicsElement ) : Void {
    }

    public function moveToBack( el : GraphicsElement, ?c : GraphicsElement ) : Void {
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // MISC
    //
    /////////////////////////////////////////////////////////////////////////////
    public function setOrigin( el : GraphicsElement, x : Float, y : Float ) : Void {
    }
    public function setScale( el : GraphicsElement, sx : Float, sy : Float ) : Void {
    }
    public function setRotation( el : GraphicsElement, angle : Float ) : Void {
    }
    public function transform( el : GraphicsElement, ops : Array<GraphicsTransformOperation> ) {
        throw new haxe.Exception('Graphics.transofrm not implemented');
    }
    public function clearTransform( el : GraphicsElement ) : Void {
    }

    /** convert point in client coordinate system (i.e. html body element) to this SVG user coord system */
    public function clientPointToUser( x : Float, y : Float ) : GraphicsPoint {
        return {
            x: x,
            y: y,
        }
    }

    public function getElementBox( el : GraphicsElement ) : GraphicsBox {
        return castAsTestElement( el ).aabb;
        // return {
        //     x: 0.0,
        //     y: 0.0,
        //     width: 0.0,
        //     height: 0.0,
        // }
    }

    public function getElementFromPoint( x : Float, y : Float ) : GraphicsElement {
        return null;
    }

    public function getElementsFromPoint( x : Float, y : Float ) : Array<GraphicsElement> {
        return [];
    }
}
