// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import utest.Assert;
import utest.Test;

import rxdrawing.core.graphics.Graphics.GraphicsPoint;
import rxdrawing.core.graphics.Graphics.PathInstruction;
import rxsvggraphics.SvgPathBuilder;

using rxdrawing.RxDrawingTools;


class TestSvgPathBuilder extends Test {

    private function convertToPathInstructions( points : Array<GraphicsPoint> ) {
        if( points.length < 2 ) return [];
        var r : Array<PathInstruction>  = [];
        var first = true;
        for( pt in points ) {
            if( first ) {
                first = false;
                r.push( PathInstruction.CPathMoveTo(pt) );
            } else {
                r.push( PathInstruction.CPathLineTo(pt) );
            }
        }
        return r;
    }

    function testBuild() {

        var points : Array<Float> = [
            1000,375,
            1000,375
        ];

        var d = new SvgPathBuilder( convertToPathInstructions( points.toGraphicsPointArray() ) ).build();
        // trace(d);
        Assert.notNull( d );
        // Assert.equals( 'M1000.0000,375.0000 L1000.0000,375.0000', d );
        Assert.equals( 'M1000,375 L1000,375', d );
    }
}
