// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

// import utest.Runner;
// import utest.ui.Report;
import utest.UTest;


class TestsMain {

    public static function main() {

        UTest.run([
            new TestSvgPathBuilder(),
        ]);
    }
}
