// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing;

import rxdrawing.core.graphics.Graphics;

using StringTools;

final class RxDrawingTools {
    private function new(){}

    static var _nextId : Int = 0;
    public static function nextId() : Int { _nextId++; return _nextId; }

    public static function translatePath( path : Array<PathInstruction>, translation : GraphicsPoint ) : Array<PathInstruction> {
        if( translation == null ) translation = { x: 0.0, y: 0.0 };
        var tx = isNumber( translation.x ) ? translation.x : 0.0;
        var ty = isNumber( translation.y ) ? translation.y : 0.0;


        final translateAPoint : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return {
                x: pt.x + tx,
                y: pt.y + ty,
            }
        }

        var translated : Array<PathInstruction> = [];

        for( pi in path ) {
            switch( pi ) {
                case CPathMoveTo(p): translated.push( CPathMoveTo( translateAPoint( p ) ) );
                case CPathLineTo(p): translated.push( CPathLineTo( translateAPoint( p ) ) );
                case CPathQuadraticCurveTo(p2, p3): translated.push( CPathQuadraticCurveTo( translateAPoint( p2 ), translateAPoint( p3 ) ) );
                case CPathCubicCurveTo(p2, p3, p4): translated.push( CPathCubicCurveTo( translateAPoint( p2 ), translateAPoint( p3 ), translateAPoint( p4 ) ) );
                case CPathEllipticArcTo(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2): translated.push( CPathEllipticArcTo( rx, ry, xAxisRotation, largeArcFlag, sweepFlag, translateAPoint( p2 ) ) );

                case CPathMoveToRelative(p): translated.push( CPathMoveToRelative( clonep( p ) ) );
                case CPathLineToRelative(p): translated.push( CPathLineToRelative( clonep( p ) ) );
                case CPathQuadraticCurveToRelative(p2, p3): translated.push( CPathQuadraticCurveToRelative( clonep( p2 ), clonep( p3 ) ) );
                case CPathCubicCurveToRelative(p2, p3, p4): translated.push( CPathCubicCurveToRelative( clonep( p2 ), clonep( p3 ), clonep( p4 ) ) );
                case CPathEllipticArcToRelative(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2): translated.push( CPathEllipticArcToRelative( rx, ry, xAxisRotation, largeArcFlag, sweepFlag, clonep( p2 ) ) );

                case CPathClosePath: translated.push( CPathClosePath );
            }
        }

        return translated;
    }

    public static function scalePath( path : Array<PathInstruction>, scale : GraphicsPoint, ?translation : GraphicsPoint ) : Array<PathInstruction> {
        if( scale == null ) scale = { x: 1.0, y: 1.0 };
        if( translation == null ) translation = { x: 0.0, y: 0.0 };
        var sx = isNumber( scale.x ) ? scale.x : 1.0;
        var sy = isNumber( scale.y ) ? scale.y : 1.0;
        var tx = isNumber( translation.x ) ? translation.x : 0.0;
        var ty = isNumber( translation.y ) ? translation.y : 0.0;


        final scaleAPoint : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return {
                x: ( pt.x - tx ) * sx + tx,
                y: ( pt.y - ty ) * sy + ty,
            }
        }
        final scaleAPointRelative : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return {
                x: pt.x * sx,
                y: pt.y * sy,
            }
        }

        var scaled : Array<PathInstruction> = [];

        for( pi in path ) {
            switch( pi ) {
                case CPathMoveTo(p): scaled.push( CPathMoveTo( scaleAPoint( p ) ) );
                case CPathLineTo(p): scaled.push( CPathLineTo( scaleAPoint( p ) ) );
                case CPathQuadraticCurveTo(p2, p3): scaled.push( CPathQuadraticCurveTo( scaleAPoint( p2 ), scaleAPoint( p3 ) ) );
                case CPathCubicCurveTo(p2, p3, p4): scaled.push( CPathCubicCurveTo( scaleAPoint( p2 ), scaleAPoint( p3 ), scaleAPoint( p4 ) ) );
                case CPathEllipticArcTo(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2): scaled.push( CPathEllipticArcTo( rx * sx, ry * sy, xAxisRotation, largeArcFlag, sweepFlag, scaleAPoint( p2 ) ) );

                case CPathMoveToRelative(p): scaled.push( CPathMoveToRelative( scaleAPointRelative( p ) ) );
                case CPathLineToRelative(p): scaled.push( CPathLineToRelative( scaleAPointRelative( p ) ) );
                case CPathQuadraticCurveToRelative(p2, p3): scaled.push( CPathQuadraticCurveToRelative( scaleAPointRelative( p2 ), scaleAPointRelative( p3 ) ) );
                case CPathCubicCurveToRelative(p2, p3, p4): scaled.push( CPathCubicCurveToRelative( scaleAPointRelative( p2 ), scaleAPointRelative( p3 ), scaleAPointRelative( p4 ) ) );
                case CPathEllipticArcToRelative(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2): scaled.push( CPathEllipticArcToRelative( rx * sx, ry * sy, xAxisRotation, largeArcFlag, sweepFlag, scaleAPointRelative( p2 ) ) );

                case CPathClosePath: scaled.push( CPathClosePath );
            }
        }

        return scaled;
    }

    public static function rotatePath( path : Array<PathInstruction>, angle : Float, ?origin : GraphicsPoint ) : Array<PathInstruction> {
        if( origin == null ) origin = { x: 0.0, y: 0.0 };
        var tx = isNumber( origin.x ) ? origin.x : 0.0;
        var ty = isNumber( origin.y ) ? origin.y : 0.0;


        final toOrigin : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return {
                x: pt.x - tx,
                y: pt.x - ty,
            }
        }
        final fromOrigin : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return {
                x: pt.x + tx,
                y: pt.x + ty,
            }
        }
        final rotate : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return {
                x: pt.x * Math.cos( angle ) - pt.y * Math.sin( angle ),
                y: pt.x * Math.sin( angle ) + pt.y * Math.cos( angle ),
            }
        }
        final rotateAPoint : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return fromOrigin( rotate( toOrigin( pt ) ) );
        }
        final rotateAPointRelative : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return rotate( pt );
        }

        var rotated : Array<PathInstruction> = [];

        for( pi in path ) {
            switch( pi ) {
                case CPathMoveTo(p): rotated.push( CPathMoveTo( rotateAPoint( p ) ) );
                case CPathLineTo(p): rotated.push( CPathLineTo( rotateAPoint( p ) ) );
                case CPathQuadraticCurveTo(p2, p3): rotated.push( CPathQuadraticCurveTo( rotateAPoint( p2 ), rotateAPoint( p3 ) ) );
                case CPathCubicCurveTo(p2, p3, p4): rotated.push( CPathCubicCurveTo( rotateAPoint( p2 ), rotateAPoint( p3 ), rotateAPoint( p4 ) ) );
                case CPathEllipticArcTo(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2): rotated.push( CPathEllipticArcTo( rx, ry, xAxisRotation, largeArcFlag, sweepFlag, rotateAPoint( p2 ) ) );

                case CPathMoveToRelative(p): rotated.push( CPathMoveToRelative( rotateAPointRelative( p ) ) );
                case CPathLineToRelative(p): rotated.push( CPathLineToRelative( rotateAPointRelative( p ) ) );
                case CPathQuadraticCurveToRelative(p2, p3): rotated.push( CPathQuadraticCurveToRelative( rotateAPointRelative( p2 ), rotateAPointRelative( p3 ) ) );
                case CPathCubicCurveToRelative(p2, p3, p4): rotated.push( CPathCubicCurveToRelative( rotateAPointRelative( p2 ), rotateAPointRelative( p3 ), rotateAPointRelative( p4 ) ) );
                case CPathEllipticArcToRelative(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2): rotated.push( CPathEllipticArcToRelative( rx, ry, xAxisRotation, largeArcFlag, sweepFlag, rotateAPointRelative( p2 ) ) );

                case CPathClosePath: rotated.push( CPathClosePath );
            }
        }

        return rotated;
    }

    public static function aabb( points : Array<GraphicsPoint> ) : GraphicsBox {
        if( points.length == 0 ) return {
            x: 0,
            y: 0,
            width: 0,
            height: 0,
        }

        var left = Math.POSITIVE_INFINITY;
        var top = Math.POSITIVE_INFINITY;
        var right = Math.NEGATIVE_INFINITY;
        var bottom = Math.NEGATIVE_INFINITY;

        for( pt in points ) {
            left = Math.min( pt.x, left );
            top = Math.min( pt.y, top );
            right = Math.max( pt.x, right );
            bottom = Math.max( pt.y, bottom );
        }

        return {
            x: left,
            y: top,
            width: right - left,
            height: bottom - top,
        }
    }

    /**
     * Converts from angular degrees to radians.
     *
     * For example, `180.deg()` returns `Float` value equal to `Math.PI`
     *
     * @param v angular degrees
     */
    public static inline function deg( v : Float ) {
        return v * Math.PI / 180.0;
    }

    public static inline function toGraphicsPointArray( values : Array<Float> ) {
        var r : Array<GraphicsPoint> = [];
        var npairs : Int = values.length >> 1;
        var idx = 0;
        for( n in 0...npairs ) {
            var x = values[ idx ]; idx++;
            var y = values[ idx ]; idx++;
            r.push({
                x: x,
                y: y,
            });
        }
        return r;
    }

    public static inline function clonep( pt : GraphicsPoint ) : GraphicsPoint {
        return { x: pt.x, y: pt.y, }
    }

    public static inline function cloneb( box : GraphicsBox ) : GraphicsBox {
        return { x: box.x, y: box.y, width: box.width, height: box.height, }
    }

    public static var epsilon : Float = 0.001;

    public static inline function isCloseTo( r : Float, value : Float ) : Bool {
        return Math.abs( value - r ) < epsilon;
    }

    public static inline function isNumber( v : Float ) : Bool {
        return v != null && Math.isFinite( v );
    }

    public static inline function assertValidNumber( v : Float, ?message : String ) : Void {
        if( ! isNumber( v ) ) throw new haxe.Exception( message != null ? 'not a valid numeric value: "${message}"' : 'assert valid number' );
    }

    public static inline function assertValidPoint( v : GraphicsPoint, ?message : String ) : Void {
        if( ! isNumber( v.x ) || ! isNumber( v.y ) ) throw new haxe.Exception( message != null ? 'not a valid point x,y values: "${message}"' : 'assert valid GraphicsPoint' );
    }

    /** Generate random 4-hex-digit number as a string **/
    public static function quad() : String {
        // Get random value between 0x10000 and 0x20000.
        // This will ensure that we have leading zeroes later on
        // when we do conversion to hex string
        var randomValue = Math.floor( ( 1.0 + Math.random() ) * 0x10000 );

        return ''+
            randomValue
            // convert to hex string ( base == 16 ),
            // this string will be 5 characters long
            // since generated value is between 0x10000 and 0x20000.
            .hex( 5 )
            // strip leading character so we are left
            // with 4-character string
            .substring( 1 );
    }
}
