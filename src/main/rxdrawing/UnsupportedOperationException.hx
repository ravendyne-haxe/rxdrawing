// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing;

class UnsupportedOperationException extends haxe.Exception {}
