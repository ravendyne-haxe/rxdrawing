// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.core;

import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;


abstract class DrawingNode {

    public var element(default,null) : GraphicsElement;
    private var children(default,null) : Array<DrawingNode>;
    private var parent(default,null) : DrawingNode;
    private var graphics(default,null) : Graphics;

    /**
     * X-coordinate of this node's origin position, relative to its parent.
     * All *significant points* of the node have their coordinates relative to this origin.
     * All *transforms* (rotation, translation, scaling, skew) are relative to this origin.
     */
    public var x (get,set) : Float;
    /**
     * Y-coordinate of this node's origin position, relative to its parent.
     * All *significant points* of the node have their coordinates relative to this origin.
     * All *transforms* (rotation, translation, scaling, skew) are relative to this origin.
     */
    public var y (get,set) : Float;

    private var _x : Float;
    private var _y : Float;

    private function get_x() : Float { return _x; }
    private function set_x(v:Float) : Float {
        v.assertValidNumber('x');
        _x = v;
        graphics.setOrigin( element, _x, _y );
        return _x;
    }
    private function get_y() : Float { return _y; }
    private function set_y(v:Float) : Float {
        v.assertValidNumber('y');
        _y = v;
        graphics.setOrigin( element, _x, _y );
        return _y;
    }

    /**
     * **Changes** significant points so that new *non-transformed* bounding box has size (`width`,`height`),
     * while top-left corner of the bounding box stays unchanged.
     * @param width
     * @param height
     */
    abstract public function resizeTo( width : Float, height : Float ) : Void;
    /**
     * **Changes** significant points by adding `dx`, `dy` to their current positions.
     * @param dx
     * @param dy
     */
    abstract public function reshapeMoveBy( dx : Float, dy : Float ) : Void;
    /**
     * **Changes** significant points by scaling their current positions by (`sx`,`sy`).
     * This means the node shape is scaled relative to it's origin.
     * @param sx
     * @param sy
     */
    abstract public function reshapeScale( sx : Float, sy : Float ) : Void;
    /**
     * **Changes** significant points by scaling rotation their current positions
     * around node's origin by `angle` radians counter-clockwise.
     * @param sx
     * @param sy
     */
    abstract public function reshapeRotateBy( angle : Float ) : Void;
    /**
     * **Changes** significant points by setting them so that
     * **new** *non-transformed* bounding box has it's top-left corner in (`x`,`y`).
     * @param x
     * @param y
     */
    abstract public function moveTo( x : Float, y : Float ) : Void;
    /**
     * Apply transformations to the node, relative to its origin.
     * `ops` are added to whatever existing transforms.
     * @param ops
     */
    abstract public function transform( ops : Array<GraphicsTransformOperation> ) : Void;
    /**
     * Remove all transofrms applied with `transform()` method for this node.
     */
    abstract public function clearTransform() : Void;
    /**
     * Translate node's origin relative to its parent.
     * Changes `T` part in `SRT` element's core transform chain.
     * @param x
     * @param y
     */
    abstract public function setOrigin( x : Float, y : Float ) : Void;
    /**
     * Scale node's points relative to its origin.
     * Changes `S` part in `SRT` element's core transform chain.
     * @param sx
     * @param sy
     */
    abstract public function setScale( sx : Float, sy : Float ) : Void;
    /**
     * Rotate node's origin relative to its parent's X-axis.
     * This rotates the node's coordinate system.
     * Changes `R` part in `SRT` element's core transform chain.
     * @param angle
     */
    abstract public function setRotation( angle : Float ) : Void;

    private function new( g : Graphics ) {
        graphics = g;
        parent = null;
        element = null;
        children = [];
        _x = 0;
        _y = 0;
    }

    /**
     * Make this node childe of the `p` node. The node position (`x`, `y`) and transform (rotation, translation)
     * will be relative to it's new parent.
     *
     * Setting parent to `null` will make this node a child of the `Graphics` root.
     * To remove the node completely, use `destroy()` method.
     *
     * @param p node to attach this node to, if `null` the node will be child of the `Graphics` root
     */
    public function setParent( p : DrawingNode ) {
        // js.Syntax.code("console.log({0}, {1}, {2})", 'setPrent', this.element, p != null ? p.element : null );
        if( parent != null ) {
            // js.Syntax.code("console.log({0}, {1}, {2})", 'setPrent ->  parent != null -> removeChild', this.parent.element, this.element);
            parent.removeChild( this );
        }
        if( p != null ) {
            p.registerChild( this );
        } else {
            graphics.appendChild( element );
        }
        parent = p;
    }

    private function registerChild( child : DrawingNode ) {
        children.push( child );
        graphics.appendChild( element, child.element );
        child.parent = this;
    }

    private function removeChild( child : DrawingNode ) {
        children.remove( child );
        graphics.removeChild( element, child.element );
        child.parent = null;
    }

    public function setAttribute( attr : GraphicsAttribute, value : String ) {
        graphics.setAttribute( element, attr, value );
    }
    public function setAttributes( attrs : Map<GraphicsAttribute, String> ) {
        graphics.setAttributes( element, attrs );
    }
    public function getAABBox() : GraphicsBox {
        return graphics.getElementBox( element );
    }

    function reshapeScaleAPoint( pt : GraphicsPoint, sx : Float, sy : Float ) : GraphicsPoint {
        return {
            x: pt.x * sx,
            y: pt.y * sy,
        }
    }

    function reshapeTranslateAPoint( pt : GraphicsPoint, dx : Float, dy : Float ) : GraphicsPoint {
        return {
            x: pt.x + dx,
            y: pt.y + dy,
        }
    }

    function reshapeRotateAPoint( pt : GraphicsPoint, angle : Float ) : GraphicsPoint {
        return {
            x: pt.x * Math.cos( angle ) - pt.y * Math.sin( angle ),
            y: pt.x * Math.sin( angle ) + pt.y * Math.cos( angle ),
        }
    }

    public function destroy() {
        if( parent != null ) {
            parent.removeChild( this );
        } else {
            graphics.removeChild( element );
        }
        for( child in children ) {
            child.destroy();
        }
    }
}
