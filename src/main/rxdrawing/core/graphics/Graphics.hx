// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.core.graphics;

using rxdrawing.RxDrawingTools;



/////////////////////////////////////////////////////////////////////////////
//
// GRAPHICS TYPES
//
/////////////////////////////////////////////////////////////////////////////
interface GraphicsElement {
}

typedef GraphicsPoint = {
    var x : Float;
    var y : Float;
}

typedef GraphicsSize = {
    var width : Float;
    var height : Float;
}

typedef GraphicsBox = {
    > GraphicsPoint,
    > GraphicsSize,
}

typedef GraphicsEventHnd = Any;

typedef GraphicsMouseEventCallback = GraphicsMouseEvent -> Void;
typedef GraphicsEventCallback = GraphicsEvent -> Void;


/////////////////////////////////////////////////////////////////////////////
//
// PATH INSTRUCTIONS
//
/////////////////////////////////////////////////////////////////////////////
enum PathInstruction {
    // absolute
    CPathMoveTo( p : GraphicsPoint );
    CPathLineTo( p : GraphicsPoint );
    CPathQuadraticCurveTo( p2 : GraphicsPoint, p3 : GraphicsPoint );
    CPathCubicCurveTo( p2 : GraphicsPoint, p3 : GraphicsPoint, p4 : GraphicsPoint );
    CPathEllipticArcTo( rx : Float, ry : Float, xAxisRotation : Float, largeArcFlag : Int, sweepFlag : Int, p2 : GraphicsPoint );
    // relative
    CPathMoveToRelative( p : GraphicsPoint );
    CPathLineToRelative( p : GraphicsPoint );
    CPathQuadraticCurveToRelative( p2 : GraphicsPoint, p3 : GraphicsPoint );
    CPathCubicCurveToRelative( p2 : GraphicsPoint, p3 : GraphicsPoint, p4 : GraphicsPoint );
    CPathEllipticArcToRelative( rx : Float, ry : Float, xAxisRotation : Float, largeArcFlag : Int, sweepFlag : Int, p2 : GraphicsPoint );
    // n/a
    CPathClosePath;
}


/////////////////////////////////////////////////////////////////////////////
//
// ATTRIBUTES
//
/////////////////////////////////////////////////////////////////////////////
enum GraphicsAttribute {
    Id;

    Stroke;
    StrokeWidth;
    StrokeOpacity;
    Fill;
    FillOpacity;

    Opacity;
    Visible;

    FontFamily;
    FontSize;
    FontStyle;
    FontWeight;

    Custom( name : String );
}

abstract GraphicsAttributeValue(String) from String to String {

    public static var NONE( default, never ) : GraphicsAttributeValue = "none";
    public static var NULL( default, never ) : GraphicsAttributeValue = null;

    inline function new( value : String ) {
        this = value;
    }
}


/////////////////////////////////////////////////////////////////////////////
//
// SPECIFIC OPTIONS
//
/////////////////////////////////////////////////////////////////////////////

typedef PanelOptions = {
    var ?rx : Float;
    var ?ry : Float;
    var ?tl : Bool;
    var ?tr : Bool;
    var ?bl : Bool;
    var ?br : Bool;
    var ?curved : Bool;
}

typedef Radian = Float;

enum GraphicsTransformOperation {
    Translate( tx : Float, ty : Float );
    TranslateX( tx : Float );
    TranslateY( ty : Float );

    Rotate( angle : Radian );

    Scale( sx : Float, sy : Float );
    ScaleX( s : Float );
    ScaleY( s : Float );

    SkewX( angle : Radian );
    SkewY( angle : Radian );
}

/////////////////////////////////////////////////////////////////////////////
//
// EVENTS
//
/////////////////////////////////////////////////////////////////////////////
typedef GraphicsEvent = {
}

final class MouseButton {
    // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons#value

    // No button or un-initialized
    public static inline var NONE : Int = 0x00;
    // Primary button (usually the left button)
    public static inline var PRIMARY : Int = 0x01;
    // Secondary button (usually the right button)
    public static inline var SECONDARY : Int = 0x02;
    // Auxiliary button (usually the mouse wheel button or middle button)
    public static inline var AUXILIARY : Int = 0x04;
    // 4th button (typically the "Browser Back" button)
    public static inline var BUTTON_4 : Int = 0x08;
    // 5th button (typically the "Browser Forward" button)
    public static inline var BUTTON_5 : Int = 0x10;
}
final class MouseModifierKey {
    // No key or un-initialized
    public static inline var NONE : Int = 0x00;
    public static inline var CTRL : Int = 0x01;
    public static inline var SHIFT : Int = 0x02;
    public static inline var ALT : Int = 0x04;
    public static inline var META : Int = 0x08;
}
typedef GraphicsMouseEventModifier = {
	// var ?modifierAltGraph : Bool;
	// var ?modifierCapsLock : Bool;
	// var ?modifierFn : Bool;
	// var ?modifierFnLock : Bool;
	// var ?modifierNumLock : Bool;
	// var ?modifierOS : Bool;
	// var ?modifierScrollLock : Bool;
	// var ?modifierSymbol : Bool;
	// var ?modifierSymbolLock : Bool;
}
typedef GraphicsMouseEvent = GraphicsEvent & GraphicsMouseEventModifier & {
    // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent#properties
    var clientX(default,null) : Float;
    var clientY(default,null) : Float;
    var movementX(default,null) : Float;
    var movementY(default,null) : Float;

    // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons
    var buttons(default,null) : Int;

    var keys(default,null) : Int;
}

final class GraphicsWheelDeltaMode {
    public static inline var DELTA_PIXEL : Int = 0x00;
    public static inline var DELTA_LINE : Int = 0x01;
    public static inline var DELTA_PAGE : Int = 0x02;
}
typedef GraphicsWheelEvent = GraphicsMouseEvent & {
    // https://developer.mozilla.org/en-US/docs/Web/API/WheelEvent#instance_properties
    var deltaX(default,null) : Float;
    var deltaY(default,null) : Float;
    var deltaZ(default,null) : Float;
    var deltaMode(default,null) : Int;
}

typedef GraphicsCustomEvent = GraphicsEvent & {
}

enum GraphicsEventName {
    MouseUp;
    MouseDown;
    MouseEnter;
    MouseLeave;
    MouseClick;
    MouseMove;
    MouseWheel;

    Custom( evtName: String );
}


final class GraphicsTools {
    private function new(){}

    public static inline function hasAltKey( v : Int ) { return v & MouseModifierKey.ALT != 0; }
    public static inline function hasCtrlKey( v : Int ) { return v & MouseModifierKey.CTRL != 0; }
    public static inline function hasShiftKey( v : Int ) { return v & MouseModifierKey.SHIFT != 0; }
    public static inline function hasMetaKey( v : Int ) { return v & MouseModifierKey.META != 0; }
}

/////////////////////////////////////////////////////////////////////////////
//
// GRAPHICS
//
/////////////////////////////////////////////////////////////////////////////
interface Graphics {

    /////////////////////////////////////////////////////////////////////////////
    //
    // ATTRIBUTES
    //
    /////////////////////////////////////////////////////////////////////////////
    function setAttribute( el : GraphicsElement, attr : GraphicsAttribute, value : GraphicsAttributeValue ) : Void;

    function setAttributes( el : GraphicsElement, attrs : Map<GraphicsAttribute, GraphicsAttributeValue> ) : Void;

    function getAttribute( el : GraphicsElement, attr : GraphicsAttribute ) : GraphicsAttributeValue;

    function removeAttribute( el : GraphicsElement, attr : GraphicsAttribute ) : Void;

    /////////////////////////////////////////////////////////////////////////////
    //
    // CREATE
    //
    /////////////////////////////////////////////////////////////////////////////
    function createPath( pi : Array<PathInstruction> ) : GraphicsElement;

    function createCircle( cx : Float, cy : Float, r : Float ) : GraphicsElement;

    function createEllipse( cx : Float, cy : Float, rx : Float, ry : Float ) : GraphicsElement;

    function createRect( x : Float, y : Float, width : Float, height : Float, ?rx : Float, ?ry : Float ) : GraphicsElement;

    function createLine( x1 : Float, y1 : Float, x2 : Float, y2 : Float ) : GraphicsElement;

    /**
     * `p0` is starting point, `p3` is the end point, and `p1` and `p2` are control points.
     *
     *  All points are absolute positions in user coordinate system.
     *
     * @param p0 start point
     * @param p1 control point 1
     * @param p2 control point 2
     * @param p3 end point
     * @return GraphicsElement
     */
    function createCubicCurve( p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint, p3 : GraphicsPoint/* , rel : Bool */ ) : GraphicsElement;

    /**
     * `p0` is starting point, `p2` is the end point, and `p1` is control point.
     *
     *  All points are absolute positions in user coordinate system.
     *
     * @param p0 start point
     * @param p1 control point
     * @param p2 end point
     * @return GraphicsElement
     */
    function createQuadraticCurve( p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint/* , rel : Bool */ ) : GraphicsElement;

    function createText( text : String, ?x : Float, ?y : Float ) : GraphicsElement;

    function createImage( href : String, ?width : Float, ?height : Float, ?x : Float, ?y : Float ) : GraphicsElement;

    function createGroup( children : Array<GraphicsElement> ) : GraphicsElement;

    function createPanel( width : Float, height : Float, ?x : Float, ?y : Float, ?options : PanelOptions ) : GraphicsElement;


    /////////////////////////////////////////////////////////////////////////////
    //
    // UPDATE
    //
    /////////////////////////////////////////////////////////////////////////////
    function updatePath( el : GraphicsElement, pi : Array<PathInstruction> ) : Void;

    function updatCircle( el : GraphicsElement, cx : Float, cy : Float, r : Float ) : Void;

    function updateEllipse( el : GraphicsElement,cx : Float, cy : Float, rx : Float, ry : Float ) : Void;

    function updateRect( el : GraphicsElement, x : Float, y : Float, width : Float, height : Float, ?rx : Float, ?ry : Float ) : Void;

    function updateLine( el : GraphicsElement, x1 : Float, y1 : Float, x2 : Float, y2 : Float ) : Void;

    function updateCubicCurve( el : GraphicsElement, p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint, p3 : GraphicsPoint ) : Void;

    function updateQuadraticCurve( el : GraphicsElement, p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint ) : Void;

    function updateText( el : GraphicsElement, text : String, ?x : Float, ?y : Float ) : Void;

    function updateImage( el : GraphicsElement, href : String, ?width : Float, ?height : Float, ?x : Float, ?y : Float ) : Void;

    function updatePanel( el : GraphicsElement,  width : Float, height : Float, x : Float, y : Float, ?options : PanelOptions ) : Void;

    /////////////////////////////////////////////////////////////////////////////
    //
    // EVENTS
    //
    /////////////////////////////////////////////////////////////////////////////

    // add listener / dispatch event for MouseEvent event type
    function addMouseEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: GraphicsMouseEvent -> Void ) : GraphicsEventHnd;
    function dispatchMouseEvent( el : GraphicsElement, evtName : GraphicsEventName, event: GraphicsMouseEvent ) : Void;

    // add listener / dispatch event for WheelEvent event type
    function addWheelEventListener( el : GraphicsElement, handler: GraphicsWheelEvent -> Void ) : GraphicsEventHnd;
    function dispatchWheelEvent( el : GraphicsElement, event: GraphicsWheelEvent ) : Void;

    // add listener / dispatch event for Event event type
    function addEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: GraphicsEvent -> Void ) : GraphicsEventHnd;
    function dispatchEvent( el : GraphicsElement, evtName : GraphicsEventName, event: GraphicsEvent ) : Void;

    // add listener / dispatch event for CustomEvent event type
    function addCustomEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: Dynamic -> Void ) : GraphicsEventHnd;
    function dispatchCustomEvent( el : GraphicsElement, evtName : GraphicsEventName, event: Dynamic ) : Void;

    function removeEventListener( el : GraphicsElement, evtName : GraphicsEventName, handle: GraphicsEventHnd ) : Void;

    /////////////////////////////////////////////////////////////////////////////
    //
    // STRUCTURE
    //
    /////////////////////////////////////////////////////////////////////////////
    function appendChild( el : GraphicsElement, ?c : GraphicsElement ) : Void;

    function removeChild( el : GraphicsElement, ?c : GraphicsElement ) : Void;

    function moveToFront( el : GraphicsElement, ?c : GraphicsElement ) : Void;

    function moveToBack( el : GraphicsElement, ?c : GraphicsElement ) : Void;

    /////////////////////////////////////////////////////////////////////////////
    //
    // MISC
    //
    /////////////////////////////////////////////////////////////////////////////
    /**
     * Moves `GraphicsElement`'s origin to a new point.
     *
     * Origin coordinates are relative to the element's parent origin.
     *
     * Changes `T` part in `SRT` element's core transform chain.
     *
     * @param el
     * @param x
     * @param y
     */
    function setOrigin( el : GraphicsElement, x : Float, y : Float ) : Void;
    /**
     * Scales `GraphicsElement` relative to the origin.
     *
     * Changes `S` part in `SRT` element's core transform chain.
     *
     * @param el
     * @param sx scale factor in X-axis direction
     * @param sy scale factor in Y-axis direction
     */
    function setScale( el : GraphicsElement, sx : Float, sy : Float ) : Void;
    /**
     * Rotates `GraphicsElement`'s coordinate system relative to its parent's X-axis.
     *
     * Rotation angle is angle between parent's X-axis and `GraphicsElement`'s
     * coordinate system X-axis. Positive angle value rotates `GraphicsElement`'s
     * coordinate system X-axis in counter-clockwise direction.
     *
     * Changes `R` part in `SRT` element's core transform chain.
     *
     * @param el
     * @param angle rotation angle in radians
     * @param y
     */
    function setRotation( el : GraphicsElement, angle : Float ) : Void;
    /**
     * Applies given transofrmations to the `GraphicsElement`.
     *
     * Transforms are relative to element's origin point.
     *
     * @param el
     * @param ops
     */
    function transform( el : GraphicsElement, ops : Array<GraphicsTransformOperation> ) : Void;
    function clearTransform( el : GraphicsElement ) : Void;

    /**
     * Convert point in client coordinate system (i.e. html body element, or graphics window, or display screen etc.)
     * to the user coordinate system of this graphics viewport (i.e. SVG viewport, or UI root, or HUD root etc.).
     */
    function clientPointToUser( x : Float, y : Float ) : GraphicsPoint;

    /**
     * Return the element's axis-aligned bounding box in this graphics context
     */
    function getElementBox( el : GraphicsElement ) : GraphicsBox;

    function getElementFromPoint( x : Float, y : Float ) : GraphicsElement;

    function getElementsFromPoint( x : Float, y : Float ) : Array<GraphicsElement>;
}
