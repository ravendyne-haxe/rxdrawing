// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.core.graphics;

import haxe.ds.Vector;

using StringTools;

// https://developer.mozilla.org/en-US/docs/Web/CSS/color_value
// https://rgbacolorpicker.com/
// https://en.wikipedia.org/wiki/RGBA_color_model#Representation
abstract RGBA8888Color(Vector<Float>) from Vector<Float> to Vector<Float> {

    public inline function new() {
        this = new Vector<Float>(4);
        this[0] = 0.0;
        this[1] = 0.0;
        this[2] = 0.0;
        this[3] = 0.0;
    }
    @:arrayAccess
    public inline function getk(key:Int) {
        return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v < 0.0 ? 0.0 : v > 1.0 ? 1.0 : v;
        return this[k];
    }

    public var r ( get, set ) : Float;
    public var g ( get, set ) : Float;
    public var b ( get, set ) : Float;
    public var a ( get, set ) : Float;
    private inline function get_r() : Float {
        return this[0];
    }
    private inline function set_r( r : Float) : Float {
        return this[0] = r < 0.0 ? 0.0 : r > 1.0 ? 1.0 : r;
    }
    private inline function get_g() : Float {
        return this[1];
    }
    private inline function set_g( g : Float) : Float {
        return this[1] = g < 0.0 ? 0.0 : g > 1.0 ? 1.0 : g;
    }
    private inline function get_b() : Float {
        return this[2];
    }
    private inline function set_b( b : Float) : Float {
        return this[2] = b < 0.0 ? 0.0 : b > 1.0 ? 1.0 : b;
    }
    private inline function get_a() : Float {
        return this[3];
    }
    private inline function set_a( a : Float) : Float {
        return this[3] = a < 0.0 ? 0.0 : a > 1.0 ? 1.0 : a;
    }

    public var ri ( get, set ) : Int;
    public var gi ( get, set ) : Int;
    public var bi ( get, set ) : Int;
    public var ai ( get, set ) : Int;
    private inline function get_ri() : Int {
        return Std.int( this[0] * 255 );
    }
    private inline function set_ri( r : Int) : Int {
        this[0] = r < 0 ? 0.0 : r > 255 ? 1.0 : r / 255;
        return ri;
    }
    private inline function get_gi() : Int {
        return Std.int( this[1] * 255 );
    }
    private inline function set_gi( g : Int) : Int {
        this[1] = g < 0 ? 0.0 : g > 255 ? 1.0 : g / 255;
        return gi;
    }
    private inline function get_bi() : Int {
        return Std.int( this[2] * 255 );
    }
    private inline function set_bi( b : Int) : Int {
        this[2] = b < 0 ? 0.0 : b > 255 ? 1.0 : b / 255;
        return bi;
    }
    private inline function get_ai() : Int {
        return Std.int( this[3] * 255 );
    }
    private inline function set_ai( a : Int) : Int {
        this[3] = a < 0 ? 0.0 : a > 255 ? 1.0 : a / 255;
        return ai;
    }

    public static inline function create( r : Float, g : Float, b : Float, a : Float ) : RGBA8888Color {

        var rgbaColor = new RGBA8888Color();

        rgbaColor.r = r;
        rgbaColor.g = g;
        rgbaColor.b = b;
        rgbaColor.a = a;

        return rgbaColor;
    }

    public static inline function createi( r : Int, g : Int, b : Int, a : Int ) : RGBA8888Color {

        var rgbaColor = new RGBA8888Color();

        rgbaColor.ri = r;
        rgbaColor.gi = g;
        rgbaColor.bi = b;
        rgbaColor.ai = a;

        return rgbaColor;
    }

    @:from
    public static function fromInt( value : Int ) : RGBA8888Color {

        var rgbaColor = new RGBA8888Color();

        rgbaColor.ri = (value >> 24) & 255;
        rgbaColor.gi = (value >> 16) & 255;
        rgbaColor.bi = (value >> 8) & 255;
        rgbaColor.ai = value & 255;

        return rgbaColor;
    }
    @:to
    public function toInt() : Int {

        var value : Int = ri << 24 | gi << 16 | bi << 8 | ai;

        return value;
    }

    @:from
    static public function fromArray( a : Array<Float> ) {

        var xc = new RGBA8888Color();

        xc.r = a[0];
        xc.g = a[1];
        xc.b = a[2];
        xc.a = a[3];

        return xc;
    }
    @:to
    public function toArray() : Array<Float> {
        return [r, g, b, a];
    }


    @:from
    public static function fromCSShex( value : String ) : RGBA8888Color {

        // https://developer.mozilla.org/en-US/docs/Web/CSS/hex-color
        var rgbaColor = new RGBA8888Color();

        if( value == null ) return NamedColor.black;

        value = value.trim();
        if( ! value.startsWith('#') ) return NamedColor.black;
        if( value.length < 4 ) return NamedColor.black;

        // strip leading '#'
        value = value.substring( 1, value.length );

        var r = '';
        var g = '';
        var b = '';
        var a = '';
        if( value.length == 3 ) {
            // #RGB
            r = value.substr( 0, 1 ); r = r + r;
            g = value.substr( 1, 1 ); g = g + g;
            b = value.substr( 2, 1 ); b = b + b;
            a = 'ff';
        } else if( value.length == 4 ) {
            // #RGBA
            r = value.substr( 0, 1 ); r = r + r;
            g = value.substr( 1, 1 ); g = g + g;
            b = value.substr( 2, 1 ); b = b + b;
            a = value.substr( 3, 1 ); a = a + a;
        } else if( value.length == 6 ) {
            // #RRGGBB
            r = value.substr( 0, 2 );
            g = value.substr( 2, 2 );
            b = value.substr( 4, 2 );
            a = 'ff';
        } else if( value.length == 8 ) {
            // #RRGGBBAA
            r = value.substr( 0, 2 );
            g = value.substr( 2, 2 );
            b = value.substr( 4, 2 );
            a = value.substr( 6, 2 );
        } else return NamedColor.black;

        rgbaColor.ri = Std.parseInt( '0x' + r );
        rgbaColor.gi = Std.parseInt( '0x' + g );
        rgbaColor.bi = Std.parseInt( '0x' + b );
        rgbaColor.ai = Std.parseInt( '0x' + a );

        return rgbaColor;
    }
    @:to
    public function toCSSrgba() : String {
        // https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#formal_syntax
        return 'rgba(${ri},${gi},${bi},${a})';
    }

    public function toString() : String {
        var vi = StringTools.hex( toInt(), 8 );
        return 'RGBA8888Color(${r},${g},${b},${a})#0x$vi';
    }
}

typedef GraphicsColor = RGBA8888Color;
