// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.core.graphics.theme;


final class MaterialPalette {
    private function new(){}

    public static final Red: Map<String,RGBA8888Color> = [
        '50' => 0xFFEBEEFF,
        '100' => 0xFFCDD2FF,
        '200' => 0xEF9A9AFF,
        '300' => 0xE57373FF,
        '400' => 0xEF5350FF,
        '500' => 0xF44336FF,
        '600' => 0xE53935FF,
        '700' => 0xD32F2FFF,
        '800' => 0xC62828FF,
        '900' => 0xB71C1CFF,
        'A100' => 0xFF8A80FF,
        'A200' => 0xFF5252FF,
        'A400' => 0xFF1744FF,
        'A700' => 0xD50000FF,
    ];

    public static final Pink: Map<String,RGBA8888Color> = [
        '50' => 0xFCE4ECFF,
        '100' => 0xF8BBD0FF,
        '200' => 0xF48FB1FF,
        '300' => 0xF06292FF,
        '400' => 0xEC407AFF,
        '500' => 0xE91E63FF,
        '600' => 0xD81B60FF,
        '700' => 0xC2185BFF,
        '800' => 0xAD1457FF,
        '900' => 0x880E4FFF,
        'A100' => 0xFF80ABFF,
        'A200' => 0xFF4081FF,
        'A400' => 0xF50057FF,
        'A700' => 0xC51162FF,
    ];

    public static final Purple: Map<String,RGBA8888Color> = [
        '50' => 0xF3E5F5FF,
        '100' => 0xE1BEE7FF,
        '200' => 0xCE93D8FF,
        '300' => 0xBA68C8FF,
        '400' => 0xAB47BCFF,
        '500' => 0x9C27B0FF,
        '600' => 0x8E24AAFF,
        '700' => 0x7B1FA2FF,
        '800' => 0x6A1B9AFF,
        '900' => 0x4A148CFF,
        'A100' => 0xEA80FCFF,
        'A200' => 0xE040FBFF,
        'A400' => 0xD500F9FF,
        'A700' => 0xAA00FFFF,
    ];

    public static final Deep_Purple: Map<String,RGBA8888Color> = [
        '50' => 0xEDE7F6FF,
        '100' => 0xD1C4E9FF,
        '200' => 0xB39DDBFF,
        '300' => 0x9575CDFF,
        '400' => 0x7E57C2FF,
        '500' => 0x673AB7FF,
        '600' => 0x5E35B1FF,
        '700' => 0x512DA8FF,
        '800' => 0x4527A0FF,
        '900' => 0x311B92FF,
        'A100' => 0xB388FFFF,
        'A200' => 0x7C4DFFFF,
        'A400' => 0x651FFFFF,
        'A700' => 0x6200EAFF,
    ];

    public static final Indigo: Map<String,RGBA8888Color> = [
        '50' => 0xE8EAF6FF,
        '100' => 0xC5CAE9FF,
        '200' => 0x9FA8DAFF,
        '300' => 0x7986CBFF,
        '400' => 0x5C6BC0FF,
        '500' => 0x3F51B5FF,
        '600' => 0x3949ABFF,
        '700' => 0x303F9FFF,
        '800' => 0x283593FF,
        '900' => 0x1A237EFF,
        'A100' => 0x8C9EFFFF,
        'A200' => 0x536DFEFF,
        'A400' => 0x3D5AFEFF,
        'A700' => 0x304FFEFF,
    ];

    public static final Blue: Map<String,RGBA8888Color> = [
        '50' => 0xE3F2FDFF,
        '100' => 0xBBDEFBFF,
        '200' => 0x90CAF9FF,
        '300' => 0x64B5F6FF,
        '400' => 0x42A5F5FF,
        '500' => 0x2196F3FF,
        '600' => 0x1E88E5FF,
        '700' => 0x1976D2FF,
        '800' => 0x1565C0FF,
        '900' => 0x0D47A1FF,
        'A100' => 0x82B1FFFF,
        'A200' => 0x448AFFFF,
        'A400' => 0x2979FFFF,
        'A700' => 0x2962FFFF,
    ];

    public static final Light_Blue: Map<String,RGBA8888Color> = [
        '50' => 0xE1F5FEFF,
        '100' => 0xB3E5FCFF,
        '200' => 0x81D4FAFF,
        '300' => 0x4FC3F7FF,
        '400' => 0x29B6F6FF,
        '500' => 0x03A9F4FF,
        '600' => 0x039BE5FF,
        '700' => 0x0288D1FF,
        '800' => 0x0277BDFF,
        '900' => 0x01579BFF,
        'A100' => 0x80D8FFFF,
        'A200' => 0x40C4FFFF,
        'A400' => 0x00B0FFFF,
        'A700' => 0x0091EAFF,
    ];

    public static final Cyan: Map<String,RGBA8888Color> = [
        '50' => 0xE0F7FAFF,
        '100' => 0xB2EBF2FF,
        '200' => 0x80DEEAFF,
        '300' => 0x4DD0E1FF,
        '400' => 0x26C6DAFF,
        '500' => 0x00BCD4FF,
        '600' => 0x00ACC1FF,
        '700' => 0x0097A7FF,
        '800' => 0x00838FFF,
        '900' => 0x006064FF,
        'A100' => 0x84FFFFFF,
        'A200' => 0x18FFFFFF,
        'A400' => 0x00E5FFFF,
        'A700' => 0x00B8D4FF,
    ];

    public static final Teal: Map<String,RGBA8888Color> = [
        '50' => 0xE0F2F1FF,
        '100' => 0xB2DFDBFF,
        '200' => 0x80CBC4FF,
        '300' => 0x4DB6ACFF,
        '400' => 0x26A69AFF,
        '500' => 0x009688FF,
        '600' => 0x00897BFF,
        '700' => 0x00796BFF,
        '800' => 0x00695CFF,
        '900' => 0x004D40FF,
        'A100' => 0xA7FFEBFF,
        'A200' => 0x64FFDAFF,
        'A400' => 0x1DE9B6FF,
        'A700' => 0x00BFA5FF,
    ];

    public static final Green: Map<String,RGBA8888Color> = [
        '50' => 0xE8F5E9FF,
        '100' => 0xC8E6C9FF,
        '200' => 0xA5D6A7FF,
        '300' => 0x81C784FF,
        '400' => 0x66BB6AFF,
        '500' => 0x4CAF50FF,
        '600' => 0x43A047FF,
        '700' => 0x388E3CFF,
        '800' => 0x2E7D32FF,
        '900' => 0x1B5E20FF,
        'A100' => 0xB9F6CAFF,
        'A200' => 0x69F0AEFF,
        'A400' => 0x00E676FF,
        'A700' => 0x00C853FF,
    ];

    public static final Light_Green: Map<String,RGBA8888Color> = [
        '50' => 0xF1F8E9FF,
        '100' => 0xDCEDC8FF,
        '200' => 0xC5E1A5FF,
        '300' => 0xAED581FF,
        '400' => 0x9CCC65FF,
        '500' => 0x8BC34AFF,
        '600' => 0x7CB342FF,
        '700' => 0x689F38FF,
        '800' => 0x558B2FFF,
        '900' => 0x33691EFF,
        'A100' => 0xCCFF90FF,
        'A200' => 0xB2FF59FF,
        'A400' => 0x76FF03FF,
        'A700' => 0x64DD17FF,
    ];

    public static final Lime: Map<String,RGBA8888Color> = [
        '50' => 0xF9FBE7FF,
        '100' => 0xF0F4C3FF,
        '200' => 0xE6EE9CFF,
        '300' => 0xDCE775FF,
        '400' => 0xD4E157FF,
        '500' => 0xCDDC39FF,
        '600' => 0xC0CA33FF,
        '700' => 0xAFB42BFF,
        '800' => 0x9E9D24FF,
        '900' => 0x827717FF,
        'A100' => 0xF4FF81FF,
        'A200' => 0xEEFF41FF,
        'A400' => 0xC6FF00FF,
        'A700' => 0xAEEA00FF,
    ];

    public static final Yellow: Map<String,RGBA8888Color> = [
        '50' => 0xFFFDE7FF,
        '100' => 0xFFF9C4FF,
        '200' => 0xFFF59DFF,
        '300' => 0xFFF176FF,
        '400' => 0xFFEE58FF,
        '500' => 0xFFEB3BFF,
        '600' => 0xFDD835FF,
        '700' => 0xFBC02DFF,
        '800' => 0xF9A825FF,
        '900' => 0xF57F17FF,
        'A100' => 0xFFFF8DFF,
        'A200' => 0xFFFF00FF,
        'A400' => 0xFFEA00FF,
        'A700' => 0xFFD600FF,
    ];

    public static final Amber: Map<String,RGBA8888Color> = [
        '50' => 0xFFF8E1FF,
        '100' => 0xFFECB3FF,
        '200' => 0xFFE082FF,
        '300' => 0xFFD54FFF,
        '400' => 0xFFCA28FF,
        '500' => 0xFFC107FF,
        '600' => 0xFFB300FF,
        '700' => 0xFFA000FF,
        '800' => 0xFF8F00FF,
        '900' => 0xFF6F00FF,
        'A100' => 0xFFE57FFF,
        'A200' => 0xFFD740FF,
        'A400' => 0xFFC400FF,
        'A700' => 0xFFAB00FF,
    ];

    public static final Orange: Map<String,RGBA8888Color> = [
        '50' => 0xFFF3E0FF,
        '100' => 0xFFE0B2FF,
        '200' => 0xFFCC80FF,
        '300' => 0xFFB74DFF,
        '400' => 0xFFA726FF,
        '500' => 0xFF9800FF,
        '600' => 0xFB8C00FF,
        '700' => 0xF57C00FF,
        '800' => 0xEF6C00FF,
        '900' => 0xE65100FF,
        'A100' => 0xFFD180FF,
        'A200' => 0xFFAB40FF,
        'A400' => 0xFF9100FF,
        'A700' => 0xFF6D00FF,
    ];

    public static final Deep_Orange: Map<String,RGBA8888Color> = [
        '50' => 0xFBE9E7FF,
        '100' => 0xFFCCBCFF,
        '200' => 0xFFAB91FF,
        '300' => 0xFF8A65FF,
        '400' => 0xFF7043FF,
        '500' => 0xFF5722FF,
        '600' => 0xF4511EFF,
        '700' => 0xE64A19FF,
        '800' => 0xD84315FF,
        '900' => 0xBF360CFF,
        'A100' => 0xFF9E80FF,
        'A200' => 0xFF6E40FF,
        'A400' => 0xFF3D00FF,
        'A700' => 0xDD2C00FF,
    ];

    public static final Brown: Map<String,RGBA8888Color> = [
        '50' => 0xEFEBE9FF,
        '100' => 0xD7CCC8FF,
        '200' => 0xBCAAA4FF,
        '300' => 0xA1887FFF,
        '400' => 0x8D6E63FF,
        '500' => 0x795548FF,
        '600' => 0x6D4C41FF,
        '700' => 0x5D4037FF,
        '800' => 0x4E342EFF,
        '900' => 0x3E2723FF,
        // not present in the Material palette
        'A100' => NamedColor.fuchsia,
        'A200' => NamedColor.fuchsia,
        'A400' => NamedColor.fuchsia,
        'A700' => NamedColor.fuchsia,
    ];

    public static final Gray: Map<String,RGBA8888Color> = [
        '50' => 0xFAFAFAFF,
        '100' => 0xF5F5F5FF,
        '200' => 0xEEEEEEFF,
        '300' => 0xE0E0E0FF,
        '400' => 0xBDBDBDFF,
        '500' => 0x9E9E9EFF,
        '600' => 0x757575FF,
        '700' => 0x616161FF,
        '800' => 0x424242FF,
        '900' => 0x212121FF,
        // not present in the Material palette
        'A100' => NamedColor.fuchsia,
        'A200' => NamedColor.fuchsia,
        'A400' => NamedColor.fuchsia,
        'A700' => NamedColor.fuchsia,
    ];

    public static final Blue_Gray: Map<String,RGBA8888Color> = [
        '50' => 0xECEFF1FF,
        '100' => 0xCFD8DCFF,
        '200' => 0xB0BEC5FF,
        '300' => 0x90A4AEFF,
        '400' => 0x78909CFF,
        '500' => 0x607D8BFF,
        '600' => 0x546E7AFF,
        '700' => 0x455A64FF,
        '800' => 0x37474FFF,
        '900' => 0x263238FF,
        // not present in the Material palette
        'A100' => NamedColor.fuchsia,
        'A200' => NamedColor.fuchsia,
        'A400' => NamedColor.fuchsia,
        'A700' => NamedColor.fuchsia,
    ];

}
