// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.core.graphics.theme;

using StringTools;

// https://material.angular.io/guide/theming#palettes
// https://m2.material.io/design/color/the-color-system.html#tools-for-picking-colors
// https://m2.material.io/resources/color/#!/?view.left=0&view.right=0
// https://m3.material.io/styles/color/the-color-system/key-colors-tones
interface GraphicsTheme {

    var primaryPalette: Map<String,RGBA8888Color>;
    var primaryPaletteContrast: Map<String,RGBA8888Color>;
    var secondaryPalette: Map<String,RGBA8888Color>;
    var secondaryPaletteContrast: Map<String,RGBA8888Color>;

    var primary : String;
    var primaryLight : String;
    var primaryDark : String;
    var primaryVariant : String;

    var secondary : String;
    var secondaryLight : String;
    var secondaryDark : String;
    var secondaryVariant : String;

    // surface colours
    var backgroundColor : RGBA8888Color;
    var surfaceColor : RGBA8888Color;
    var errorColor : RGBA8888Color;

    // text and icon colours
    var onPrimaryColor : RGBA8888Color;
    var onSecondaryColor : RGBA8888Color;
    var onBackgroundColor : RGBA8888Color;
    var onSurfaceColor : RGBA8888Color;
    var onErrorColor : RGBA8888Color;
}
