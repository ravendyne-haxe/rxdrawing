// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components;

import rxdrawing.elements.Text;
import rxlayout.base.LayoutElement;
import rxdrawing.core.graphics.Graphics;
import rxlayout.types.LayoutBox;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;

class Label extends ComponentBase {

    var _text : Text;

    public function new( g : Graphics, text : String, ?parent_container : ComponentBase ) {
        super( g );

        container = null;
        layout = new LayoutElement( this );

        _text = new Text( g, text );

        _left = 0;
        _top = 0;
        _width = 10;
        _height = 10;

        element = _text.element;

        updateSize();

        setParent( parent_container );
    }

    private function updateSize() {
        var aabb = _text.getAABBox();
        layout.setSize( aabb.width.px(), aabb.height.px() );
        // trace('LABEL -> updateSize');
        // trace( 'aabb: ${aabb}' );
    }

    public function setText( text : String ) {
        // trace('LABEL -> setText');
        _text.setText( text );
        updateSize();
    }

    /**
     * Set font family
     *
     * @param fontFamily
     * @see https://www.w3.org/TR/css-fonts-3/#font-family-prop
     */
     public function setFontFamily( fontFamily : String ) {
        // trace('LABEL -> setFontFamily');
        _text.setFontFamily( fontFamily );
        updateSize();
    }

    /**
     * Set font size
     *
     * @param fontSize
     * @see https://www.w3.org/TR/css-fonts-3/#font-size-prop
     */
    public function setFontSize( fontSize : Int ) {
        _text.setFontSize( fontSize );
        updateSize();
    }

    /**
     * Set font weight
     *
     * @param fontWeight
     * @see https://www.w3.org/TR/css-fonts-3/#font-weight-prop
     */
     public function setFontWeight( fontWeight : String ) {
        _text.setFontWeight( fontWeight );
        updateSize();
    }

    /**
     * Set font style
     *
     * @param fontStyle
     * @see https://www.w3.org/TR/css-fonts-3/#font-style-prop
     */
     public function setFontStyle( fontStyle : String ) {
        _text.setFontStyle( fontStyle );
        updateSize();
    }

    public function setFill( value : String ) {
        // trace('LABEL -> setFill');
        _text.setFill( value );
    }

    public function setStroke( colour : String, width : Float = 1.0 ) {
        _text.setStroke( colour, width );
    }

    override public function setBox( box : LayoutBox ) : Void {
        super.setBox( box );

        _text.setOrigin( _left, _top );
        // we can't set width/height of a label
    }

}
