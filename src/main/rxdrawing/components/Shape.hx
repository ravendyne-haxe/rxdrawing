// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components;

import rxlayout.base.LayoutElement;
import rxdrawing.elements.AbstractElement;
import rxdrawing.core.graphics.Graphics;
import rxlayout.types.LayoutBox;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;

class Shape extends ComponentBase {
    // implements DrawingElement, ViewElement
    // accepts one of elements.XYZ as ctr param
    // and makes it container friendly
    // using LayoutElement container and
    // calling DrawingElement.setPosition/Size from
    // its own ViewElement.setBox()
    var _shape : AbstractElement;

    public function new( g : Graphics, shape : AbstractElement, ?parent_container : ComponentBase ) {
        super( g );

        container = null;
        layout = new LayoutElement( this );

        _shape = shape;

        _left = 0;
        _top = 0;
        _width = 10;
        _height = 10;

        element = _shape.element;

        var aabb = _shape.getAABBox();
        layout.setSize( aabb.width.px(), aabb.height.px() );
        // trace('SHAPE aabb');
        // trace(aabb);

        setParent( parent_container );
    }

    public function setFill( value : String ) {
        _shape.setFill( value );
    }

    public function setStroke( colour : String, width : Float = 1.0 ) {
        _shape.setStroke( colour, width );
    }

    override public function setBox( box : LayoutBox ) : Void {
        super.setBox( box );

        _shape.setOrigin( _left, _top );
        _shape.resizeTo( _width, _height );
    }
}
