// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.controls;

import rxlayout.types.LayoutBox;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

// impelements ViewElement and is base for all controls
abstract class AbstractControl extends ComponentBase {

    public function new( g : Graphics ) {
        super( g );
        container = null;
        layout = null;

        _left = 0;
        _top = 0;
        _width = 0;
        _height = 0;

        element = g.createRect( 0, 0, _width, _height );

        setAttributes([
            Fill => 'fuchsia',
        ]);
    }

    override public function setBox( box : LayoutBox ) : Void {
        super.setBox( box );

        graphics.setOrigin( element, _left, _top );
        graphics.updateRect( element, 0, 0, _width, _height );
    }

    // public function setBorder( thickness : Float, colour : String = 'black', style : String = 'solid' ) : Void {
    //     if( ! thickness.isNumber() ) { graphics.setAttribute( element, Stroke, GraphicsAttributeValue.NONE ); return; }

    //     graphics.setAttribute( element, StrokeWidth, '${thickness}' );
    //     graphics.setAttribute( element, Stroke, colour );
    // }
}
