// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.controls.toggle;

import rxlayout.types.SizeValue;
import rxdrawing.components.controls.toggle.AbstractToggle;
import rxdrawing.elements.Rectangle;
import rxdrawing.components.containers.HBox;
import rxdrawing.elements.Image;
import rxdrawing.components.containers.ContainerPanel;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;


typedef ButtonOptions = {
    var label : String;
    var ?labelColour : String;
    var ?iconUrl : String;
    var ?iconWidth : Float;
    var ?iconHeight : Float;
}

class Button extends AbstractToggle {

    // Button graphics elements
    var _button_content : HBox;
    var _label : Label;
    var _icon : Shape;
    var button_options : ButtonOptions;

    // background colours
    public var background(default,set) : String;
    public var backgroundHover : String;
    public var backgroundPressed : String;

    public function new( g : Graphics, ?options : ButtonOptions, ?parent_container : ContainerPanel ) {
        button_options = options;
        super( g, parent_container );
    }

    function createUiElements() {
        // stretch horizontally & verticlly to account for icon and label
        layout.setSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // setAttribute( Id, 'the_button' );

        var opt_label = 'Button';
        var opt_label_colour = null;
        var opt_icon_href = null;
        var opt_icon_w = 32.0;
        var opt_icon_h = 32.0;

        if( button_options != null ) {
            opt_label = button_options.label != null ? button_options.label : opt_label;
            opt_label_colour = button_options.labelColour != null ? button_options.labelColour : opt_label_colour;
            opt_icon_href = button_options.iconUrl != null ? button_options.iconUrl : opt_icon_href;
            opt_icon_w = button_options.iconWidth != null ? button_options.iconWidth : opt_icon_w;
            opt_icon_h = button_options.iconHeight != null ? button_options.iconHeight : opt_icon_h;
        }

        background = 'green';
        backgroundHover = 'purple';
        backgroundPressed = 'silver';

        _button_content = new HBox( graphics, this );
        // _button_content.setAttribute( Id, 'the_button_content' );
        _button_content.setBackground('none');
        _button_content.layout.setPosition( 0.px(), 0.px() );
        // _button_content stretches as much as icon + label are big
        _button_content.layout.setSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // _button_content.layout.setSize( 100.percent(), 100.percent() );
        _button_content.container.verticalAlignment = Center;
        _button_content.container.margins = 10;
        _button_content.container.gaps = 5;

        if( opt_icon_href != null ) {
            _icon = new Shape( graphics, new Image( graphics, opt_icon_href, opt_icon_w, opt_icon_h ), _button_content );
        }

        _label = new Label( graphics, opt_label, _button_content );
        _label.setFontFamily('sans-serif');
        if( opt_label_colour != null ) _label.setFill( opt_label_colour );
    }

    function set_background( value : String ) : String {
        background = value;
        showState( Normal );
        return background;
    }

    function showState( which : ToggledControlViewState ) {
        switch( which ) {
            case Normal: if( background != null ) setFill( background );
            case Toggled: if( background != null ) setFill( background );
            case Hover: if( backgroundHover != null ) setFill( backgroundHover );
            case Pressed: if( backgroundPressed != null ) setFill( backgroundPressed );
        }
    }

    function setFill( colour : String ) : Void {
        if( colour == null ) { graphics.setAttribute( element, Fill, GraphicsAttributeValue.NONE ); return; }
        graphics.setAttribute( element, Fill, colour );
    }
}
