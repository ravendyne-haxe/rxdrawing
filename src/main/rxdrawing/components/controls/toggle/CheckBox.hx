// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.controls.toggle;

import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;


class CheckBox extends AbstractCheckControl {

    var _checkmark_cmds : Array<PathInstruction> = [
        // CPathMoveTo({x: 28, y: 0}),
        // CPathLineTo({x: 4, y: 0}),
        // CPathCubicCurveTo({x: 1.79999999999999982, y: 0},{x: 0, y: 1.8},{x: 0, y: 4}),
        // CPathLineTo({x: 0, y: 28}),
        // CPathCubicCurveTo({x: 0, y: 30.2},{x: 1.8, y: 32},{x: 4, y: 32}),
        // CPathLineTo({x: 28, y: 32}),
        // CPathCubicCurveTo({x: 30.2, y: 32},{x: 32, y: 30.2},{x: 32, y: 28}),
        // CPathLineTo({x: 32, y: 4}),
        // CPathCubicCurveTo({x: 32, y: 1.79999999999999982},{x: 30.2, y: 0},{x: 28, y: 0}),
        // CPathClosePath,
        CPathMoveTo({x: 14, y: 24.828}),
        CPathLineTo({x: 6.586, y: 17.414}),
        CPathLineTo({x: 9.414, y: 14.5860000000000021}),
        CPathLineTo({x: 14, y: 19.1720000000000041}),
        CPathLineTo({x: 23.586, y: 9.58600000000000385}),
        CPathLineTo({x: 26.4139999999999979, y: 12.4140000000000033}),
        CPathLineTo({x: 13.9999999999999982, y: 24.828000000000003}),
        CPathClosePath,
    ];
    function getCheckmarkPathCommands() : Array<PathInstruction> {
        return _checkmark_cmds;
    }

    var _checkmark_border_cmds : Array<PathInstruction> = [
        CPathMoveTo({x: 28, y: 0}),
        CPathLineTo({x: 4, y: 0}),
        CPathCubicCurveTo({x: 1.79999999999999982, y: 0},{x: 0, y: 1.8},{x: 0, y: 4}),
        CPathLineTo({x: 0, y: 28}),
        CPathCubicCurveTo({x: 0, y: 30.2},{x: 1.8, y: 32},{x: 4, y: 32}),
        CPathLineTo({x: 28, y: 32}),
        CPathCubicCurveTo({x: 30.2, y: 32},{x: 32, y: 30.2},{x: 32, y: 28}),
        CPathLineTo({x: 32, y: 4}),
        CPathCubicCurveTo({x: 32, y: 1.79999999999999982},{x: 30.2, y: 0},{x: 28, y: 0}),
        CPathClosePath,
        CPathMoveTo({x: 28, y: 28}),
        CPathLineTo({x: 4, y: 28}),
        CPathLineTo({x: 4, y: 4}),
        CPathLineTo({x: 28, y: 4}),
        CPathLineTo({x: 28, y: 28}),
        CPathClosePath,
    ];
    function getCheckmarkBorderPathCommands() : Array<PathInstruction> {
        return _checkmark_border_cmds;
    }
}
