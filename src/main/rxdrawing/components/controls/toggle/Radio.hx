// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.controls.toggle;

import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;


class Radio extends AbstractCheckControl {

    var _checkmark_cmds : Array<PathInstruction> = [
        // CPathMoveTo({x: 16, y: 0}),
        // CPathCubicCurveTo({x: 7.163, y: 0},{x: 0, y: 7.163},{x: 0, y: 16}),
        // CPathCubicCurveTo({x: 0, y: 24.837},{x: 7.163, y: 32},{x: 16, y: 32}),
        // CPathCubicCurveTo({x: 24.837, y: 32},{x: 32, y: 24.837},{x: 32, y: 16}),
        // CPathCubicCurveTo({x: 32, y: 7.163},{x: 24.837, y: 0},{x: 16, y: 0}),
        // CPathClosePath,
        // CPathMoveTo({x: 16, y: 28}),
        // CPathCubicCurveTo({x: 9.37300000000000111, y: 28},{x: 4, y: 22.627},{x: 4, y: 16}),
        // CPathCubicCurveTo({x: 4, y: 9.37300000000000111},{x: 9.37300000000000111, y: 4},{x: 16, y: 4}),
        // CPathCubicCurveTo({x: 22.627, y: 4},{x: 28, y: 9.37300000000000111},{x: 28, y: 16}),
        // CPathCubicCurveTo({x: 28, y: 22.627},{x: 22.627, y: 28},{x: 16, y: 28}),
        // CPathClosePath,
        CPathMoveTo({x: 10, y: 16}),
        CPathCubicCurveTo({x: 10, y: 12.686},{x: 12.686, y: 10},{x: 16, y: 10}),
        CPathCubicCurveTo({x: 19.314, y: 10},{x: 22, y: 12.686},{x: 22, y: 16}),
        CPathCubicCurveTo({x: 22, y: 19.314},{x: 19.314, y: 22},{x: 16, y: 22}),
        CPathCubicCurveTo({x: 12.686, y: 22},{x: 10, y: 19.314},{x: 10, y: 16}),
        CPathClosePath,
    ];
    function getCheckmarkPathCommands() : Array<PathInstruction> {
        return _checkmark_cmds;
    }

    var _checkmark_border_cmds : Array<PathInstruction> = [
        CPathMoveTo({x: 16, y: 0}),
        CPathCubicCurveTo({x: 7.163, y: 0},{x: 0, y: 7.163},{x: 0, y: 16}),
        CPathCubicCurveTo({x: 0, y: 24.837},{x: 7.163, y: 32},{x: 16, y: 32}),
        CPathCubicCurveTo({x: 24.837, y: 32},{x: 32, y: 24.837},{x: 32, y: 16}),
        CPathCubicCurveTo({x: 32, y: 7.163},{x: 24.837, y: 0},{x: 16, y: 0}),
        CPathClosePath,
        CPathMoveTo({x: 16, y: 28}),
        CPathCubicCurveTo({x: 9.37300000000000111, y: 28},{x: 4, y: 22.627},{x: 4, y: 16}),
        CPathCubicCurveTo({x: 4, y: 9.37300000000000111},{x: 9.37300000000000111, y: 4},{x: 16, y: 4}),
        CPathCubicCurveTo({x: 22.627, y: 4},{x: 28, y: 9.37300000000000111},{x: 28, y: 16}),
        CPathCubicCurveTo({x: 28, y: 22.627},{x: 22.627, y: 28},{x: 16, y: 28}),
        CPathClosePath,
        // CPathMoveTo({x: 10, y: 16}),
        // CPathCubicCurveTo({x: 10, y: 12.686},{x: 12.686, y: 10},{x: 16, y: 10}),
        // CPathCubicCurveTo({x: 19.314, y: 10},{x: 22, y: 12.686},{x: 22, y: 16}),
        // CPathCubicCurveTo({x: 22, y: 19.314},{x: 19.314, y: 22},{x: 16, y: 22}),
        // CPathCubicCurveTo({x: 12.686, y: 22},{x: 10, y: 19.314},{x: 10, y: 16}),
        // CPathClosePath,
    ];
    function getCheckmarkBorderPathCommands() : Array<PathInstruction> {
        return _checkmark_border_cmds;
    }
}
