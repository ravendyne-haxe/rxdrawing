// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.controls.toggle;

import rxdrawing.elements.Path;
import rxdrawing.components.containers.StackingContainer;
import rxlayout.types.SizeValue;
import rxdrawing.components.controls.toggle.AbstractToggle;
import rxdrawing.components.containers.HBox;
import rxdrawing.components.containers.ContainerPanel;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;


typedef CheckControlOptions = {
    var label : String;
    var ?labelColour : String;
    var ?size : Float;
}

abstract class AbstractCheckControl extends AbstractToggle {

    // CheckBox graphics elements
    var _control_content : HBox;
    var _label : Label;

    var _checkmark_content : StackingContainer;

    var _checkmark : Shape;
    var _checkmark_path : Path;
    abstract function getCheckmarkPathCommands() : Array<PathInstruction>;

    var _checkmark_border : Shape;
    var _checkmark_border_path : Path;
    abstract function getCheckmarkBorderPathCommands() : Array<PathInstruction>;

    var checkbox_options : CheckControlOptions;

    // icon and label colours
    public var colour(default,set) : String;
    public var colourHover : String;
    public var colourPressed : String;

    public function new( g : Graphics, ?options : CheckControlOptions, ?parent_container : ContainerPanel ) {
        checkbox_options = options;
        super( g, parent_container );
    }

    function createUiElements() {
        // stretch horizontally & verticlly to account for icon and label
        layout.setSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // setAttribute( Id, 'the_checkbox' );

        var opt_label = 'Checkbox';
        var opt_label_colour = 'black';
        var opt_checkmark_size = 32.0;

        if( checkbox_options != null ) {
            opt_label = checkbox_options.label != null ? checkbox_options.label : opt_label;
            opt_label_colour = checkbox_options.labelColour != null ? checkbox_options.labelColour : opt_label_colour;
            opt_checkmark_size = checkbox_options.size != null ? checkbox_options.size : opt_checkmark_size;
        }

        _control_content = new HBox( graphics, this );
        // _control_content.setAttribute( Id, 'the_checkbox_content' );
        // _control_content.setBackground('fuchsia');
        _control_content.setBackground('none');
        _control_content.layout.setPosition( 0.px(), 0.px() );
        // _control_content stretches as much as icon + label are big
        _control_content.layout.setSize( SizeValue.UNDEFINED, SizeValue.UNDEFINED );
        // _control_content.layout.setSize( 100.percent(), 100.percent() );
        _control_content.container.verticalAlignment = Center;
        _control_content.container.margins = 10;
        _control_content.container.gaps = 5;

        // _control_content
        {
            _checkmark_content = new StackingContainer( graphics, _control_content );
            _checkmark_content.layout.setSize( opt_checkmark_size.px(), opt_checkmark_size.px() );
            _checkmark_content.setAttribute( Opacity, '0' );

            // _checkmark_content
            {
                _checkmark_border_path = new Path( graphics, getCheckmarkBorderPathCommands(), { width: opt_checkmark_size, height: opt_checkmark_size } );
                _checkmark_border_path.setStroke('none', 0);
                _checkmark_border_path.setFill(opt_label_colour);
                _checkmark_border = new Shape( graphics, _checkmark_border_path, _checkmark_content );
                // _checkmark_border.layout.setSize( 100.percent(), 100.percent() );

                _checkmark_path = new Path( graphics, getCheckmarkPathCommands(), { width: opt_checkmark_size, height: opt_checkmark_size } );
                _checkmark_path.setStroke('none', 0);
                _checkmark_path.setFill(opt_label_colour);
                _checkmark = new Shape( graphics, _checkmark_path, _checkmark_content );
                // _checkmark.layout.setSize( 100.percent(), 100.percent() );

                if( ! isToggled ) _checkmark.setAttribute( Visible, 'false' );
            }

            _label = new Label( graphics, opt_label, _control_content );
            _label.setFontFamily('sans-serif');
            if( opt_label_colour != null ) _label.setFill( opt_label_colour );
        }

        colour = opt_label_colour;

        graphics.setAttribute( element, Opacity, '0' );
    }

    function set_colour( value : String ) : String {
        colour = value;
        _label.setFill( colour );
        showState( Normal );
        return colour;
    }

    function showState( which : ToggledControlViewState ) {
        switch( which ) {
            case Normal: if( colour != null ) {
                _checkmark_border_path.setFill(colour);
                _checkmark_path.setFill(colour);
                _checkmark.setAttribute( Visible, 'false' );
            }
            case Toggled: if( colour != null ) {
                _checkmark_border_path.setFill(colour);
                _checkmark_path.setFill(colour);
                _checkmark.setAttribute( Visible, 'true' );
            }
            case Hover: if( colourHover != null ) {
                _checkmark_border_path.setFill(colourHover);
                _checkmark_path.setFill(colourHover);
            }
            case Pressed: if( colourPressed != null ) {
                _checkmark_border_path.setFill(colourPressed);
                _checkmark_path.setFill(colourPressed);
            }
        }
    }
}
