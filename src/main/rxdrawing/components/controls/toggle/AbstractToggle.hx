// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.controls.toggle;

import rxdrawing.components.containers.AbstractContainer;
import rxdrawing.components.controls.AbstractControl;
import rxdrawing.elements.Rectangle;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;
using rxlayout.types.SizeBoxTools;

enum ToggledControlViewState {
    Normal;
    Toggled;
    Hover;
    Pressed;
}

abstract class AbstractToggle extends AbstractControl {

    var _event_target : Shape;

    // click handling
    public var onclick(never,set) : GraphicsMouseEvent -> Void;
    function set_onclick( v : GraphicsMouseEventCallback ) : GraphicsMouseEventCallback {
        _onclick = v;
        graphics.removeEventListener( _event_target.element, MouseClick, onclickHnd );
        onclickHnd = null;
        if( _onclick != null ) onclickHnd = graphics.addMouseEventListener( _event_target.element, MouseClick, _onclick);
        return _onclick;
    }
    var _onclick : GraphicsMouseEventCallback;
    var onclickHnd : GraphicsEventHnd;

    public var isToggled(default,null) : Bool;

    public function new( g : Graphics, parent_container : AbstractContainer ) {
        super( g );

        isToggled = false;
        _onclick = null;
        onclickHnd = null;
        _event_target = null;

        container = new rxlayout.containers.StackingContainer( this );
        layout = container;

        createUiElements();

        if( _event_target == null ) {
            _event_target = new Shape( g, new Rectangle( g, 0, 0 ), this );
            _event_target.layout.setPositionAndSize( 0.px(), 0.px(), 100.percent(), 100.percent() );
            _event_target.setAttribute( Opacity, '0' );
        }

        graphics.addMouseEventListener( _event_target.element, MouseDown, (ev) -> { showState( Pressed ); });
        graphics.addMouseEventListener( _event_target.element, MouseUp, (ev) -> { isToggled = !isToggled; showState( isToggled ? Toggled : Normal ); });
        graphics.addMouseEventListener( _event_target.element, MouseEnter, (ev) -> { showState( Hover ); });
        graphics.addMouseEventListener( _event_target.element, MouseLeave, (ev) -> { showState( isToggled ? Toggled : Normal ); });

        showState( Normal );

        setParent( parent_container );
    }

    abstract function createUiElements() : Void;

    abstract function showState( state : ToggledControlViewState ) : Void;
}
