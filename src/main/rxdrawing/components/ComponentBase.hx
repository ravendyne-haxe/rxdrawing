// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components;

import rxlayout.types.IViewElement;
import rxlayout.types.LayoutBox;
import rxlayout.base.LayoutContainer;
import rxlayout.types.ILayoutContainer;
import rxdrawing.core.graphics.Graphics.GraphicsTransformOperation;
import rxdrawing.core.DrawingNode;

using rxdrawing.RxDrawingTools;

class ComponentBase extends DrawingNode implements IViewElement {

    public var layout(default,null) : ILayoutContainer;
    public var container(default,null) : LayoutContainer;

    private var _left : Float;
    private var _top : Float;
    private var _width : Float;
    private var _height : Float;

    //
    // These do nothing by design.
    // All moving/resizing of components should be done
    // by using ILayoutContainer.setPosition/Size/Box() methods.
    //
    // Actual changes to x,y,width,height properties and to the
    // element property's position/size should be done inside the
    // IViewElement.setBox() method.
    //
    override private function set_x(v:Float) : Float { return _x; }
    override private function set_y(v:Float) : Float { return _y; }
    public function resizeTo( width : Float, height : Float ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function reshapeMoveBy( dx : Float, dy : Float ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function reshapeScale( sx : Float, sy : Float ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function reshapeRotateBy( angle : Float ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function moveTo( x : Float, y : Float ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function transform( ops : Array<GraphicsTransformOperation> ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function clearTransform() : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function setOrigin( x : Float, y : Float ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function setScale( sx : Float, sy : Float ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }
    public function setRotation( angle : Float ) : Void { throw new UnsupportedOperationException("Use 'layout' methods"); }

    override private function registerChild( child : DrawingNode ) {
        super.registerChild( child );
        if( Std.isOfType( child, ComponentBase ) ) {
            var kid = cast( child, ComponentBase );
            layout.addChildren([ kid.layout ]);
        }
    }

    override private function removeChild( child : DrawingNode ) {
        super.removeChild( child );
        if( Std.isOfType( child, ComponentBase ) ) {
            var kid = cast( child, ComponentBase );
            layout.removeChildren([ kid.layout ]);
        }
    }

    public function setBox( box : LayoutBox ) : Void {
        _left = box.x.isNumber() ? box.x : _left;
        _top = box.y.isNumber() ? box.y : _top;
        _width = box.width.isNumber() ? box.width : _width;
        _height = box.height.isNumber() ? box.height : _height;
    }

}
