// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.containers;

import rxdrawing.core.graphics.Graphics;

class HBox extends ContainerPanel {
    public function new( g : Graphics, gap : Float = 0.0, ?parent_container : ComponentBase ) {
        super( g );

        container = new rxlayout.containers.HBox( gap, this );
        layout = container;

        setParent( parent_container );
    }
}
