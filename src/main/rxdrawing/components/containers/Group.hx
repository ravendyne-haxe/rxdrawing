// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.containers;

import rxlayout.types.LayoutBox;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Group extends AbstractContainer {

    public function new( g : Graphics, ?parent_container : ComponentBase ) {
        super( g );
        container = new rxlayout.containers.Container( this );
        layout = container;

        _left = 0;
        _top = 0;
        _width = 0;
        _height = 0;

        element = g.createGroup([]);

        setParent( parent_container );
    }

    override public function setBox( box : LayoutBox ) : Void {
        super.setBox( box );

        // trace('Group -> setBox', box );
        graphics.setOrigin( element, _left, _top );
        // we can't set width/height of a graphics group
    }

}
