// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.containers;

import rxdrawing.core.graphics.Graphics;

class Container extends ContainerPanel {
    public function new( g : Graphics, ?parent_container : ComponentBase ) {
        super( g );

        container = new rxlayout.containers.Container( this );
        layout = container;

        setParent( parent_container );
    }
}
