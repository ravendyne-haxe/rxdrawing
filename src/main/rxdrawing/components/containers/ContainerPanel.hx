// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.containers;

import rxlayout.types.LayoutBox;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

// impelements ViewElement and is base for all
// containers, it draws the container's ViewElement
// in a form of a rectangular panel (background)
abstract class ContainerPanel extends AbstractContainer {

    var _options : PanelOptions;

    public function new( g : Graphics, ?options : PanelOptions ) {
        super( g );
        container = null;
        layout = null;

        _options = options;
        _left = 0;
        _top = 0;
        _width = 0;
        _height = 0;

        // element = g.createRect( 0, 0, _width, _height );
        element = g.createPanel( _width, _height, 0, 0, _options );

        setAttributes([
            Fill => 'fuchsia',
        ]);
    }

    override public function setBox( box : LayoutBox ) : Void {
        super.setBox( box );

        graphics.setOrigin( element, _left, _top );
        // graphics.updateRect( element, 0, 0, _width, _height );
        graphics.updatePanel( element, _width, _height, 0, 0, _options );
    }

    public function setBorder( thickness : Float, colour : String = 'black', style : String = 'solid' ) : Void {
        if( ! thickness.isNumber() ) { graphics.setAttribute( element, Stroke, GraphicsAttributeValue.NONE ); return; }

        graphics.setAttribute( element, StrokeWidth, '${thickness}' );
        graphics.setAttribute( element, Stroke, colour );
    }

    public function setBackground( colour : String ) : Void {
        if( colour == null ) { graphics.setAttribute( element, Fill, GraphicsAttributeValue.NONE ); return; }
        graphics.setAttribute( element, Fill, colour );
    }
}
