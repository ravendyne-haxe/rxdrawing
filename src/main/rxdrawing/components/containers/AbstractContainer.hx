// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.components.containers;

import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

abstract class AbstractContainer extends ComponentBase {

    public function new( g : Graphics ) {
        super( g );
        container = null;
        layout = null;

        _left = 0;
        _top = 0;
        _width = 0;
        _height = 0;

        element = null;
    }
}
