// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Image extends AbstractElement {

    public var left (get,set) : Float;
    public var top (get,set) : Float;
    public var width (get,set) : Float;
    public var height (get,set) : Float;

    private var _left : Float;
    private var _top : Float;
    private var _width : Float;
    private var _height : Float;
    private var _href : String;

    private function get_left() : Float { return _left; }
    private function get_top() : Float { return _top; }
    private function get_width() : Float { return _width; }
    private function get_height() : Float { return _height; }

    public function new( g : Graphics, href : String, width : Float, height : Float, ?parent_node : DrawingNode ) {
        super( g );

        width = width.isNumber() ? width : 0.0;
        height = height.isNumber() ? height : 0.0;

        _width = width;
        _height = height;
        _left = 0;
        _top = 0;
        _href = href;

        element = graphics.createImage( _href, _width, _height, _left, _top );

        setAttributes([
            Fill => GraphicsAttributeValue.NONE,
        ]);

        setParent( parent_node );
    }

    private function set_left(v:Float) : Float {
        v.assertValidNumber('left');
        _left = v;
        graphics.updateImage( element, _href, _width, _height, _left, _top );
        return _left;
    }
    private function set_top(v:Float) : Float {
        v.assertValidNumber('top');
        _top = v;
        graphics.updateImage( element, _href, _width, _height, _left, _top );
        return _top;
    }

    private function set_width(v:Float) : Float {
        v.assertValidNumber('width');
        _width = v;
        graphics.updateImage( element, _href, _width, _height );
        return _width;
    }
    private function set_height(v:Float) : Float {
        v.assertValidNumber('height');
        _height = v;
        graphics.updateImage( element, _href, _width, _height );
        return _height;
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        _width = width;
        _height = height;
        graphics.updateImage( element, _href, _width, _height );
    }

    public function moveTo( x : Float, y : Float ) : Void {
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _left = x;
        _top = y;
        graphics.updateImage( element, _href, _width, _height, _left, _top );
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {
        // noop
    }

    public function reshapeRotateBy( angle : Float ) : Void {
        // noop
    }

}
