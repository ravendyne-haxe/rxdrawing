// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Panel extends AbstractElement {

    public var left (get,set) : Float;
    public var top (get,set) : Float;
    public var width (get,set) : Float;
    public var height (get,set) : Float;

    private var _left : Float;
    private var _top : Float;
    private var _width : Float;
    private var _height : Float;

    private function get_left() : Float { return _left; }
    private function get_top() : Float { return _top; }
    private function get_width() : Float { return _width; }
    private function get_height() : Float { return _height; }

    private var _options : PanelOptions;

    public function new( g : Graphics, width : Float, height : Float, ?options : PanelOptions, ?parent_node : DrawingNode ) {
        super( g );

        _options = options;
        _width = width;
        _height = height;
        _left = 0;
        _top = 0;

        element = g.createPanel( width, height, options );
        setAttributes([
            Fill => 'fuchsia',
            Stroke => GraphicsAttributeValue.NONE,
        ]);

        setParent( parent_node );
    }

    private function set_left(v:Float) : Float {
        v.assertValidNumber('left');
        _left = v;
        graphics.updatePanel( element,_width, _height, _left, _top, _options );
        return _left;
    }
    private function set_top(v:Float) : Float {
        v.assertValidNumber('top');
        _top = v;
        graphics.updatePanel( element,_width, _height, _left, _top, _options );
        return _top;
    }

    private function set_width(v:Float) : Float {
        v.assertValidNumber('width');
        _width = v;
        graphics.updatePanel( element,_width, _height, _left, _top, _options );
        return _width;
    }
    private function set_height(v:Float) : Float {
        v.assertValidNumber('height');
        _height = v;
        graphics.updatePanel( element,_width, _height, _left, _top, _options );
        return _height;
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        _width = width;
        _height = height;
        graphics.updatePanel( element,_width, _height, _left, _top, _options );
    }

    public function moveTo( x : Float, y : Float ) : Void {
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _left = x;
        _top = y;
        graphics.updatePanel( element,_width, _height, _left, _top, _options );
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {
        resizeTo( _width * sx, _height * sy );
    }

    public function reshapeRotateBy( angle : Float ) : Void {
        // TODO
    }
}
