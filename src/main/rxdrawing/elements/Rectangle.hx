// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Rectangle extends AbstractElement {

    public var left (get,set) : Float;
    public var top (get,set) : Float;
    public var width (get,set) : Float;
    public var height (get,set) : Float;

    private var _left : Float;
    private var _top : Float;
    private var _width : Float;
    private var _height : Float;

    private function get_left() : Float { return _left; }
    private function get_top() : Float { return _top; }
    private function get_width() : Float { return _width; }
    private function get_height() : Float { return _height; }

    /**
     * X-radius of the rectangle rounded corner
     */
    public var rx (get,set) : Float;
    /**
     * Y-radius of the rectangle rounded corner
     */
    public var ry (get,set) : Float;

    private var _rx : Float;
    private var _ry : Float;

    public function new( g : Graphics, width : Float, height : Float, ?rx : Float, ?ry : Float, ?parent_node : DrawingNode ) {
        super( g );

        width = width.isNumber() ? width : 0.0;
        height = height.isNumber() ? height : 0.0;
        rx = rx.isNumber() ? rx : 0.0;
        ry = ry.isNumber() ? ry : 0.0;

        _width = width;
        _height = height;
        _left = 0;
        _top = 0;
        _rx = rx;
        _ry = ry;

        element = g.createRect( _left, _top, _width, _height, _rx, _ry );
        setAttributes([
            Fill => 'fuchsia',
        ]);

        setParent( parent_node );
    }

    private function get_rx() : Float { return _rx; }
    private function set_rx( v : Float ) : Float {
        v.assertValidNumber('rx');
        _rx = v;
        graphics.updateRect( element, _left, _top, _width, _height, _rx, _ry );
        return _rx;
    }
    private function get_ry() : Float { return _ry; }
    private function set_ry( v : Float ) : Float {
        v.assertValidNumber('ry');
        _ry = v;
        graphics.updateRect( element, _left, _top, _width, _height, _rx, _ry );
        return _ry;
    }

    private function set_left(v:Float) : Float {
        v.assertValidNumber('left');
        _left = v;
        graphics.updateRect( element, _left, _top, _width, _height, _rx, _ry );
        return _left;
    }
    private function set_top(v:Float) : Float {
        v.assertValidNumber('top');
        _top = v;
        graphics.updateRect( element, _left, _top, _width, _height, _rx, _ry );
        return _top;
    }

    private function set_width(v:Float) : Float {
        v.assertValidNumber('width');
        _width = v;
        graphics.updateRect( element, _left, _top, _width, _height, _rx, _ry );
        return _width;
    }
    private function set_height(v:Float) : Float {
        v.assertValidNumber('height');
        _height = v;
        graphics.updateRect( element, _left, _top, _width, _height, _rx, _ry );
        return _height;
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        _width = width;
        _height = height;
        graphics.updateRect( element, _left, _top, _width, _height, _rx, _ry );
    }

    public function moveTo( x : Float, y : Float ) : Void {
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _left = x;
        _top = y;
        graphics.updateRect( element, _left, _top, _width, _height, _rx, _ry );
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {
        resizeTo( _width * sx, _height * sy );
    }

    public function reshapeRotateBy( angle : Float ) : Void {
        // noop
    }

}
