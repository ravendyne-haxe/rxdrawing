// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Line extends AbstractElement {
    /**
     * X-coordinate of the line **start** point
     */
    public var x1 (get,set) : Float;
    /**
     * Y-coordinate of the line **start** point
     */
    public var y1 (get,set) : Float;
    /**
     * X-coordinate of the line **end** point
     */
    public var x2 (get,set) : Float;
    /**
     * Y-coordinate of the line **end** point
     */
    public var y2 (get,set) : Float;

    private var _x1 : Float;
    private var _y1 : Float;
    private var _x2 : Float;
    private var _y2 : Float;

    public function new( g : Graphics, x1 : Float, y1 : Float, x2 : Float, y2 : Float, ?parent_node : DrawingNode ) {
        super( g );

        x1 = x1.isNumber() ? x1 : 0.0;
        y1 = y1.isNumber() ? y1 : 0.0;
        x2 = x2.isNumber() ? x2 : 0.0;
        y2 = y2.isNumber() ? y2 : 0.0;

        _x1 = x1;
        _y1 = y1;
        _x2 = x2;
        _y2 = y2;

        element = g.createLine( x1, y1, x2, y2 );
        setAttributes([
            Stroke => 'fuchsia',
            StrokeWidth => '2',
        ]);

        setParent( parent_node );
    }

    private function get_x1() : Float { return _x1; }
    private function set_x1( v : Float ) : Float {
        v.assertValidNumber('x1');
        _x1 = v;
        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
        return _x1;
    }
    private function get_y1() : Float { return _y1; }
    private function set_y1( v : Float ) : Float {
        v.assertValidNumber('y1');
        _y1 = v;
        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
        return _y1;
    }

    private function get_x2() : Float { return _x2; }
    private function set_x2( v : Float ) : Float {
        v.assertValidNumber('x2');
        _x2 = v;
        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
        return _x2;
    }
    private function get_y2() : Float { return _y2; }
    private function set_y2( v : Float ) : Float {
        v.assertValidNumber('y2');
        _y2 = v;
        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
        return _y2;
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        // throw new UnsupportedOperationException('resize not implemented');
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        var deltaw = width - Math.abs( x2 - x1 );
        var deltah = height - Math.abs( y2 - y1 );

        if( x2 > x1 ) {
            _x2 += deltaw;
        } else {
            _x1 += deltaw;
        }
        if( y2 > y1 ) {
            _y2 += deltah;
        } else {
            _y1 += deltah;
        }

        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
    }

    public function moveTo( x : Float, y : Float ) : Void {
        // throw new UnsupportedOperationException('moveTo not implemented');
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        var l = Math.min( _x1, _x2 );
        var t = Math.min( _y1, _y2 );
        var dx = x - l;
        var dy = y - t;
        _x1 += dx;
        _y1 += dy;
        _x2 += dx;
        _y2 += dy;

        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
    }

    public function setStart( x : Float, y : Float ) : Void {
        // throw new UnsupportedOperationException('moveTo not implemented');
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _x1 = x;
        _y1 = y;

        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
    }

    public function setEnd( x : Float, y : Float ) : Void {
        // throw new UnsupportedOperationException('moveTo not implemented');
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _x2 = x;
        _y2 = y;

        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {

        var p1 = reshapeScaleAPoint( { x:_x1, y:_y1 }, sx, sy );
        var p2 = reshapeScaleAPoint( { x:_x2, y:_y2 }, sx, sy );

        _x1 = p1.x;
        _y1 = p1.y;
        _x2 = p2.x;
        _y2 = p2.y;

        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
    }

    public function reshapeRotateBy( angle : Float ) : Void {

        var p1 = reshapeRotateAPoint( { x:_x1, y:_y1 }, angle );
        var p2 = reshapeRotateAPoint( { x:_x2, y:_y2 }, angle );

        _x1 = p1.x;
        _y1 = p1.y;
        _x2 = p2.x;
        _y2 = p2.y;

        graphics.updateLine( element, _x1, _y1, _x2, _y2 );
    }

}
