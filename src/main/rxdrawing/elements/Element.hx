// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Element extends AbstractElement {

    public function new( g : Graphics, element : GraphicsElement, ?parent_node : DrawingNode ) {
        super( g );

        _x = 0.0;
        _y = 0.0;

        this.element = element;

        setParent( parent_node );
    }

    override public function setFill( value : String ) {}

    override public function setStroke( colour : String, width : Float = 1.0 ) {}

    public function resizeTo( width : Float, height : Float ) : Void {
        // noop
    }

    public function moveTo( x : Float, y : Float ) : Void {
        // noop
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {
        // noop
    }

    public function reshapeRotateBy( angle : Float ) : Void {
        // noop
    }
}
