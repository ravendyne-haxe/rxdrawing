// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Polygon extends Polyline {

    override private function convertToPathInstructions() {
        var r = super.convertToPathInstructions();
        r.push( PathInstruction.CPathClosePath );
        return r;
    }
}
