// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Circle extends AbstractElement {
    /**
     * radius of the circle
     */
     public var r (get,set) : Float;
     /**
      * X-coordinate of the circle **center** point
      */
     public var cx (get,set) : Float;
     /**
      * Y-coordinate of the circle **center** point
      */
     public var cy (get,set) : Float;

     private var _r : Float;
     private var _cx : Float;
     private var _cy : Float;

    public function new( g : Graphics, r : Float, ?cx : Float, ?cy : Float, ?parent_node : DrawingNode ) {
        super( g );

        r = r.isNumber() ? r : 0.0;
        cx = cx.isNumber() ? cx : 0.0;
        cy = cy.isNumber() ? cy : 0.0;

        _r = r;
        _cx = cx;
        _cy = cy;

        element = g.createCircle( cx, cy, r );
        setAttributes([
            Fill => 'fuchsia',
        ]);

        setParent( parent_node );
    }

    private function get_r() : Float { return _r; }
    private function set_r( v : Float ) : Float {
        v.assertValidNumber('r');
        _r = v;
        graphics.updatCircle( element, _cx, _cy, _r );
        return _r;
    }

    private function get_cx() : Float { return _cx; }
    private function set_cx( v : Float ) : Float {
        v.assertValidNumber('cx');
        _cx = v;
        graphics.updatCircle( element, _cx, _cy, _r );
        return _cx;
    }
    private function get_cy() : Float { return _cy; }
    private function set_cy( v : Float ) : Float {
        v.assertValidNumber('ct');
        _cy = v;
        graphics.updatCircle( element, _cx, _cy, _r );
        return _cy;
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        _cx = width / 2;
        _cy = height / 2;
        _r = Math.min( width , height ) / 2;
        graphics.updatCircle( element, _cx, _cy, _r );
    }

    public function moveTo( x : Float, y : Float ) : Void {
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _cx = x;
        _cy = y;
        graphics.updatCircle( element, _cx, _cy, _r );
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {
        // noop
    }

    public function reshapeRotateBy( angle : Float ) : Void {
        // noop
    }

}
