// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Text extends AbstractElement {

    public var left (get,set) : Float;
    public var top (get,set) : Float;

    private var _left : Float;
    private var _top : Float;

    private function get_left() : Float { return _left; }
    private function get_top() : Float { return _top; }

    var _text : String;

    public function new( g : Graphics, text : String, ?parent_node : DrawingNode ) {
        super( g );

        _left = 0;
        _top = 0;
        _text = text;

        element = graphics.createText( text, _left, _top );

        setAttributes([
            Fill=>'black',
        ]);

        setParent( parent_node );
    }

    public function setText( text : String ) {
        graphics.updateText( element, text, _left, _top );
    }

    /**
     * Set font family
     *
     * @param fontFamily
     * @see https://www.w3.org/TR/css-fonts-3/#font-family-prop
     */
    public function setFontFamily( fontFamily : String ) {
        setAttribute( FontFamily, fontFamily );
    }

    /**
     * Set font size
     *
     * @param fontSize
     * @see https://www.w3.org/TR/css-fonts-3/#font-size-prop
     */
    public function setFontSize( fontSize : Int ) {
        setAttribute( FontSize, '${fontSize}' );
    }

    /**
     * Set font weight
     *
     * @param fontWeight
     * @see https://www.w3.org/TR/css-fonts-3/#font-weight-prop
     */
     public function setFontWeight( fontWeight : String ) {
        setAttribute( FontWeight, fontWeight );
    }

    /**
     * Set font style
     *
     * @param fontStyle
     * @see https://www.w3.org/TR/css-fonts-3/#font-style-prop
     */
     public function setFontStyle( fontStyle : String ) {
        setAttribute( FontStyle, fontStyle );
    }

    private function set_left(v:Float) : Float {
        v.assertValidNumber('left');
        _left = v;
        graphics.updateText( element, _text, _left, _top );
        return _left;
    }
    private function set_top(v:Float) : Float {
        v.assertValidNumber('top');
        _top = v;
        graphics.updateText( element, _text, _left, _top );
        return _top;
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        // throw new UnsupportedOperationException('resize not supported for Text');
        // width.assertValidNumber('width');
        // height.assertValidNumber('height');
    }

    public function moveTo( x : Float, y : Float ) : Void {
        // throw new UnsupportedOperationException('resize not implemented');
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _left = x;
        _top = y;
        graphics.updateText( element, _text, _left, _top );
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {
        // noop
    }

    public function reshapeRotateBy( angle : Float ) : Void {
        // noop
    }

}
