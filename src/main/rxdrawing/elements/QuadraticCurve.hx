// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class QuadraticCurve extends AbstractElement {
    /**
     * Start point of the quadratic curve
     */
     public var p0 (get,set) : GraphicsPoint;
     /**
      * Control point of the quadratic curve
      */
     public var p1 (get,set) : GraphicsPoint;
     /**
      * End point of the quadratic curve
      */
     public var p2 (get,set) : GraphicsPoint;

     private var _p0 : GraphicsPoint;
     private var _p1 : GraphicsPoint;
     private var _p2 : GraphicsPoint;

     var _left : Float;
     var _top : Float;
     // REFERENCE width and height, NOT the same as actual AABB
     var _width : Float;
     var _height : Float;

     public function new( g : Graphics, p0 : GraphicsPoint, p1 : GraphicsPoint, p2 : GraphicsPoint, ?ref_size: GraphicsSize, ?parent_node : DrawingNode ) {
        super( g );

        _left = 0;
        _top = 0;
        _width = 100;
        _height = 100;
        if( ref_size != null ) {
            _width = ref_size.width.isNumber() ? ref_size.width : _width;
            _height = ref_size.height.isNumber() ? ref_size.height : _height;
        }

        _p0 = p0;
        _p1 = p1;
        _p2 = p2;

        element = g.createQuadraticCurve( p0, p1, p2 );

        setAttributes([
            Stroke => 'fuchsia',
            StrokeWidth => '2',
            Fill => GraphicsAttributeValue.NONE,
        ]);

        setParent( parent_node );
    }

    private function get_p0() : GraphicsPoint { return _p0; }
    private function set_p0( v : GraphicsPoint ) : GraphicsPoint {
        v.assertValidPoint('p0');
        _p0 = v;
        graphics.updateQuadraticCurve( element, _p0, _p1, _p2 );
        return _p0;
    }
    private function get_p1() : GraphicsPoint { return _p1; }
    private function set_p1( v : GraphicsPoint ) : GraphicsPoint {
        v.assertValidPoint('p1');
        _p1 = v;
        graphics.updateQuadraticCurve( element, _p0, _p1, _p2 );
        return _p1;
    }
    private function get_p2() : GraphicsPoint { return _p2; }
    private function set_p2( v : GraphicsPoint ) : GraphicsPoint {
        v.assertValidPoint('p2');
        _p2 = v;
        graphics.updateQuadraticCurve( element, _p0, _p1, _p2 );
        return _p2;
    }

    public function update() {
        graphics.updateQuadraticCurve( element, _p0, _p1, _p2 );
    }

    // QuadraticCurve's AABB is whatever is set in constructor and
    // by parameters to calls to moveTo()/resizeTo()
    override public function getAABBox() : GraphicsBox {
        // trace('QuadraticCurve getAABBox( ${_left}, ${_top}, ${_width}, ${_height} )');
        return {
            x: _left,
            y: _top,
            width: _width,
            height: _height,
        }
        // return graphics.getElementBox( element );
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        // throw new UnsupportedOperationException('resize not implemented');
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        // DON'T scale to 0.0 on any dimension
        if( width.isCloseTo( 0 ) || height.isCloseTo( 0 ) ) {
            // trace('PATH resizeTo() not resizing to 0');
            // trace(haxe.CallStack.toString(haxe.CallStack.callStack()));
            return;
        }

        var sx = width / _width;
        var sy = height / _height;
        _width = width;
        _height = height;

        final scaleAPoint : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return {
                x: ( pt.x - _left ) * sx + _left,
                y: ( pt.y - _top ) * sy + _top,
            }
        }

        _p0 = scaleAPoint( _p0 );
        _p1 = scaleAPoint( _p1 );
        _p2 = scaleAPoint( _p2 );

        update();
    }

    public function moveTo( x : Float, y : Float ) : Void {
        // throw new UnsupportedOperationException('moveTo not implemented');
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        var dx = x - _left;
        var dy = y - _top;
        _left = x;
        _top = y;

        final translateAPoint : (GraphicsPoint) -> GraphicsPoint = ( pt ) -> {
            return {
                x: pt.x + dx,
                y: pt.y + dy,
            }
        }

        _p0 = translateAPoint( _p0 );
        _p1 = translateAPoint( _p1 );
        _p2 = translateAPoint( _p2 );

        update();
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {

        _p0 = reshapeScaleAPoint( _p0, sx, sy );
        _p1 = reshapeScaleAPoint( _p1, sx, sy );
        _p2 = reshapeScaleAPoint( _p2, sx, sy );

        update();
    }

    public function reshapeRotateBy( angle : Float ) : Void {

        _p0 = reshapeRotateAPoint( _p0, angle );
        _p1 = reshapeRotateAPoint( _p1, angle );
        _p2 = reshapeRotateAPoint( _p2, angle );

        update();
    }

    public function toString() : String {
        return 'QC( ${p0}, ${p1}, ${p2} )';
    }
}
