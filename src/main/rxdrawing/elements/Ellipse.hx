// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Ellipse extends AbstractElement {
    /**
     * X-radius of the ellipse
     */
    public var rx (get,set) : Float;
    /**
     * Y-radius of the ellipse
     */
    public var ry (get,set) : Float;
    /**
     * X-coordinate of the ellipse **center** point
     */
    public var cx (get,set) : Float;
    /**
     * Y-coordinate of the ellipse **center** point
     */
    public var cy (get,set) : Float;

    private var _rx : Float;
    private var _ry : Float;
    private var _cx : Float;
    private var _cy : Float;

    public function new( g : Graphics, rx : Float, ry : Float, ?cx : Float, ?cy : Float, ?parent_node : DrawingNode ) {
        super( g );

        rx = rx.isNumber() ? rx : 0.0;
        ry = ry.isNumber() ? ry : 0.0;
        cx = cx.isNumber() ? cx : 0.0;
        cy = cy.isNumber() ? cy : 0.0;

        _rx = rx;
        _ry = ry;
        _cx = cx;
        _cy = cy;

        element = g.createEllipse( cx, cy, rx, ry );

        setAttributes([
            Fill => 'fuchsia',
        ]);

        setParent( parent_node );
    }

    private function get_rx() : Float { return _rx; }
    private function set_rx( v : Float ) : Float {
        v.assertValidNumber('rx');
        _rx = v;
        graphics.updateEllipse( element, _cx, _cy, _rx, _ry );
        return _rx;
    }
    private function get_ry() : Float { return _ry; }
    private function set_ry( v : Float ) : Float {
        v.assertValidNumber('ry');
        _ry = v;
        graphics.updateEllipse( element, _cx, _cy, _rx, _ry );
        return _ry;
    }

    private function get_cx() : Float { return _cx; }
    private function set_cx( v : Float ) : Float {
        v.assertValidNumber('cx');
        _cx = v;
        graphics.updateEllipse( element, _cx, _cy, _rx, _ry );
        return _cx;
    }
    private function get_cy() : Float { return _cy; }
    private function set_cy( v : Float ) : Float {
        v.assertValidNumber('cy');
        _cy = v;
        graphics.updateEllipse( element, _cx, _cy, _rx, _ry );
        return _cy;
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        throw new UnsupportedOperationException('resize not implemented');
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        _cx = x + width / 2;
        _cy = y + height / 2;
        _rx = width / 2;
        _ry = height / 2;
        graphics.updateEllipse( element, _cx, _cy, _rx, _ry );
    }

    public function moveTo( x : Float, y : Float ) : Void {
        throw new UnsupportedOperationException('moveTo not implemented');
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _x = x;
        _y = y;
        _cx = _x + rx;
        _cy = _y + ry;
        graphics.updateEllipse( element, _cx, _cy, _rx, _ry );
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {
        // noop
    }

    public function reshapeRotateBy( angle : Float ) : Void {
        // noop
    }

}
