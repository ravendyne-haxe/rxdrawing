// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Polyline extends AbstractElement {

    public var points (get,never) : Array<GraphicsPoint>;

    private var _points : Array<GraphicsPoint>;

    public function new( g : Graphics, points : Array<GraphicsPoint>, ?parent_node : DrawingNode ) {
        super( g );

        _points = [ for( pt in points ) pt.clonep() ];

        element = g.createPath( convertToPathInstructions() );
        setAttributes([
            Fill => GraphicsAttributeValue.NONE,
            Stroke => 'fuchsia',
            StrokeWidth => '2',
        ]);

        setParent( parent_node );
    }

    private function convertToPathInstructions() {
        if( _points.length < 2 ) return [];
        var r : Array<PathInstruction>  = [];
        var first = true;
        for( pt in _points ) {
            if( first ) {
                first = false;
                r.push( PathInstruction.CPathMoveTo(pt) );
            } else {
                r.push( PathInstruction.CPathLineTo(pt) );
            }
        }
        return r;
    }

    private function get_points() : Array<GraphicsPoint> { return _points; }

    public function update() {
        graphics.updatePath( element, convertToPathInstructions() );
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        // throw new UnsupportedOperationException('resize not implemented');
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        var aabb = _points.aabb();

        var sx = aabb.width > 0.0 ? width / aabb.width : 1.0;
        var sy = aabb.height > 0.0 ? height / aabb.height : 1.0;
        // translate to 0,0
        // scale by sx,sy
        // translate back to wherever it was
        for( pt in _points ) {
            pt.x = ( pt.x - aabb.x ) * sx + aabb.x;
            pt.y = ( pt.y - aabb.y ) * sy + aabb.y;
        }
        update();
    }

    public function moveTo( x : Float, y : Float ) : Void {
        // throw new UnsupportedOperationException('moveTo not implemented');
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        var aabb = _points.aabb();

        var dx = x - aabb.x;
        var dy = y - aabb.y;
        for( pt in _points ) {
            pt.x += dx;
            pt.y += dy;
        }
        update();
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {

        for( pt in _points ) {
            var p = reshapeScaleAPoint( pt, sx, sy );
            pt.x = p.x;
            pt.y = p.y;
        }

        update();
    }

    public function reshapeRotateBy( angle : Float ) : Void {

        for( pt in _points ) {
            var p = reshapeRotateAPoint( pt, angle );
            pt.x = p.x;
            pt.y = p.y;
        }

        update();
    }
}
