// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Group extends DrawingNode {

    public function new( g : Graphics, ?parent_node : DrawingNode ) {
        super( g );

        _x = 0.0;
        _y = 0.0;

        element = g.createGroup([]);

        setParent( parent_node );
    }

    override private function registerChild( child : DrawingNode ) {
        super.registerChild( child );
    }

    override private function removeChild( child : DrawingNode ) {
        super.removeChild( child );
    }

    public function reshapeMoveBy( dx : Float, dy : Float ) : Void {
        if( ! dx.isNumber() || ! dy.isNumber() ) return;

        moveTo( x + dx, y + dy );
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {
        // noop
    }

    public function reshapeRotateBy( angle : Float ) : Void {
        // noop
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        // noop
    }

    public function moveTo( x : Float, y : Float ) : Void {
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        _x = x;
        _y = y;
        graphics.setOrigin( element, _x, _y );
    }

    public function transform( ops : Array<GraphicsTransformOperation> ) : Void {
        graphics.transform( element, ops );
    }

    public function clearTransform() : Void {
        graphics.clearTransform( element );
    }

    public function setOrigin( x : Float, y : Float ) : Void {
        moveTo( x, y );
    }

    public function setScale( sx : Float, sy : Float ) : Void {
        graphics.setScale( element, sx, sy );
    }

    public function setRotation( angle : Float ) : Void {
        graphics.setRotation( element, angle );
    }

}
