// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

class Path extends AbstractElement {

    public var commands (get,never) : Array<PathInstruction>;

    private var _commands : Array<PathInstruction>;

    var _left : Float;
    var _top : Float;
    // REFERENCE width and height, NOT the same as path's AABB
    var _width : Float;
    var _height : Float;

    public function new( g : Graphics, commands : Array<PathInstruction>, ?ref_size: GraphicsSize, ?parent_node : DrawingNode ) {
        super( g );

        _commands = [ for( cmd in commands ) cmd ];

        _left = 0;
        _top = 0;
        _width = 100;
        _height = 100;
        if( ref_size != null ) {
            _width = ref_size.width.isNumber() ? ref_size.width : _width;
            _height = ref_size.height.isNumber() ? ref_size.height : _height;
        }

        element = g.createPath( _commands );
        setAttributes([
            Fill => GraphicsAttributeValue.NONE,
            Stroke => 'fuchsia',
            StrokeWidth => '2',
        ]);

        setParent( parent_node );
    }

    private function get_commands() : Array<PathInstruction> { return _commands; }

    public function update() {
        graphics.updatePath( element, _commands );
    }

    public function next( cmd : PathInstruction ) {
        commands.push( cmd );
        return this;
    }

    // Path's AABB is whatever is set in constructor and
    // by parameters to calls to moveTo()/resizeTo()
    override public function getAABBox() : GraphicsBox {
        // trace('PATH getAABBox( ${_left}, ${_top}, ${_width}, ${_height} )');
        return {
            x: _left,
            y: _top,
            width: _width,
            height: _height,
        }
        // return graphics.getElementBox( element );
    }

    public function resizeTo( width : Float, height : Float ) : Void {
        // trace('PATH resizeTo( ${width}, ${height} ) from ( ${_width}, ${_height} )');
        width.assertValidNumber('width');
        height.assertValidNumber('height');

        // DON'T scale to 0.0 on any dimension
        if( width.isCloseTo( 0 ) || height.isCloseTo( 0 ) ) {
            // trace('PATH resizeTo() not resizing to 0');
            // trace(haxe.CallStack.toString(haxe.CallStack.callStack()));
            return;
        }

        var sx = width / _width;
        var sy = height / _height;
        _width = width;
        _height = height;

        _commands = _commands.scalePath( { x: sx,y: sy }, { x: _left,y: _top } );
        update();
    }

    public function moveTo( x : Float, y : Float ) : Void {
        // trace('PATH moveTo( ${x}, ${y} ) from ( ${_left}, ${_top} )');
        x.assertValidNumber('x');
        y.assertValidNumber('y');

        var dx = x - _left;
        var dy = y - _top;
        _left = x;
        _top = y;

        _commands = _commands.translatePath({ x: dx, y: dy });
        update();
    }

    public function reshapeScale( sx : Float, sy : Float ) : Void {

        _commands = _commands.scalePath( { x: sx,y: sy }, { x: 0.0,y: 0.0 } );
        update();
    }

    public function reshapeRotateBy( angle : Float ) : Void {

        _commands = _commands.rotatePath( angle, { x: 0.0,y: 0.0 } );
        update();
    }

    public function toString() : String {
        // return 'PATH( ${new SvgPathBuilder(_commands).build()} )';
        return 'PATH( ${_commands} )';
    }
}
