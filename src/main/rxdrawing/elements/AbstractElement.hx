// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxdrawing.elements;

import rxdrawing.core.DrawingNode;
import rxdrawing.core.graphics.Graphics;

using rxdrawing.RxDrawingTools;

abstract class AbstractElement extends DrawingNode {

    override private function registerChild( child : DrawingNode ) {}
    override function removeChild( child : DrawingNode ) {}

    public function setFill( value : String ) {
        graphics.setAttribute( element, Fill, value );
    }

    public function setStroke( colour : String, width : Float = 1.0 ) {
        graphics.setAttribute( element, Stroke, colour );
        graphics.setAttribute( element, StrokeWidth, '${width}' );
    }

    public function reshapeMoveBy( dx : Float, dy : Float ) : Void {
        if( ! dx.isNumber() || ! dy.isNumber() ) return;

        moveTo( x + dx, y + dy );
    }

    public function transform( ops : Array<GraphicsTransformOperation> ) : Void {
        graphics.transform( element, ops );
    }

    public function clearTransform() : Void {
        graphics.clearTransform( element );
    }

    public function setOrigin( x : Float, y : Float ) : Void {
        _x = x;
        _y = y;
        graphics.setOrigin( element, _x, _y );
    }

    public function setScale( sx : Float, sy : Float ) : Void {
        graphics.setScale( element, sx, sy );
    }

    public function setRotation( angle : Float ) : Void {
        graphics.setRotation( element, angle );
    }

}
