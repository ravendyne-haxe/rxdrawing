// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package svgrx.format.svgpoints;

import svgrx.util.Util;
import svgrx.format.svgpoints.Data.PointElement;

using StringTools;


class Reader {

    var _data : String;

    // TODO add units: px, em, cm etc
    final _pointsRegex = new EReg( '${Constants.decimalNumberRegexStr}\\s?,\\s?${Constants.decimalNumberRegexStr}', 'g' );

    public function new( data : String ) {
        _data = data == null ? '' : data;
    }

    public function read() : Data {

        var _pointsdData = Util.collectMatches( _pointsRegex, _data );

        var elements : Array<PointElement> = _pointsdData.map( ( val ) -> {

            var parts = val.split(',');
            var px = Std.parseFloat( parts[0].trim() );
            var py = Std.parseFloat( parts[1].trim() );

            if( Math.isNaN( px ) || Math.isNaN( py ) ) {
                return CUnknown( val );
            }

            return CPoint( px, py );
        });

        return {
            elements: elements
        }
    }
}
