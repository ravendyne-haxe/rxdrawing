// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package svgrx.format.svgpoints;


enum PointElement {

    CPoint( x : Float, y : Float );

    CUnknown( l : String );
}

typedef Data = {
    elements : Array< PointElement >
}
