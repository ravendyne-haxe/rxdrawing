// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package svgrx.format.svgpath;


enum PathElement {

    CEnd;

    CMoveTo( x : Float, y : Float, relative : Bool );
    CLineTo( x : Float, y : Float, relative : Bool );
    CLineToHorizontal( x : Float, relative : Bool );
    CLineToVertical( y : Float, relative : Bool );
    CClosePath;
    CCubicBezier( x1 : Float, y1 : Float, x2 : Float, y2 : Float, x : Float, y : Float, relative : Bool );
    CCubicBezierSmooth( x2 : Float, y2 : Float, x : Float, y : Float, relative : Bool );
    CQuadraticBezier( x1 : Float, y1 : Float, x : Float, y : Float, relative : Bool );
    CQuadraticBezierSmooth( x : Float, y : Float, relative : Bool );
    CEllipticArc( rx : Float, ry : Float, xAxisRotation : Float, largeArcFlag : Int, sweepFlag : Int, x : Float, y : Float, relative : Bool );

    CUnknown( l : String );
}

typedef Data = {
    elements : Array< PathElement >
}
