// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package svgrx.format;

class Constants {
    // floating point
    public static final numberRegexStr = '[-+]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)(?:[eE][-+]?\\d+)?';
    // 10, 10.00, .10, +10, -10
    public static final decimalNumberRegexStr = '[-+]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)';
    public static final pathCommands = 'MmZzLlHhVvCcSsQqTtAa';
}
