// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package svgrx.format.svgtransform;

import svgrx.util.Util;
import svgrx.format.svgtransform.Data;

class Reader {

    var _data : String;

    var _transformsRegex : EReg;
    var _numberRegex : EReg;

    public function new( data : String ) {
        _data = data == null ? '' : data;
        _transformsRegex = buildTransformsRegex();
        _numberRegex = new EReg( Constants.numberRegexStr, 'g' );
    }

    public function read() : Data {

        var elements : Array<TransformElement> = [];

        var matches = Util.collectMatches( _transformsRegex, _data );
        // 'matches' is an array of strings in a form "operation_name ( arg1, arg2 ... )"
        // i.e. ["translate(-10,-20)", "scale(2)", "rotate(45)", "translate(5,10)"]
        for( i in 0...matches.length ) {
            // opargs is an array of two strings: operation name and arguments between ()
            // i.e. ["translate", "-10,-20)"]
            var opargs = matches[ i ].split('(');

            // TODO: split by ',' and process each part with Util.lengthInBaseUnits?
            // TODO: or split by ',' and send strings as args, and let opToCommand call Util.lengthInBaseUnits where needed
            // opargs[1] is matched to produce args array of strings where each element is one operation argument
            // i.e. ["-10", "-20"]
            var args = Util.collectMatches( _numberRegex, opargs[ 1 ] );
            var fargs = args.map( ( val ) -> {
                var parsed = Std.parseFloat( val );
                // return ! Math.isNaN( parsed ) ? parsed : 0.0;
                return parsed;
            });

            elements.push( opToCommand( opargs[ 0 ], fargs ) );
        }

        return {
            elements: elements
        }
    }

    private static function opToCommand( op: String, args: Array<Float> ) : TransformElement {

        // https://www.w3.org/TR/css-transforms-1/#two-d-transform-functions
        switch( op ) {

            case 'matrix': {
                if( args.length < 6 ) throw 'transform "matrix()" function expects 6 arguments, got ${args.length} instead';
                return CTransformMatrix( args );
            }

            case 'translate': {
                if( args.length < 1 ) throw 'transform "translate()" function expects at least 1 argument';
                var tx = args[ 0 ];
                var ty = args.length > 1 ? args[ 1 ] : 0;
                return CTransformTranslate( tx, ty );
            }

            case 'scale': {
                if( args.length < 1 ) throw 'transform "scale()" function expects at least 1 argument';
                var sx = args[ 0 ];
                var sy = args.length > 1 ? args[ 1 ] : sx;
                return CTransformScale( sx, sy );
            }

            case 'rotate': {
                if( args.length < 1 ) throw 'transform "rotate()" function expects at least 1 argument';
                var angle = args[ 0 ];
                var cx = args.length > 1 ? args[ 1 ] : 0;
                var cy = args.length > 2 ? args[ 2 ] : 0;
                return CTransformRotate( angle, cx, cy );
            }

            case 'skewX': {
                var angle = args.length > 0 ? args[ 0 ] : 0;
                return CTransformSkewX( angle );
            }

            case 'skewY': {
                var angle = args.length > 0 ? args[ 0 ] : 0;
                return CTransformSkewY( angle );
            }

            default: {
                return CUnknown( op, args );
            }
        }
    }

    private function buildTransformsRegex() {

        var svg_transforms = [ 'matrix', 'translate', 'scale', 'rotate', 'skewX', 'skewY' ];

        // match any SVG transformation with its parameter (until final parenthese)
        // [^)]*    == anything but a closing parenthese
        // '|'.join == OR-list of SVG transformations
        svg_transforms = svg_transforms.map(
        ( val ) -> {
            // match transform name and anything after it up to, and including, ')'
            return val + '[^)]*\\)';
        });
        // join matchers for all transforms with regex OR: '|''
        var regex = svg_transforms.join('|');
        return new EReg( regex, 'g');
    }
}
