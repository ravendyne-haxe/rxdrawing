// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package svgrx.format.svgtransform;


enum TransformElement {

    CTransformMatrix( args: Array<Float> );
    CTransformTranslate( tx: Float, ty: Float );
    CTransformScale( sx: Float, sy: Float );
    CTransformRotate( angle: Float, cx: Float, cy: Float );
    CTransformSkewX( angle: Float );
    CTransformSkewY( angle: Float );

    CUnknown( op: String, args: Array<Float> );
}

typedef Data = {
    elements : Array< TransformElement >
}
