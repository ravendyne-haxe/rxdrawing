// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxsvggraphics;

import js.Browser;
import js.html.MouseEvent;
import js.html.CustomEvent;
import js.html.Event;
import js.html.Element;
import js.html.svg.Point;
import js.html.svg.SVGElement;

import rxdrawing.core.graphics.Graphics;
import rxsvggraphics.SvgGraphicsImplementation.SvgNamespaces;
import rxsvggraphics.SvgGraphicsImplementation.SVGTools;


typedef SvgStopElement = {
    offset : Float,
    stopColor: String,
    ?stopOpacity: String,
}

class SvgGraphics extends SvgGraphicsImplementation {

    public function new( container : js.html.DivElement/* , width : Float, height : Float */ ) {

        // var container = Browser.document.getElementById( containerId );

        // the only way to explicitly add namespaces other than SVG_NS
        container.innerHTML = '<svg xmlns="${SvgNamespaces.SVG_NS}" xmlns:xlink="${SvgNamespaces.XLink_NS}"></svg>';
        // this is the other way, but how to add other namespaces?
        // svg = document.createElementNS(SVG_NS,'svg');

        var _svg = container.firstElementChild;
        // var el : GraphicsElement = cast {e:_svg};
        var el : js.html.svg.GraphicsElement = cast _svg;

        SVGTools.setAttribute(el,Custom('width'), '100%');
        SVGTools.setAttribute(el,Custom('height'), '100%');
        // SVGTools.setAttribute(el,Custom('width'), '${width}');
        // SVGTools.setAttribute(el,Custom('height'), '${height}');
        // SVGTools.setAttribute(el,Custom('viewBox'), '0 0 ${width} ${height}');

        container.appendChild( _svg );

        svg = cast _svg;
        pt = svg.createSVGPoint();
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // CREATE
    //
    /////////////////////////////////////////////////////////////////////////////

    public function svgCreateDef() : GraphicsElement {
        var def = createSvgElement('def');
        return def;
    }

    public function svgCreatePattern( id : String, x : Float, y : Float, width : Float, height : Float ) : GraphicsElement {

        var pattern = createSvgElement('pattern');

        setAttribute(pattern,Custom('x'),'${x}');
        setAttribute(pattern,Custom('y'),'${y}');
        setAttribute(pattern,Custom('width'),'${width}');
        setAttribute(pattern,Custom('height'),'${height}');
        setAttribute(pattern,Id,id);
        setAttribute(pattern,Custom('patternUnits'),'userSpaceOnUse');

        return pattern;
    }

    public function svgCreateLinearGradient( id : String, stops : Array<SvgStopElement> ) : GraphicsElement {

        var ling = createSvgElement('linearGradient');

        setAttributes( ling, [
            Id=>id,
            Custom('x1')=>'0',
            Custom('y1')=>'0',
            Custom('x2')=>'1',
            Custom('y2')=>'1',
        ]);
        // setAttribute(ling,Custom('gradientUnits'),'objectBoundingBox'); // default
        for( stop in stops ) {
            var stopel = createSvgElement('stop');
            setAttribute(stopel, Custom('offset'),'${stop.offset}');
            setAttribute(stopel, Custom('stop-color'),stop.stopColor);
            if( stop.stopOpacity != null ) setAttribute(stopel, Custom('stop-opacity'),stop.stopOpacity);
            appendChild(ling,stopel);
        }
        // setAttribute(pattern,Custom('patternUnits'),'userSpaceOnUse');

        return ling;
    }
}
