// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxsvggraphics;

import js.html.WheelEvent;
import js.html.DOMMatrix;
import js.Browser;
import js.html.MouseEvent;
import js.html.CustomEvent;
import js.html.Event;
import js.html.svg.Point;
import js.html.svg.SVGElement;

import rxdrawing.core.graphics.Graphics;

using rxdrawing.core.graphics.Graphics.GraphicsTools;
using rxdrawing.RxDrawingTools;
using StringTools;

final class SvgNamespaces {
    private function new() {}
    public static var SVG_NS(default,null) = 'http://www.w3.org/2000/svg';
    public static var XLink_NS(default,null) = 'http://www.w3.org/1999/xlink';
    public static function createW3CSvgGraphicsElement( tag : String ) : js.html.svg.GraphicsElement {
        return cast Browser.document.createElementNS( SVG_NS, tag );
    }
}

final class SVGTools {
    private function new(){}

    public static inline function mouseEventKeysToKeys( e : js.html.MouseEvent ) : Int {
        var keys : Int = MouseModifierKey.NONE;
        keys |= e.altKey ? MouseModifierKey.ALT : 0;
        keys |= e.ctrlKey ? MouseModifierKey.CTRL : 0;
        keys |= e.shiftKey ? MouseModifierKey.SHIFT : 0;
        keys |= e.metaKey ? MouseModifierKey.META : 0;
        return keys;
    }

    public static function getElementBox( e : js.html.svg.GraphicsElement ) : GraphicsBox {

        // https://www.w3.org/Graphics/SVG/IG/resources/svgprimer.html#getBBox
        // https://developer.mozilla.org/en-US/docs/Web/API/SVGGraphicsElement/getBBox
        var bbox = e.getBBox();
        return {
            x: bbox.x,
            y: bbox.y,
            width: bbox.width,
            height: bbox.height,
        }

        /*
        // get position (and size) in HTML document's viewport coordinate space
        // https://developer.mozilla.org/en-us/docs/Web/API/Element/getBoundingClientRect
        var cr = sel.e.getBoundingClientRect();
        // convert position from viewport space into user coordinates in this SVG doc's viewport
        var cp = clientPointToUser( cr.x, cr.y );
        return {
            x: cp.x,
            y: cp.y,
            width: cr.width,
            height: cr.height,
        }
        */
    }

    public static function getTextElementBox( svg : SVGElement, e : js.html.svg.GraphicsElement ) : GraphicsBox {

        var cloned = e.cloneNode(true);
        svg.appendChild( cloned );
        var maybe_text : js.html.svg.GraphicsElement = cast cloned.firstChild;
        // https://www.w3.org/Graphics/SVG/IG/resources/svgprimer.html#getBBox
        // https://developer.mozilla.org/en-US/docs/Web/API/SVGGraphicsElement/getBBox
        var bbox = maybe_text.getBBox();
        svg.removeChild( cloned );
        cloned = null;

        // trace('SVGTools -> getTextElementBox');
        // trace('bbox: ${bbox}');
        // trace(bbox);

        return {
            x: bbox.x,
            y: bbox.y,
            width: bbox.width,
            height: bbox.height,
        }
    }

    public static function setTransformTranslation( svg : SVGElement, e : js.html.svg.GraphicsElement, tx : Float, ty : Float ) {
        // https://drafts.csswg.org/css-transforms-1/#mathematical-description

        // js.Syntax.code("console.log({0}, {1}, {2})", 'e.transform', e.transform, '_' );
        // trace('setTransformTranslation', tx, ty);

        var el_transform = e.transform.baseVal;

        var tfm = svg.createSVGTransform();

        if( el_transform.length == 0 ) {

            // list is empty, no transforms
            tfm.setTranslate( tx, ty );

        } else {

            // list is not empty, el_tfm is DOMMatrix
            // https://www.w3.org/TR/geometry-1/#DOMMatrix
            var el_tfm = el_transform.consolidate();

            var el_mtx = el_tfm.matrix;
            var matrix = new DOMMatrix([
                el_mtx.a,
                el_mtx.b,
                el_mtx.c,
                el_mtx.d,
                0,
                0,
            ]);
            matrix.translateSelf( tx, ty );

            tfm.setMatrix( cast matrix );

            el_transform.clear();
        }

        el_transform.appendItem( tfm );

        // js.Syntax.code("console.log({0}, {1}, {2})", 'el_transform', el_transform, '_' );
    }

    public static function applyTransforms( svg : SVGElement, e : js.html.svg.GraphicsElement, ops : Array<GraphicsTransformOperation> ) {

        // https://developer.mozilla.org/en-US/docs/Web/API/SVGGraphicsElement#instance_properties
        // GraphicsElement.transform            https://developer.mozilla.org/en-US/docs/Web/API/SVGAnimatedTransformList
        // GraphicsElement.transform.baseVal    https://developer.mozilla.org/en-US/docs/Web/API/SVGTransformList
        // https://svgwg.org/svg2-draft/coords.html#InterfaceSVGTransformList
        var el_transform = e.transform.baseVal;

        el_transform.clear();
        for( op in ops ) {
            var tfm = svg.createSVGTransform();
            switch( op ) {
                case Translate(tx, ty): tfm.setTranslate(tx,ty);
                case TranslateX(tx): tfm.setTranslate(tx,0);
                case TranslateY(ty): tfm.setTranslate(0,ty);
                case Rotate(angle): tfm.setRotate(angle,0,0);
                case Scale(sx, sy): tfm.setScale(sx,sy);
                case ScaleX(s): tfm.setScale(s,1);
                case ScaleY(s): tfm.setScale(1,s);
                case SkewX(angle): tfm.setSkewX(angle);
                case SkewY(angle): tfm.setSkewY(angle);
            }
            el_transform.appendItem( tfm );
        }
        el_transform.consolidate();
    }

    public static function getAttributeName( ga : GraphicsAttribute ) : String {
        switch( ga ) {
            case Id: return 'id';

            case Stroke: return 'stroke';
            case StrokeWidth: return 'stroke-width';
            case StrokeOpacity: return 'stroke-opacity';
            case Fill: return 'fill';
            case FillOpacity: return 'fill-opacity';

            case Opacity: return 'opacity';
            case Visible: return 'visible';

            case FontFamily: return 'font-family';
            case FontSize: return 'font-size';
            case FontStyle: return 'font-style';
            case FontWeight: return 'font-weight';

            case Custom(name): return name;
        }
    }

    public static function getGraphicsMouseEventName( en : GraphicsEventName ) : String {
        switch( en ) {
            case MouseUp: return 'mouseup';
            case MouseDown: return 'mousedown';
            case MouseEnter: return 'mouseenter';
            case MouseLeave: return 'mouseleave';
            case MouseClick: return 'click';
            case MouseMove: return 'mousemove';
            case MouseWheel: return 'wheel';

            case Custom(evtName): return evtName;
        }
    }

    public static function setAttribute( e : js.html.svg.GraphicsElement, attr : GraphicsAttribute, value : String ) : Void {
        if( attr == Visible ) {
            if( value == 'false' )
                e.setAttributeNS( null, 'display', 'none' );
            else
                e.setAttributeNS( null, 'display', 'inline' );
            return;
        }
        e.setAttributeNS( null, SVGTools.getAttributeName( attr ), value );
    }

    public static function setAttributeNS( e : js.html.svg.GraphicsElement, ns : String, ns_id : String, attr : GraphicsAttribute, value : String ) : Void {
        if( attr == Visible ) {
            if( value == 'false' )
                e.setAttributeNS( null, 'display', 'none' );
            else
                e.setAttributeNS( null, 'display', 'inline' );
            return;
        }
        e.setAttributeNS( ns, '${ns_id}:${SVGTools.getAttributeName( attr )}', value );
    }

    public static function createPathCommandsForPanel( x : Float, y : Float, width : Float, height : Float, ?options : PanelOptions ) : String {
        // trace('options', options);

        // sanitize options
        if( options == null ) options = {};
        if( options.rx == null ) options.rx = 0;
        if( options.ry == null ) options.ry = 0;
        if( options.tl == null ) options.tl = false;
        if( options.tr == null ) options.tr = false;
        if( options.bl == null ) options.bl = false;
        if( options.br == null ) options.br = false;
        if( options.curved == null ) options.curved = true;
        // trace('options after', options);

        final c = 0.551915024494;
        final ci = 0.448084975506;

        final h = height;
        final w = width;

        var tl_rx = options.tl ? options.rx : 0.0;
        var tl_ry = options.tl ? options.ry : 0.0;

        var tr_rx = options.tr ? options.rx : 0.0;
        var tr_ry = options.tr ? options.ry : 0.0;

        var bl_rx = options.bl ? options.rx : 0.0;
        var bl_ry = options.bl ? options.ry : 0.0;

        var br_rx = options.br ? options.rx : 0.0;
        var br_ry = options.br ? options.ry : 0.0;

        var initial = '
            M ${x},${y}
        ';

        var top = '
            m ${tl_rx}, 0
            h ${w - tl_rx - tr_rx}
        ';

        var top_right = '';
        if( options.tr ) {
            if( options.curved )
            top_right = '
                c ${c * tr_rx},0
                ${tr_rx},${ci * tr_ry}
                ${tr_rx},${tr_ry}
            ';
            else
                top_right = 'l ${tr_rx},${tr_ry}';
        }

        var right = '
            v ${h - tr_ry - br_ry}
        ';

        var bottom_right = '';
        if( options.br ) {
            if( options.curved )
            bottom_right = '
                c 0,${ci * br_ry}
                -${ci * br_rx},${br_ry}
                -${br_rx},${br_ry}
            ';
            else
                bottom_right = 'l -${br_rx},${br_ry}';
        }

        var bottom = '
            h -${w - br_rx - bl_rx}
        ';

        var bottom_left = '';
        if( options.bl ) {
            if( options.curved )
            bottom_left = '
                c -${c * bl_rx},0
                -${bl_rx},-${ci * bl_ry}
                -${bl_rx},-${bl_ry}
            ';
            else
                bottom_left = 'l -${bl_rx},-${bl_ry}';
        }

        var left = '
            v -${h - bl_ry - tl_ry}
        ';

        var top_left = '';
        if( options.tl ) {
            if( options.curved )
            top_left = '
                c 0,-${c * tl_ry}
                ${ci * tl_rx},-${tl_ry}
                ${tl_rx},-${tl_ry}
            ';
            else
                top_left = 'l ${tl_rx},-${tl_ry}';
        }

        var d = '
        ${initial}
        ${top}
        ${top_right}
        ${right}
        ${bottom_right}
        ${bottom}
        ${bottom_left}
        ${left}
        ${top_left}
        z
        ';
        // trace('options d', d);
        return d;
    }

    public static function toPx( value : Float ) : String {
        if( ! value.isNumber() ) return '0px';

        return '${Std.int(value)}px';
    }
}

private interface SvgGraphicsElement extends GraphicsElement {
    var e(default,null) : js.html.svg.GraphicsElement;
    var event_target(default,null) : js.html.svg.GraphicsElement;

    // function update(); <- this has to be class-specific so we must cast() and then call
    function setAttribute( attr : GraphicsAttribute, value : GraphicsAttributeValue ) : Void;
    // function getAttribute( el : GraphicsElement, attr : GraphicsAttribute ) : GraphicsAttributeValue;
    // function removeAttribute( el : GraphicsElement, attr : GraphicsAttribute ) : Void;

    /**
     * Translate origin relative to parent
     * @param x
     * @param y
     */
    function setOrigin( x : Float, y : Float ) : Void;
    /**
     * Apply scaling transformation to all significant points, relative to origin
     * @param sx
     * @param sy
     */
    function setScale( sx : Float, sy : Float ) : Void;
    /**
     * Rotate coordinate system around origin
     * @param angle rotation angle in radians
     */
    function setRotation( angle : Float ) : Void;
     /**
     * Apply transforms relative to origin
     * @param ops
     */
    function transform( ops : Array<GraphicsTransformOperation> ) : Void;
    function clearTransform() : Void;

    function getBoundingBox() : GraphicsBox;

    // function _events();
}

private class SvgGraphicsElementBase implements SvgGraphicsElement {
	public var e(default, null) : js.html.svg.GraphicsElement;
    public var event_target(default,null) : js.html.svg.GraphicsElement;

    var svg (default, null) : SVGElement;
    var focus_e(default,null) : js.html.svg.GraphicsElement;

    var coreScale : GraphicsPoint;
    var coreRotation : Float;
    var coreTranslation : GraphicsPoint;

    public function new( svg : SVGElement, ge : js.html.svg.GraphicsElement ) {
        this.svg = svg;
        e = ge;
        focus_e = e;
        // focus_e = null;
        event_target = e;

        coreScale = { x: 1.0, y: 1.0, }
        coreRotation = 0.0;
        coreTranslation = { x: 0.0, y: 0.0, }
    }

    public function setAttribute( attr : GraphicsAttribute, value : GraphicsAttributeValue ) : Void {
        SVGTools.setAttribute( focus_e, attr, value );
    }
    public function setOrigin( x : Float, y : Float ) : Void {
        coreTranslation = { x: x, y: y, }
        setCoreTransforms();
        // SVGTools.setTransformTranslation( svg, e, x, y );
        // SVGTools.applyTransforms( svg, e, [Translate(x,y)] );
    }
    public function setScale( sx : Float, sy : Float ) : Void {
        coreScale = { x: sx, y: sy, }
        setCoreTransforms();
    }
    public function setRotation( angle : Float ) : Void {
        coreRotation = angle;
        setCoreTransforms();
    }
    function setCoreTransforms() {
        SVGTools.applyTransforms( svg, e, [
            Scale( coreScale.x, coreScale.y ),
            Rotate( coreRotation ),
            Translate( coreTranslation.x, coreTranslation.y ),
        ]);
    }
    public function transform( ops : Array<GraphicsTransformOperation> ) : Void {
        SVGTools.applyTransforms( svg, focus_e, ops );
    }
    public function clearTransform() : Void {
        focus_e.transform.baseVal.clear();
    }
    public function getBoundingBox() : GraphicsBox {
        // return SVGTools.getElementBox( e );
        return SVGTools.getElementBox( focus_e );
    }
}

private class Group extends SvgGraphicsElementBase {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'g' ) );
        focus_e = e;
    }
    override public function setAttribute( attr : GraphicsAttribute, value : GraphicsAttributeValue ) : Void {
        SVGTools.setAttribute( e, attr, value );
    }
    override public function transform( ops : Array<GraphicsTransformOperation> ) : Void {
        SVGTools.applyTransforms( svg, e, ops );
    }
    override public function clearTransform() : Void {
        e.transform.baseVal.clear();
    }
}

private class SvgGraphicsElementElement extends SvgGraphicsElementBase {
    public function new( svg : SVGElement, el : js.html.svg.GraphicsElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'g' ) );
        focus_e = el;
        e.appendChild( focus_e );
        event_target = focus_e;
    }
    // override public function setAttribute( attr : GraphicsAttribute, value : GraphicsAttributeValue ) : Void {}
    // override public function transform( ops : Array<GraphicsTransformOperation> ) : Void {}
    // override public function clearTransform() : Void {}
}

private class Path extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'path' ) );
    }
    public function update( pi : Array<PathInstruction> ) {
        var d = new SvgPathBuilder(pi).build();
        setAttribute( Custom('d'), d );
    }
}
private class Circle extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'circle' ) );
    }
    public function update( cx : Float, cy : Float, r : Float ) {
        setAttribute( Custom('cx'), '${cx}' );
        setAttribute( Custom('cy'), '${cy}' );
        setAttribute( Custom('r'), '${r}' );
    }
}
private class Ellipse extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'ellipse' ) );
    }
    public function update( cx : Float, cy : Float, rx : Float, ry : Float ) {
        setAttribute( Custom('cx'), '${cx}' );
        setAttribute( Custom('cy'), '${cy}' );
        setAttribute( Custom('rx'), '${rx}' );
        setAttribute( Custom('ry'), '${ry}' );
    }
}
private class Rect extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'rect' ) );
    }
    public function update( x : Float, y : Float, width : Float, height : Float, ?rx : Float, ?ry : Float ) {
        setAttribute( Custom('x'),'${x}' );
        setAttribute( Custom('y'),'${y}' );
        setAttribute( Custom('width'),'${width}' );
        setAttribute( Custom('height'),'${height}' );
        if( rx.isNumber() )
            setAttribute( Custom('rx'),'${rx}' );
        if( ry.isNumber() )
            setAttribute( Custom('ry'),'${ry}' );

    }
}
private class Line extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'line' ) );
    }
    public function update( x1 : Float, y1 : Float, x2 : Float, y2 : Float ) {
        setAttribute( Custom('x1'),'${x1}' );
        setAttribute( Custom('y1'),'${y1}' );
        setAttribute( Custom('x2'),'${x2}' );
        setAttribute( Custom('y2'),'${y2}' );
    }
}
private class CubicCurve extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'path' ) );
    }
    public function update( p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint, p3 : GraphicsPoint ) {

        var d = new SvgPathBuilder([
            CPathMoveTo(p0),
            CPathCubicCurveTo(p1,p2,p3),
        ]).build();

        setAttribute( Custom('d'), d );
    }
}
private class QuadraticCurve extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'path' ) );
    }
    public function update( p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint ) {

        var d = new SvgPathBuilder([
            CPathMoveTo(p0),
            CPathQuadraticCurveTo(p1,p2),
        ]).build();

        setAttribute( Custom('d'), d );
    }
}
private class Text extends SvgGraphicsElementElement {
    var _x : Float;
    var _y : Float;
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'text' ) );
        // we have to append 'text' element to 'svg' now
        // in order to be able to get text position and size
        // right away, i.e. in subsequent calls to 'update()' method
        svg.appendChild( e );
        _x = 0;
        _y = 0;
    }
    override public function getBoundingBox() : GraphicsBox {
        return SVGTools.getTextElementBox( svg, e );
    }
    override public function setAttribute( attr : GraphicsAttribute, value : GraphicsAttributeValue ) : Void {
        // trace('TEXT -> setAttribute');
        // trace('attr: ${Type.enumConstructor(attr)}');
        SVGTools.setAttribute( focus_e, attr, value );
        // we need to account for change in text size, style, font family, etc.
        // and fix the position because width/height of the text could change
        // which will change where the top-left of the text bbox is relative
        // to its dominant-baseline
        fixPosition();
    }
    function fixPosition() {
        var aabb = getBoundingBox();

        // trace('TEXT -> fixPosition');
        // js.Syntax.code("console.log({0}, {1}, {2})", 'e', e, '_' );
        // js.Syntax.code("console.log({0}, {1}, {2})", 'focus_e', focus_e, '_' );
        // trace('aabb: ${aabb}');
        // trace('B: ${_x},${_y}');
        _x = _x - aabb.x;
        _y = _y - aabb.y;
        // trace('A: ${_x},${_y}');

        SVGTools.setAttribute( focus_e, Custom('x'), '${_x}' );
        SVGTools.setAttribute( focus_e, Custom('y'), '${_y}' );
    }
    public function update( text : String, ?x : Float, ?y : Float ) {
        // trace('TEXT -> update');
        // trace('I: ${x},${y}');

        _x = x.isNumber() ? x : _x;
        _y = y.isNumber() ? y : _y;
        // trace('S: ${_x},${_y}');

        // castAsHtmlElement(txt.e).appendChild( Browser.document.createTextNode( text ) );
        // this does the same as above line, see: https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent#value
        // castAsHtmlElement(txt.e).textContent = text;
        // this removes ALL children of the element's html node
        // and replaces them with single js.html.Text node
        focus_e.textContent = text;

        // fix text position
        // for SVG, text's X,Y attributes represent start of the text's dominant baseline
        // https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/dominant-baseline
        // and NOT bottom-left coordinates of the text's bounding box.
        // the baseline in general is positioned somewhere between top and bottom
        // of the space that text occupies.
        SVGTools.setAttribute( focus_e, Custom('x'), '${_x}' );
        SVGTools.setAttribute( focus_e, Custom('y'), '${_y}' );

        // js.Syntax.code("console.log({0}, {1}, {2})", 'Text.update 1', SVGTools.getElementBox( focus_e ), '_' );

        fixPosition();

        // js.Syntax.code("console.log({0}, {1}, {2})", 'Text.update 2', SVGTools.getElementBox( focus_e ), '_' );

        // Vertically center, horizontally left align text
        // setAttribute(txt,Custom('dominant-baseline'),'middle');
        // Vertically display below baseline
        // setAttribute(txt,Custom('dominant-baseline'),'hanging');
        // setAttribute(txt,Custom('text-anchor'),'start'); // default
    }
}
private class Image extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'image' ) );
    }
    override public function setAttribute( attr : GraphicsAttribute, value : GraphicsAttributeValue ) : Void {
        switch( attr ) {
            // https://developer.mozilla.org/en-US/docs/Web/CSS/outline-color
            case Stroke: focus_e.style.outlineColor = value; focus_e.style.outlineStyle = 'solid';
            // https://developer.mozilla.org/en-US/docs/Web/CSS/outline-width
            case StrokeWidth: focus_e.style.outlineWidth = SVGTools.toPx( Std.parseFloat(value) );
            case StrokeOpacity: return;
            default: super.setAttribute( attr, value );
        }
    }
    public function update( href : String, ?width : Float, ?height : Float, ?x : Float, ?y : Float ) {
        if( x.isNumber() ) setAttribute( Custom('x'), '${x}' );
        if( y.isNumber() ) setAttribute( Custom('x'), '${x}' );
        if( width.isNumber() ) setAttribute( Custom('width'),'${width}' );
        if( height.isNumber() ) setAttribute( Custom('height'),'${height}' );
        SVGTools.setAttributeNS( focus_e, SvgNamespaces.XLink_NS, 'xlink', Custom('href'), href );
    }
}
private class Panel extends SvgGraphicsElementElement {
    public function new( svg : SVGElement ) {
        super( svg, SvgNamespaces.createW3CSvgGraphicsElement( 'path' ) );
    }
    public function update( width : Float, height : Float, x : Float, y : Float, ?options : PanelOptions ) {
        setAttribute( Custom('d'), SVGTools.createPathCommandsForPanel( x, y, width, height, options ) );
    }
}



class SvgGraphicsImplementation implements Graphics {

    public var svg (default, null) : SVGElement;
    var pt : Point;


    function castAsSvgGraphicsElement( e : GraphicsElement ) : SvgGraphicsElement {
        return cast e;
    }

    function setAttributeNS( el : GraphicsElement, ns : String, ns_id : String, attr : GraphicsAttribute, value : String ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        if( attr == Visible ) {
            if( value == 'false' )
                sel.e.setAttributeNS( null, 'display', 'none' );
            else
                sel.e.setAttributeNS( null, 'display', 'inline' );
            return;
        }
        sel.e.setAttributeNS( ns, '${ns_id}:${SVGTools.getAttributeName( attr )}', value );
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // ATTRIBUTES
    //
    /////////////////////////////////////////////////////////////////////////////
    public function setAttribute( el : GraphicsElement, attr : GraphicsAttribute, value : GraphicsAttributeValue ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        sel.setAttribute( attr, value );
    }

    public function setAttributes( el : GraphicsElement, attrs : Map<GraphicsAttribute, GraphicsAttributeValue> ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        for( attr=>value in attrs.keyValueIterator() )
            sel.setAttribute( attr, value );
            // sel.e.setAttributeNS( null, SVGTools.getAttributeName( attr ), value );
    }

    public function getAttribute( el : GraphicsElement, attr : GraphicsAttribute ) : GraphicsAttributeValue {
        var sel = castAsSvgGraphicsElement( el );
        return sel.e.getAttribute( SVGTools.getAttributeName( attr ) );
    }

    public function removeAttribute( el : GraphicsElement, attr : GraphicsAttribute ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        sel.e.removeAttributeNS( null, SVGTools.getAttributeName( attr ) );
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // CREATE
    //
    /////////////////////////////////////////////////////////////////////////////
    function createSvgElement( tag : String ) : SvgGraphicsElement {
        return new SvgGraphicsElementBase( svg, cast Browser.document.createElementNS( SvgNamespaces.SVG_NS, tag ) );
    }


    public function createPath( pi : Array<PathInstruction> ) : GraphicsElement {
        var path = new Path( svg );
        path.update( pi );
        return path;
    }

    public function createImage( href : String, ?width : Float, ?height : Float, ?x : Float, ?y : Float ) : GraphicsElement {

        var image = new Image( svg );
        image.update( href, width, height, x, y );

        return image;
    }

    public function updateImage( el : GraphicsElement, href : String, ?width : Float, ?height : Float, ?x : Float, ?y : Float ) : Void {
        var image = cast( el, Image );
        image.update( href, width, height, x, y );
    }

    public function createCircle( cx : Float, cy : Float, r : Float ) : GraphicsElement {
        var circle = new Circle( svg );
        circle.update( cx, cy, r );
        return circle;
    }

    public function updatCircle( el : GraphicsElement, cx : Float, cy : Float, r : Float ) : Void {
        var circle = cast( el, Circle );
        circle.update( cx, cy, r );
    }

    public function createEllipse( cx : Float, cy : Float, rx : Float, ry : Float ) : GraphicsElement {
        var ellipse = new Ellipse( svg );
        ellipse.update( cx, cy, rx, ry );
        return ellipse;
    }

    public function updateEllipse( el : GraphicsElement, cx : Float, cy : Float, rx : Float, ry : Float ) : Void {
        var ellipse = cast( el, Ellipse );
        ellipse.update( cx, cy, rx, ry );
    }

    public function createGroup( children : Array<GraphicsElement> ) : GraphicsElement {
        var group = new Group( svg );
        for( c in children )
            group.e.appendChild( castAsSvgGraphicsElement( c ).e );
        return group;
    }

    public function createRect( x : Float, y : Float, width : Float, height : Float, ?rx : Float, ?ry : Float ) : GraphicsElement {

        var rect = new Rect( svg );
        rect.update( x, y, width, height, rx, ry );

        return rect;
    }

    public function updateRect( el : GraphicsElement, x : Float, y : Float, width : Float, height : Float, ?rx : Float, ?ry : Float ) : Void {

        var rect = cast( el, Rect );
        rect.update( x, y, width, height, rx, ry );
    }

    public function createLine( x1 : Float, y1 : Float, x2 : Float, y2 : Float ) : GraphicsElement {

        var line = new Line( svg );
        line.update( x1, y1, x2, y2 );

        return line;
    }

    public function updateLine( el : GraphicsElement, x1 : Float, y1 : Float, x2 : Float, y2 : Float ) : Void {

        var line = cast( el, Line );
        line.update( x1, y1, x2, y2 );
    }

     public function createCubicCurve( p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint, p3 : GraphicsPoint ) : GraphicsElement {

        var curve = new CubicCurve( svg );
        curve.update( p0, p1, p2, p3 );

        return curve;
    }

    public function updatePath( el : GraphicsElement, pi : Array<PathInstruction> ) : Void {
        var path = cast( el, Path );
        path.update( pi );
    }

    public function updateCubicCurve( el : GraphicsElement, p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint, p3 : GraphicsPoint ) : Void {
        var curve = cast( el, CubicCurve );
        curve.update( p0, p1, p2, p3 );
    }

    public function createQuadraticCurve( p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint ) : GraphicsElement {

        var curve = new QuadraticCurve( svg );
        curve.update( p0, p1, p2 );

        return curve;
    }

    public function updateQuadraticCurve( el : GraphicsElement, p0 : GraphicsPoint, p1 : GraphicsPoint, p2: GraphicsPoint ) : Void {
        var curve = cast( el, QuadraticCurve );
        curve.update( p0, p1, p2 );
    }

    public function updatePanel( el : GraphicsElement,  width : Float, height : Float, x : Float, y : Float, ?options : PanelOptions ) : Void {
        var panel = cast( el, Panel );
        panel.update( width, height, x, y, options );
    }

    public function createPanel( width : Float, height : Float, ?x : Float, ?y : Float, ?options : PanelOptions ) : GraphicsElement {

        var x_ = x.isNumber() ? x : 0.0;
        var y_ = y.isNumber() ? y : 0.0;

        var panel = new Panel( svg );
        panel.update( width, height, x_, y_, options );

        return panel;
    }

    public function createText( text : String, ?x : Float, ?y : Float ) : GraphicsElement {

        var txt = new Text( svg );
        txt.update( text, x, y );

        return txt;
    }

    public function updateText( el : GraphicsElement, text : String, ?x : Float, ?y : Float ) : Void {
        var txt = cast( el, Text );
        txt.update( text, x, y );
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // EVENTS
    //
    /////////////////////////////////////////////////////////////////////////////
    public function addMouseEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: GraphicsMouseEvent -> Void ) : GraphicsEventHnd {
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        var handler = ( evt : Event ) -> {
            if( Std.is( evt, js.html.MouseEvent ) ) {
                var me : js.html.MouseEvent = cast evt;
                var localClient = clientPointToUser( me.clientX, me.clientY );
                handler({
                    // clientX: me.clientX,
                    // clientY: me.clientY,
                    clientX: localClient.x,
                    clientY: localClient.y,
                    movementX: me.movementX,
                    movementY: me.movementY,
                    buttons: me.buttons,
                    keys: SVGTools.mouseEventKeysToKeys( me ),
                });
            }
        };

        tgtEl.addEventListener( SVGTools.getGraphicsMouseEventName( evtName ), handler );

        return cast handler;
    }
    public function dispatchMouseEvent( el : GraphicsElement, evtName : GraphicsEventName, event: GraphicsMouseEvent ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        var htmlMouseEvent = new js.html.MouseEvent( SVGTools.getGraphicsMouseEventName( evtName ), {
            // button: 0,
            clientX: Std.int(event.clientX),
            clientY: Std.int(event.clientY),
            movementX: Std.int(event.movementX),
            movementY: Std.int(event.movementY),
            // screenX: 0,
            // screenY: 0,
            relatedTarget: tgtEl,
            buttons: event.buttons,
            altKey: event.keys.hasAltKey(),
            ctrlKey: event.keys.hasCtrlKey(),
            shiftKey: event.keys.hasShiftKey(),
            metaKey: event.keys.hasMetaKey(),
        });
        /* return */ tgtEl.dispatchEvent( htmlMouseEvent );
    }

    public function addWheelEventListener( el : GraphicsElement, handler: GraphicsWheelEvent -> Void ) : GraphicsEventHnd {
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        var handler = ( evt : Event ) -> {
            if( Std.is( evt, js.html.WheelEvent ) ) {
                var we : js.html.WheelEvent = cast evt;
                var localClient = clientPointToUser( we.clientX, we.clientY );
                handler({
                    // clientX: we.clientX,
                    // clientY: we.clientY,
                    clientX: localClient.x,
                    clientY: localClient.y,
                    movementX: we.movementX,
                    movementY: we.movementY,
                    buttons: we.buttons,
                    keys: SVGTools.mouseEventKeysToKeys( we ),

                    deltaMode: we.deltaMode,
                    deltaX: we.deltaX,
                    deltaY: we.deltaY,
                    deltaZ: we.deltaZ,
                });
            }
        };

        tgtEl.addEventListener( SVGTools.getGraphicsMouseEventName( MouseWheel ), handler );

        return cast handler;
    }
    public function dispatchWheelEvent( el : GraphicsElement, event: GraphicsWheelEvent ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        var htmlWheelEvent = new js.html.WheelEvent( SVGTools.getGraphicsMouseEventName( MouseWheel ), {
            // button: 0,
            clientX: Std.int(event.clientX),
            clientY: Std.int(event.clientY),
            movementX: Std.int(event.movementX),
            movementY: Std.int(event.movementY),
            // screenX: 0,
            // screenY: 0,
            relatedTarget: tgtEl,
            buttons: event.buttons,
            altKey: event.keys.hasAltKey(),
            ctrlKey: event.keys.hasCtrlKey(),
            shiftKey: event.keys.hasShiftKey(),
            metaKey: event.keys.hasMetaKey(),

            deltaMode: event.deltaMode,
            deltaX: event.deltaX,
            deltaY: event.deltaY,
            deltaZ: event.deltaZ,
        });
        /* return */ tgtEl.dispatchEvent( htmlWheelEvent );
    }

    public function addEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: GraphicsEvent -> Void ) : GraphicsEventHnd {
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        var _handler = ( evt : Event ) -> {
            handler( cast evt );
        };

        tgtEl.addEventListener( SVGTools.getGraphicsMouseEventName( evtName ), _handler );

        return cast _handler;
    }
    public function dispatchEvent( el : GraphicsElement, evtName : GraphicsEventName, event: GraphicsEvent ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        var htmlMouseEvent = new js.html.Event( SVGTools.getGraphicsMouseEventName( evtName ) );
        /* return */ tgtEl.dispatchEvent( htmlMouseEvent );
    }

    public function addCustomEventListener( el : GraphicsElement, evtName : GraphicsEventName, handler: Dynamic -> Void ) : GraphicsEventHnd {
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        // https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/detail
        var _handler = ( evt : Event ) -> {
            if( Std.is( evt, CustomEvent ) ) {
                var ce : CustomEvent = cast evt;
                handler( ce.detail );
            }
        };

        tgtEl.addEventListener( SVGTools.getGraphicsMouseEventName( evtName ), _handler );

        return cast _handler;
    }
    public function dispatchCustomEvent( el : GraphicsElement, evtName : GraphicsEventName, event: Dynamic ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        var customEvent = new js.html.CustomEvent( SVGTools.getGraphicsMouseEventName( evtName ), {
            detail: event,
        });
        /* return */ tgtEl.dispatchEvent( customEvent );
    }

    public function removeEventListener( el : GraphicsElement, evtName : GraphicsEventName, handle: GraphicsEventHnd ) : Void {
        if( handle == null || el == null ) return;
        var sel = castAsSvgGraphicsElement( el );
        // var tgtEl = sel.e;
        var tgtEl = sel.event_target;

        tgtEl.removeEventListener( SVGTools.getGraphicsMouseEventName( evtName ), handle );
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // STRUCTURE
    //
    /////////////////////////////////////////////////////////////////////////////
    public function appendChild( el : GraphicsElement, ?c : GraphicsElement ) {
        var sel = castAsSvgGraphicsElement( el );
        if( c == null ) {
            svg.appendChild( sel.e );
            return;
        }

        sel.e.appendChild( castAsSvgGraphicsElement( c ).e );
    }

    public function removeChild( el : GraphicsElement, ?c : GraphicsElement ) {
        var sel = castAsSvgGraphicsElement( el );
        if( c == null ) {
            svg.removeChild( sel.e );
            return;
        }

        sel.e.removeChild( castAsSvgGraphicsElement( c ).e );
    }

    public function moveToFront( el : GraphicsElement, ?c : GraphicsElement ) {
        var sel = castAsSvgGraphicsElement( el );
        if( c == null ) {
            svg.appendChild( sel.e );
            return;
        }

        sel.e.appendChild( castAsSvgGraphicsElement( c ).e );
    }

    public function moveToBack( el : GraphicsElement, ?c : GraphicsElement ) {
        var sel = castAsSvgGraphicsElement( el );
        if( c == null ) {
            svg.insertBefore( sel.e, svg.firstChild );
            return;
        }

        var ele = sel.e;
        ele.insertBefore( castAsSvgGraphicsElement( c ).e, ele.firstChild );
    }

    /////////////////////////////////////////////////////////////////////////////
    //
    // MISC
    //
    /////////////////////////////////////////////////////////////////////////////
    public function setOrigin( el : GraphicsElement, x : Float, y : Float ) {
        var sel = castAsSvgGraphicsElement( el );
        sel.setOrigin( x, y );
    }
    public function setScale( el : GraphicsElement, sx : Float, sy : Float ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        sel.setScale( sx, sy );
    }
    public function setRotation( el : GraphicsElement, angle : Float ) : Void {
        var sel = castAsSvgGraphicsElement( el );
        sel.setRotation( angle );
    }

    public function transform( el : GraphicsElement, ops : Array<GraphicsTransformOperation> ) {
        var sel = castAsSvgGraphicsElement( el );

        sel.transform( ops );
    }

    public function clearTransform( el : GraphicsElement ) : Void {
        var sel = castAsSvgGraphicsElement( el );

        sel.clearTransform();
    }

    /**
     * Convert point in client coordinate system (i.e. html body element)
     * to the user coordinate system of the SVG viewport for this
     * SVG document fragment.
     **/
     public function clientPointToUser( x : Float, y : Float ) : GraphicsPoint {
        pt.x = x;
        pt.y = y;
        var p = pt.matrixTransform(svg.getScreenCTM().inverse());
        return {x:p.x,y:p.y};
    }
    function userPointToClient( x : Float, y : Float ) : GraphicsPoint {
        pt.x = x;
        pt.y = y;
        var p = pt.matrixTransform(svg.getScreenCTM());
        return {x:p.x,y:p.y};
    }

    /**
     * Return element's size, and position relative to the top-left corner of this SVG doc's viewport
     * @param el
     * @return GraphicsBox
     */
    public function getElementBox( el : GraphicsElement ) : GraphicsBox {
        var sel = castAsSvgGraphicsElement( el );
        return sel.getBoundingBox();
    }

    public function getElementFromPoint( x : Float, y : Float ) : GraphicsElement {
        // TODO this should FIND proper SvgGraphicsElement ??
        // https://developer.mozilla.org/en-US/docs/Web/API/Document/elementFromPoint
        var clientPt = userPointToClient( x, y );
        var el = Browser.document.elementFromPoint( clientPt.x, clientPt.y );
        if( Std.isOfType( el, js.html.svg.GraphicsElement ) ) {
            return new SvgGraphicsElementBase( svg, cast el );
        }
        return null;
    }

    public function getElementsFromPoint( x : Float, y : Float ) : Array<GraphicsElement> {
        // TODO this should FIND proper SvgGraphicsElement for each 'el' ??
        // https://developer.mozilla.org/en-US/docs/Web/API/Document/elementsFromPoint
        var clientPt = userPointToClient( x, y );
        var elements = Browser.document.elementsFromPoint( clientPt.x, clientPt.y ).filter( e -> Std.isOfType( e, js.html.svg.GraphicsElement ) );
        return [ for( el in elements ) new SvgGraphicsElementBase( svg, cast el ) ];
    }
}
