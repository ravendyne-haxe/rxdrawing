// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxsvggraphics;

import sys.io.File;

using StringTools;

final class SvgGraphicsToolsMain {
    private function new(){}

    public static function main() : Void {
        Sys.println('');
        Sys.println('SvgGraphicsTools');
        Sys.println('----------------');
        Sys.println('');

        var args = Sys.args();
        // trace( Sys.args() );
        if( args.length == 0 ) return;

        var tool = args.shift();

        switch( tool ) {
            case 'c':
                if( args.length < 1 ) {
                    trace('Tool "c" needs svg path command string');
                    return;
                }
                var d = args[0];
                var pis = SvgGraphicsTools.convertPathDToCommands( d );
                var source = SvgGraphicsTools.pathInstructionsToSource( pis );
                var cd = new SvgPathBuilder( pis ).build();
                Sys.println('Input:');
                Sys.println('d = "${d}"');
                Sys.println('');
                Sys.println('Source:');
                Sys.println(source);
                Sys.println('');
                Sys.println('Converted input:');
                Sys.println('d = "${cd}"');
                Sys.println('');

            default:
                trace('Unknown tool "${tool}"');
        }

    }
}
