// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxsvggraphics;

import rxdrawing.core.graphics.Graphics.GraphicsPoint;
import svgrx.format.svgpath.Reader;
import svgrx.format.svgpath.Data;
import rxdrawing.core.graphics.Graphics.PathInstruction;

using StringTools;

final class SvgGraphicsTools {
    private function new(){}

    public static function convertPathDToCommands( d : String ) : Array<PathInstruction> {
        return pathCommandsFromElements( new Reader( d ).read().elements );
    }

    static function pathCommandsFromElements( elements : Array<PathElement> ) : Array<PathInstruction> {


        if( elements.length > 0 && ! elements[0].match(CMoveTo(_)) ) {
            throw new haxe.Exception( 'path commands string must start with "moveTo" ( "M" or "m" ) command' );
        }

        var pathInstructionList : Array<PathInstruction> = [];

        var currentPoint : GraphicsPoint = { x: 0, y: 0 };
        var subPathStartPoint = currentPoint;
        var prevControlPoint = currentPoint;

        if( elements.length > 0 && ! elements[0].match(CMoveTo(_)) ) {
            throw 'path commands string must start with "moveTo" ( "M" or "m" ) command';
        }

        for( el in elements ) {

            switch( el ) {

                case CMoveTo(x, y, relative):
                    if( relative )
                    {
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    // pathInstructionList.moveTo({ x: x, y: y });
                    pathInstructionList.push( CPathMoveTo( { x: x, y: y } ) );

                    prevControlPoint = null;
                    currentPoint = { x: x, y: y };

                    // Reset the subpath start point for CClosePath
                    subPathStartPoint = currentPoint;

                case CLineTo(x, y, relative):
                    if( relative )
                    {
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    // pathInstructionList.lineTo({ x: x, y: y });
                    pathInstructionList.push( CPathLineTo( { x: x, y: y } ) );

                    prevControlPoint = null;
                    currentPoint = { x: x, y: y };

                case CLineToHorizontal(x, relative):
                    var y = currentPoint.y;
                    if( relative )
                    {
                        x += currentPoint.x;
                    }

                    // pathInstructionList.lineTo({ x: x, y: y });
                    pathInstructionList.push( CPathLineTo( { x: x, y: y } ) );

                    prevControlPoint = null;
                    currentPoint = { x: x, y: y };

                case CLineToVertical(y, relative):
                    var x = currentPoint.x;
                    if( relative )
                    {
                        y += currentPoint.y;
                    }

                    // pathInstructionList.lineTo({ x: x, y: y });
                    pathInstructionList.push( CPathLineTo( { x: x, y: y } ) );

                    prevControlPoint = null;
                    currentPoint = { x: x, y: y };

                case CQuadraticBezier(x1, y1, x, y, relative):
                    if( relative )
                    {
                        x1 += currentPoint.x;
                        y1 += currentPoint.y;
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    // pathInstructionList.quadraticCurveTo( [x1, y1], [x, y] );
                    pathInstructionList.push( CPathQuadraticCurveTo(  { x: x1, y: y1 }, { x: x, y: y }  ) );

                    // Save control point for 'CQuadraticBezierSmooth' element
                    prevControlPoint = { x: x1, y: y1 };

                    currentPoint = { x: x, y: y };

                case CQuadraticBezierSmooth(x, y, relative):
                    if( relative )
                    {
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    if( prevControlPoint == null ) prevControlPoint = currentPoint;

                    // https://www.w3.org/TR/SVG/implnote.html#PathElementImplementationNotes
                    // We need to rotate prevControlPoint 180 degrees around currentPoint
                    // to get the control point for this curve.
                    // Rotating point (x,y) 180 degrees around point (x0, y0):
                    // x' = 2 * x0 - x
                    // y' = 2 * y0 - y
                    var x1 = 2 * currentPoint.x - prevControlPoint.x;
                    var y1 = 2 * currentPoint.y - prevControlPoint.y;

                    // pathInstructionList.quadraticCurveTo( [x1, y1], [x, y] );
                    pathInstructionList.push( CPathQuadraticCurveTo(  { x: x1, y: y1 }, { x: x, y: y }  ) );

                    // Save control point for 'CQuadraticBezierSmooth' element
                    prevControlPoint = { x: x1, y: y1 };

                    currentPoint = { x: x, y: y };

                case CCubicBezier(x1, y1, x2, y2, x, y, relative):
                    if( relative )
                    {
                        x1 += currentPoint.x;
                        y1 += currentPoint.y;
                        x2 += currentPoint.x;
                        y2 += currentPoint.y;
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    // pathInstructionList.cubicCurveTo( [x1, y1], [x2, y2], [x, y] );
                    pathInstructionList.push( CPathCubicCurveTo(  { x: x1, y: y1 }, { x: x2, y: y2 }, { x: x, y: y }  ) );

                    // Save control point for 'CCubicBezierSmooth' element
                    prevControlPoint = { x: x2, y: y2 };

                    currentPoint = { x: x, y: y };

                case CCubicBezierSmooth(x2, y2, x, y, relative):
                    if( relative )
                    {
                        x2 += currentPoint.x;
                        y2 += currentPoint.y;
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    if( prevControlPoint == null ) prevControlPoint = currentPoint;

                    // https://www.w3.org/TR/SVG/implnote.html#PathElementImplementationNotes
                    // We need to rotate prevControlPoint 180 degrees around currentPoint
                    // to get the control point for this curve.
                    // Rotating point (x,y) 180 degrees around point (x0, y0):
                    // x' = 2 * x0 - x
                    // y' = 2 * y0 - y
                    var x1 = 2 * currentPoint.x - prevControlPoint.x;
                    var y1 = 2 * currentPoint.y - prevControlPoint.y;

                    // pathInstructionList.cubicCurveTo( [x1, y1], [x2, y2], [x, y] );
                    pathInstructionList.push( CPathCubicCurveTo(  { x: x1, y: y1 }, { x: x2, y: y2 }, { x: x, y: y }  ) );

                    // Save control point for 'CCubicBezierSmooth' element
                    prevControlPoint = { x: x2, y: y2 };

                    currentPoint = { x: x, y: y };

                case CEllipticArc(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, x, y, relative):
                    if( relative )
                    {
                        x += currentPoint.x;
                        y += currentPoint.y;
                    }

                    // tarnsform() would have to be applied here to xAxisRotation too somehow.
                    // Better way would be to convert arc to cubic curve instruction(s) and add that to the list instead.
                    // pathInstructionList.ellipticArcTo( rx, ry, xAxisRotation, largeArcFlag, sweepFlag, [x, y] );
                    pathInstructionList.push( CPathEllipticArcTo(  rx, ry, xAxisRotation, largeArcFlag, sweepFlag, { x: x, y: y }  ) );

                    prevControlPoint = null;
                    currentPoint = { x: x, y: y };

                case CClosePath:

                    var x = subPathStartPoint.x;
                    var y = subPathStartPoint.y;

                    // pathInstructionList.closePath();
                    pathInstructionList.push( CPathClosePath );

                    prevControlPoint = null;
                    currentPoint = { x: x, y: y };

                default:
            }
        }

        return pathInstructionList;
    }

    public static function pathInstructionsToSource( pia : Array<PathInstruction> ) : String {
        var sb = new StringBuf();

        sb.add('var cmds : Array<PathInstruction> = [');
        sb.add('\n');

        for( pi in pia ) {
            // trace(Type.enumConstructor(pi));
            // trace(Type.enumParameters(pi).join(','));
            sb.add( Type.enumConstructor( pi ) );
            var params = Type.enumParameters( pi );
            if( params.length > 0 ) {
                sb.add('(');
                sb.add( params.join(',') );
                sb.add(')');
            }
            sb.add(',');
            sb.add('\n');
        }

        sb.add('];');
        sb.add('\n');

        return sb.toString();
    }

}
