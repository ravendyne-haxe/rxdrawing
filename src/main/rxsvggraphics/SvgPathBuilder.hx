// Copyright 2023 - 2023, Ravendyne Inc
// SPDX-License-Identifier: MIT

package rxsvggraphics;

import rxdrawing.core.graphics.Graphics;

using StringTools;

class SvgPathBuilder {

    public var instructions ( default, null ) : Array<PathInstruction>;

    public function new( ?instruction_list : Array<PathInstruction>) {
        instructions = instruction_list == null ? [] : instruction_list;
    }

    public function moveTo( p : GraphicsPoint ): SvgPathBuilder {
        instructions.push( CPathMoveTo( p ) );
        return this;
    }

    public function lineTo( p : GraphicsPoint ): SvgPathBuilder {
        instructions.push( CPathLineTo( p ) );
        return this;
    }

    public function quadraticCurveTo( p2 : GraphicsPoint, p3 : GraphicsPoint ): SvgPathBuilder {
        instructions.push( CPathQuadraticCurveTo( p2, p3 ) );
        return this;
    }

    public function cubicCurveTo( p2 : GraphicsPoint, p3 : GraphicsPoint, p4 : GraphicsPoint ): SvgPathBuilder {
        instructions.push( CPathCubicCurveTo( p2, p3, p4 ) );
        return this;
    }

    public function ellipticArcTo( rx : Float, ry : Float, xAxisRotation : Float, largeArcFlag : Int, sweepFlag : Int, p2 : GraphicsPoint ): SvgPathBuilder {
        instructions.push( CPathEllipticArcTo( rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2) );
        return this;
    }

    public function closePath(): SvgPathBuilder {
        instructions.push( CPathClosePath );
        return this;
    }

    public function build() : String {

        var sb = new StringBuf();

        // https://www.w3.org/TR/SVG2/paths.html#PathData
        for( cmd in instructions ) {

            switch( cmd ) {

                case CPathMoveTo(p):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataMovetoCommands
                    sb.add('M');
                    sb.add( p.x );
                    sb.add(',');
                    sb.add( p.y );
                    sb.add(' ');

                case CPathLineTo(p):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataLinetoCommands
                    sb.add('L');
                    sb.add( p.x );
                    sb.add(',');
                    sb.add( p.y );
                    sb.add(' ');

                case CPathCubicCurveTo(p2, p3, p4):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataCubicBezierCommands
                    sb.add('C');
                    sb.add( p2.x );
                    sb.add(',');
                    sb.add( p2.y );
                    sb.add(' ');
                    sb.add( p3.x );
                    sb.add(',');
                    sb.add( p3.y );
                    sb.add(' ');
                    sb.add( p4.x );
                    sb.add(',');
                    sb.add( p4.y );
                    sb.add(' ');

                case CPathQuadraticCurveTo(p2, p3):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataQuadraticBezierCommands
                    sb.add('Q');
                    sb.add( p2.x );
                    sb.add(',');
                    sb.add( p2.y );
                    sb.add(' ');
                    sb.add( p3.x );
                    sb.add(',');
                    sb.add( p3.y );
                    sb.add(' ');

                case CPathEllipticArcTo(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataEllipticalArcCommands
                    sb.add('A');
                    sb.add( rx );
                    sb.add(',');
                    sb.add( ry );
                    sb.add(' ');
                    sb.add( xAxisRotation );
                    sb.add(' ');
                    sb.add( largeArcFlag );
                    sb.add(',');
                    sb.add( sweepFlag );
                    sb.add(' ');
                    sb.add( p2.x );
                    sb.add(',');
                    sb.add( p2.y );
                    sb.add(' ');

                case CPathMoveToRelative(p):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataMovetoCommands
                    sb.add('m');
                    sb.add( p.x );
                    sb.add(',');
                    sb.add( p.y );
                    sb.add(' ');

                case CPathLineToRelative(p):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataLinetoCommands
                    sb.add('l');
                    sb.add( p.x );
                    sb.add(',');
                    sb.add( p.y );
                    sb.add(' ');

                case CPathCubicCurveToRelative(p2, p3, p4):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataCubicBezierCommands
                    sb.add('c');
                    sb.add( p2.x );
                    sb.add(',');
                    sb.add( p2.y );
                    sb.add(' ');
                    sb.add( p3.x );
                    sb.add(',');
                    sb.add( p3.y );
                    sb.add(' ');
                    sb.add( p4.x );
                    sb.add(',');
                    sb.add( p4.y );
                    sb.add(' ');

                case CPathQuadraticCurveToRelative(p2, p3):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataQuadraticBezierCommands
                    sb.add('q');
                    sb.add( p2.x );
                    sb.add(',');
                    sb.add( p2.y );
                    sb.add(' ');
                    sb.add( p3.x );
                    sb.add(',');
                    sb.add( p3.y );
                    sb.add(' ');

                case CPathEllipticArcToRelative(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, p2):

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataEllipticalArcCommands
                    sb.add('a');
                    sb.add( rx );
                    sb.add(',');
                    sb.add( ry );
                    sb.add(' ');
                    sb.add( xAxisRotation );
                    sb.add(' ');
                    sb.add( largeArcFlag );
                    sb.add(',');
                    sb.add( sweepFlag );
                    sb.add(' ');
                    sb.add( p2.x );
                    sb.add(',');
                    sb.add( p2.y );
                    sb.add(' ');

                case CPathClosePath:

                    // https://www.w3.org/TR/SVG2/paths.html#PathDataClosePathCommand
                    sb.add('Z');
                    sb.add(' ');

            }
        }

        return sb.toString().trim();
    }

}
