#!/usr/bin/env bash

haxe www-demos.hxml || exit 1

# https://github.com/http-party/http-server
http-server build/demos -p 0 -c-1 -o
