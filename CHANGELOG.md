## Changelog

## 1.1.0

- remove `doLayout()` from `DrawingContainer` and call `layout_container.doLayout()` instead
